$(document).ready(function () {
  $("#lbluser").text(localStorage.getItem("user"));
  $("#lblemail").text(localStorage.getItem("email"));

  //console.log(localStorage.getItem("accessToken"));
  // && localStorage.getItem("role") === 0
  if (localStorage.getItem("accessToken") != null && localStorage.getItem("role") == 0) {
    loadpage();
  } else {
    window.location.href = "../login/login.html";
  }
});
function loadpage() {
  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  $("#exampleTableSearch tbody").remove("tr");
  var host_api = "http://128.199.231.68/closures/info";

  $.ajax({
    url: host_api,
    method: "GET",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      console.log(arrdetail);
      if (arrdetail.closure.length > 0) {
        arrdetail.closure.forEach(item => {
          console.log(item._id);
          var row = "<tr>";
          if (item.status == 1) {
            row += "<td style='color:green'>Activated</td>";
          } else {
            row += "<td style='color:red'>Deactivated</td>";
          }
          row += "<td>" + item.created_At + "</td>";
          row += "<td>" + item.expired_At + "</td>";
          row += "<td>" + item.expired_Edit + "</td>";          
          row += "<td>" + item.username + "</td>";          
          row +=
            "<td> <button class='btn btn - primary' data-target='#roleform' data-toggle='modal'type = 'button'  id='" + item._id + "'  onclick='chagerole(this.id)' >Edit <i class='icon md-account' aria-hidden='true'></i> </button ></td>";



          row += "</tr>";

          var tr = document.createElement("tr");
          tr.innerHTML = row;
          dataTable.row.add(tr);
        });
        dataTable.draw(false);

      } else {
        alert("Không có kết quả !");
        dataTable.clear();
        dataTable.draw();
      }
    },
    error(jqXHR) {
      //
    }
  });
}

function chagerole(id) {

  var id = id;
  var host_api = "http://128.199.231.68/closures/getClosureInfo";
  var data_str = {
    id: id
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      console.log(arrdetail);
      $("#newcreated_At").val(arrdetail.closure.created_At).trigger("change");
      $("#newexpired_At").val(arrdetail.closure.expired_At).trigger("change");
      $("#newexpired_Edit").val(arrdetail.closure.expired_Edit).trigger("change");
      $("#newuser").val(arrdetail.closure.username).trigger("change");
      $("#newid").val(arrdetail.closure._id).trigger("change");


      $("#newstatus").val(arrdetail.closure.status).trigger("change");

    },
    error(jqXHR) {
      //
    }
  });

}
function addnew() {

  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  var created_At = $("#created_At").val();
  var expired_At = $("#expired_At").val();
  var expired_Edit = $("#expired_Edit").val();
  var created_By = localStorage.getItem("id");
  var username = $("#lbluser").text(); 

  //console.log(created_At, expired_At, expired_Edit);

  //console.log(a, b, c);
  //return false;
  var host_api = "http://128.199.231.68/closures/createClosure";
  var data_str = {
    created_At: created_At,
    expired_At: expired_At,
    expired_Edit: expired_Edit,
    created_By: created_By,
    username: username
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      if (result.code == 300) {
        alert("OK");
        loadpage();

      } else {
        alert(result.err)
      }
    },
    error(jqXHR) {
      //
    }
  });
}
function savechange() {
  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  
  var created_At = $("#newcreated_At").val();
  var expired_At = $("#newexpired_At").val();
  var expired_Edit = $("#newexpired_Edit").val();
  var created_By = localStorage.getItem("id");
  var username = $("#newuser").text();
  
  var host_api = "http://128.199.231.68/closures/updateClosure";
  var data_str = {
    created_At: created_At,
    expired_At: expired_At,
    expired_Edit: expired_Edit,
    created_By: created_By,
    username: username
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      if (result.code == 300) {
        alert("OK");
        loadpage();

      } else {
        alert(result.err)
      }


    },
    error(jqXHR) {
      //
    }
  });
}

function downloadZip(){
  var host_api = 'http://128.199.231.68/manage/downloadArticles';
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      if (result.code == 300) {
        alert("OK");

      } else {
        alert(result.err)
      }


    },
    error(jqXHR) {
      //
    }
  });
}
