<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Register | Remark Material Admin Template</title>

    <link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">

    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/register.css')}}">
    <!-- alert css -->
    <link rel="stylesheet" href="{{asset('global/vendor/alertify/alertify.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/notie/notie.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/advanced/alertify.css')}}">

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <style>
        .msg {
            color: black;
        }

    </style>
    <!--[if lt IE 9]>
    <script src="{{asset('global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="{{asset('global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="{{asset('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
        Breakpoints();

    </script>
</head>

<body class="animsition page-register layout-full page-dark">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Page -->
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
        <div class="brand">
            <img class="brand-img" src="{{asset('assets//images/logo.png')}}" alt="...">
            <h2 class="brand-text">Greenwich Mangazine</h2>
        </div>
        <p>Sign up to find interesting thing</p>
        <form autocomplete="off">
            <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="text" class="form-control empty" id="firstName" required>
                <label class="floating-label" for="firstName">First Name</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial" required>
                <input type="text" class="form-control empty" id="lastName">
                <label class="floating-label" for="lastName">Last Name</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial" required>
                <input type="text" class="form-control empty" id="userName">
                <label class="floating-label" for="userName">UserName</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial" required>
                <input type="email" class="form-control empty" id="email">
                <label class="floating-label" for="email">Email</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="password" class="form-control empty" id="inputPassword" name="password" required>
                <label class="floating-label" for="inputPassword">Password</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="password" class="form-control empty" id="inputPasswordCheck" name="passwordCheck" required>
                <label class="floating-label" for="inputPasswordCheck">Retype Password</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
                <select class="form-control empty" id="faculty">
                    <option value=""></option>

                </select>
                <label class="floating-label" for="faculty">Faculty</label>
            </div>
            <button type="button" class="btn btn-primary btn-block" id="registerBtn" onclick="LUL()">Register</button>
        </form>
        <footer class="page-copyright page-copyright-inverse">
            <p>WEBSITE BY Creation Studio</p>
            <p>© 2018. All RIGHT RESERVED.</p>
            <div class="social">
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-twitter" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-facebook" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-google-plus" aria-hidden="true"></i>
                </a>
            </div>
        </footer>
    </div>
</div>
<!-- End Page -->
<!-- Core  -->
<script src="{{asset('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script src="{{asset('global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="{{asset('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.2/waves.min.js"></script>

<!-- Plugins -->
<script src="{{asset('global/vendor/switchery/switchery.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>
<script src="{{asset('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('global/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>

<!-- Scripts -->
<script src="{{asset('global/js/Component.js')}}"></script>
<script src="{{asset('global/js/Plugin.js')}}"></script>
<script src="{{asset('global/js/Base.js')}}"></script>
<script src="{{asset('global/js/Config.js')}}"></script>

<script src="{{asset('assets/js/Section/Menubar.js')}}"></script>
<script src="{{asset('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{asset('assets/js/Section/PageAside.js')}}"></script>
<script src="{{asset('assets/js/Plugin/menu.js')}}"></script>
<!-- alert js -->
<script src="{{asset('global/vendor/alertify/alertify.js')}}"></script>
<script src="{{asset('global/vendor/notie/notie.js')}}../"></script>
<!-- Config -->
<script src="{{asset('global/js/config/colors.js')}}"></script>
<script src="{{asset('assets/js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '{{asset('')}}assets');

</script>

<!-- Page -->
<script src="{{asset('assets/js/Site.js')}}"></script>
<script src="{{asset('global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{asset('global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{asset('global/js/Plugin/switchery.js')}}"></script>
<script src="{{asset('global/js/Plugin/jquery-placeholder.js')}}"></script>
<script src="{{asset('global/js/Plugin/animate-list.js')}}"></script>
<script src="{{asset('global/js/Plugin/material.js')}}"></script>
<script src="{{asset('styling/general.js')}}"></script>
<!-- page alert -->
<script src="{{asset('global/js/Plugin/alertify.js')}}"></script>
<script src="{{asset('global/js/Plugin/notie-js.js')}}"></script>

<script>
    (function (document, window, $) {
        'use strict';

        var Site = window.Site;
        $(document).ready(function () {
            Site.run();
            loadcate();
        });
    })(document, window, jQuery);

    function LUL() {

        var userName = $("#userName").val();
        var inputPassword = $("#inputPassword").val();
        var firstName = $("#firstName").val();
        var lastName = $("#lastName").val();
        var email = $("#email").val();
        var role = 4;
        var faculty = $("#faculty").val();
        //alert(faculty);
        //return false;
        if (userName == "") {
            alert("Please input the username!");
            return false;
        } else if (userName.length > 15) {
            alert("Username very long input again");
            return false
        }
        if (inputPassword == "") {
            alert("Please input the Password!");
            return false;
        }
        if (faculty == "") {
            alert("Please choose faculty!");
            return false;
        }
        if (firstName == "") {
            alert("Please input the Fist Name");
            return false;
        } else if (firstName.length > 15) {
            alert("first Name very long input again");
            return false
        }
        if (lastName == "") {
            alert("Please input the Last Name");
            return false;
        } else if (lastName.length > 15) {
            alert("Last Name very long input again");
            return false
        }
        if (email == "") {
            alert("Please input the email");
            return false;
        }
        var data_str = {
            lastName: lastName,
            firstName: firstName,
            email: email,
            username: userName,
            password: inputPassword,
            role: role,
            userFaculty: faculty
        };
        var host_api = "http://128.199.231.68/users/register";
        //console.log(data_str);
        $.ajax({
            url: host_api,
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify(data_str),
            async: false,
            success: function (result) {
                console.log(result);
                if (result.code == 201) {
                    alert(result.msg + "! Username của bạn là " + userName);
                    window.location.href = "../login/login.html";
                }
                if (result.code == 512) {
                    alert(result.msg);
                }

            },
            error(jqXHR) {
                console.log(jqXHR);

                alert(jqXHR.msg);
            }
        });
    };

    function loadcate() {
        var host_api = "http://128.199.231.68/faculties/getAllFaculty";

        $.ajax({
            url: host_api,
            method: "GET",
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
            },
            success: function (result) {
                var arrdetail = JSON.parse(JSON.stringify(result));
                console.log(arrdetail.data);
                if (arrdetail.data.length > 0) {
                    arrdetail.data.forEach(item => {

                        $("#faculty").append("<option value=" + item._id + ">" + item.facultyName + "</option>");

                    });

                } else {
                    alert("Không có kết quả !");
                }
            },
            error(jqXHR) {
                //
            }
        });
    }
</script>
</body>

</html>
