<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    <title>Closure date Management</title>
    <link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
    <!-- Stylesheets -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

    <link rel="stylesheet" href="{{asset('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">
    <!-- General Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">
    <!-- My Plugins -->
    <link rel="stylesheet" href="{{asset('global/vendor/datatables.net-bs4/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet"
          href="{{asset('global/vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css')}}">
    <link rel="stylesheet"
          href="{{asset('global/vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css')}}">
    <link rel="stylesheet"
          href="{{asset('global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/tables/datatable.css')}}">
    <link rel="stylesheet" href="{{asset('styling/manage-account/custombox-4.0.3/package/dist/custombox.min.css')}}">
    <!-- alert css -->
    <link rel="stylesheet" href="{{asset('global/vendor/alertify/alertify.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/notie/notie.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/advanced/alertify.css')}}">
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!-- datepicker -->
    <link rel="stylesheet" href="{{asset('styling/datepicker/css/bootstrap-datetimepicker.min.css')}}">

    <!--[if lt IE 9]>
    <script src="{{asset('global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="{{asset('global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="{{asset('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
        Breakpoints();

    </script>
</head>

<body class="animsition dashboard">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
@include('layouts.header')

<div class="modal fade modal-3d-sign" id="roleform" aria-hidden="true" aria-labelledby="exampleModalTitle"
     role="dialog" tabindex="1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Set Closure Date</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <!-- Panel Floating Labels -->
                    <form autocomplete="off">
                        <div class="form-group form-material floating" data-plugin="formMaterial">
                            <label class="form-control-label" for="newcreated_At">Start date</label>
                            <input type="text" class="form-control" id="newcreated_At" />
                        </div>
                        <div class="form-group form-material floating" data-plugin="formMaterial">
                            <label class="form-control-label" for="newexpired_At">Expiry Date of Submitting</label>
                            <input type="text" class="form-control" id="newexpired_At" />
                        </div>
                        <div class="form-group form-material floating" data-plugin="formMaterial">
                            <label class="form-control-label" for="newexpired_Edit">Expiry Date of Editing</label>
                            <input type="text" class="form-control" id="newexpired_Edit" />
                        </div>
                        <div class="form-group form-material floating" data-plugin="formMaterial">
                            <label class="form-control-label" for="newuser">User</label>
                            <input type="text" class="form-control" id="newuser" readonly />
                        </div>
                        <input type="hidden" class="form-control" id="closureid" readonly />

                        <div class="form-group form-material floating" data-plugin="formMaterial">
                            <label class="form-control-label" for="newstatus">Status</label>
                            <select class="form-control" id="newstatus">
                                <option value=""></option>
                                <option value="0">Deactivate</option>

                                <option value="1">Activate</option>
                            </select>

                        </div>
                    </form>
                    <!-- End Panel Floating Labels -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal" id="closesave">Close</button>
                <button type="button" class="btn btn-primary" onclick="savechange()">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Closure Date</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="homepage">Home</a></li>
            <li class="breadcrumb-item active">Closure Date</li>
        </ol>
        <div class="page-header-actions">
            <a class="btn btn-sm btn-primary btn-round" href="http://morrisjs.github.com/morris.js" target="_blank">
                <i class="icon md-link" aria-hidden="true"></i>
                <span class="hidden-sm-down">Official Website</span>
            </a>
        </div>
    </div>
    <!--table-->
    <!-- Panel Table Add Row -->
    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">
                    <button class="btn btn-info" data-target="#createNewClosure" data-toggle="modal" type="button">
                        <span><i class='icon md-collection-plus' aria-hidden='true'></i>Add New Closure Date</span>
                    </button>
                    <button class="btn btn-success" type="button" onclick="downloadZip()">
                        <span><i class='icon md-download' aria-hidden='true'></i>Download Articles in current closure date</span>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade modal-3d-sign" id="createNewClosure" aria-hidden="true"
                         aria-labelledby="exampleModalTitle" role="dialog" tabindex="-1">
                        <div class="modal-dialog modal-simple">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title">Set Closure Date</h4>
                                </div>

                                <div class="modal-body">
                                    <div class="col-md-12">
                                        <!-- Panel Floating Labels -->
                                        <form autocomplete="off">
                                            <div class="form-group form-material floating" data-plugin="formMaterial">
                                                <label class="form-control-label">Start Date</label>
                                                <input type="datetime" class="form-control" id="created_At" />
                                            </div>
                                            <div class="form-group form-material floating" data-plugin="formMaterial">
                                                <label class="form-control-label">Expiry Date of Submitting</label>
                                                <input type="datetime" class="form-control" id="expired_At" />
                                            </div>
                                            <div class="form-group form-material floating" data-plugin="formMaterial">
                                                <label class="form-control-label">Expiry Date of Editing</label>
                                                <input type="datetime" class="form-control" id="expired_Edit" />

                                            </div>

                                        </form>
                                        <!-- End Panel Floating Labels -->
                                    </div>
                                </div>


                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-pure" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" onclick="addnew()">Add new Closure Date</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </h3>
            </header>
            <div class="panel-body">
                <table class="table table-hover dataTable w-full" id="exampleTableSearch">
                    <thead>
                    <tr>
                        <th>Status</th>
                        <th>Start Date</th>
                        <th>Expiry Date of Submitting</th>
                        <th>Expiry Date of Editing</th>
                        <th>Created By</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- End Panel Table Add Row -->
<!--End Table-->
</div>
<!-- End Page -->
<!-- Footer -->
<!-- <footer class="site-footer">
  <div class="site-footer-legal">© 2018 <a
      href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
  <div class="site-footer-right">
    Crafted with <i class="red-600 icon md-favorite"></i> by <a
      href="https://themeforest.net/user/creation-studio">Creation Studio</a>
  </div>s
</footer> -->
<!-- Core  -->
<script src="{{asset('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script src="{{asset('global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="{{asset('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.2/waves.min.js"></script>

<!-- Plugins -->
<script src="{{asset('global/vendor/switchery/switchery.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>
<script src="{{asset('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net/jquery.dataTables.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-scroller/dataTables.scroller.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-responsive/dataTables.responsive.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-buttons/dataTables.buttons.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-buttons/buttons.html5.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-buttons/buttons.flash.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-buttons/buttons.print.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-buttons/buttons.colVis.js')}}"></script>
<script src="{{asset('global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js')}}"></script>
<script src="{{asset('global/vendor/asrange/jquery-asRange.min.js')}}"></script>
<script src="{{asset('global/vendor/bootbox/bootbox.js')}}"></script>
<!-- Scripts -->
<script src="{{asset('global/js/Component.js')}}"></script>
<script src="{{asset('global/js/Plugin.js')}}"></script>
<script src="{{asset('global/js/Base.js')}}"></script>
<script src="{{asset('global/js/Config.js')}}"></script>
<script src="{{asset('assets/js/Section/Menubar.js')}}"></script>
<script src="{{asset('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{asset('assets/js/Section/PageAside.js')}}"></script>
<script src="{{asset('assets/js/Plugin/menu.js')}}"></script>
<!-- alert js -->
<script src="{{asset('global/vendor/alertify/alertify.js')}}"></script>
<script src="{{asset('global/vendor/notie/notie.js')}}"></script>
<!-- Config -->
<script src="{{asset('global/js/config/colors.js')}}"></script>
<script src="{{asset('assets/js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '{{asset('assets')}}');

</script>
<!-- Page -->
<script src="{{asset('assets/js/Site.js')}}"></script>
<script src="{{asset('global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{asset('global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{asset('global/js/Plugin/switchery.js')}}"></script>
<script src="{{asset('global/js/Plugin/datatables.js')}}"></script>
<script src="{{asset('assets/examples/js/tables/datatable.js')}}"></script>
<script src="{{asset('assets/examples/js/uikit/icon.js')}}"></script>
<script src="{{asset('styling/general.js')}}"></script>
<script src="{{asset('js/closure-date.js')}}"></script>
<script src="{{asset('js/header.js')}}"></script>

<script src="{{asset('styling/datepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('styling/moment-with-locales.js')}}"></script>
<script src="{{asset('js/logout.js')}}"></script>
<!-- page alert -->
<script src="{{asset('global/js/Plugin/alertify.js')}}"></script>
<script src="{{asset('global/js/Plugin/notie-js.js')}}"></script>

<script>
    $(document).ready(function () {

        $("#created_At").datetimepicker({
            "clearBtn": true,
            format: "MM dd yyyy, hh:mm:ss p",
            autoclose: true,
            maxView: 4,
            isRTL: false,

            language: 'vi'

        });
        $("#expired_At").datetimepicker({
            "clearBtn": true,
            format: "MM dd yyyy, hh:mm:ss p",
            autoclose: true,
            maxView: 4,
            isRTL: false,

            language: 'vi'

        });
        $("#expired_Edit").datetimepicker({
            "clearBtn": true,
            format: "MM dd yyyy, hh:mm:ss p",
            autoclose: true,
            isRTL: false,

            maxView: 4,
            language: 'vi'

        });

        $("#newcreated_At").datetimepicker({
            "clearBtn": true,
            format: "MM dd yyyy, hh:mm:ss p",
            autoclose: true,
            maxView: 4,
            isRTL: false,

            language: 'vi'

        });
        $("#newexpired_At").datetimepicker({
            "clearBtn": true,
            format: "MM dd yyyy, hh:mm:ss p",
            autoclose: true,
            maxView: 4,
            isRTL: false,

            language: 'vi'

        });
        $("#newexpired_Edit").datetimepicker({
            "clearBtn": true,
            format: "MM dd yyyy, hh:mm:ss p",
            autoclose: true,
            isRTL: false,

            maxView: 4,
            language: 'vi'

        });
    })

</script>
</body>
</html>
