<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Statistics</title>

    <link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">

    <!-- Plugins -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css"> -->
    <link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/morris/morris.css')}}">
    <!-- dropdown css -->
    <link rel="stylesheet" href="{{asset('global/vendor/bootstrap-select/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/bootstrap-markdown/bootstrap-markdown.css')}}">
    <!-- alert css -->
    <link rel="stylesheet" href="{{asset('global/vendor/alertify/alertify.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/notie/notie.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/advanced/alertify.css')}}">
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!--[if lt IE 9]>
    <script src="{{asset('global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{asset('global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="{{asset('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
        Breakpoints();

    </script>
</head>

<body>
@include('layouts.header')
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Statistics</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="homepage">Home</a></li>
            <li class="breadcrumb-item active">Statistics</li>
        </ol>
        <div class="page-header-actions">
            <a class="btn btn-sm btn-primary btn-round" href="http://morrisjs.github.com/morris.js" target="_blank">
                <i class="icon md-link" aria-hidden="true"></i>
                <span class="hidden-sm-down">Official Website</span>
            </a>
        </div>
    </div>


    <div class="page-content container-fluid">
        <div class="panel">
            <div class="panel-body" id="lineChart">
                <div class="row row-lg">
                    <div class="col-lg-12">
                        <div class="example-wrap m-md-0">
                            <h4 class="example-title">Number of contributions within each Faculty for each academic year</h4>

                            <div class="example">
                                <div id="exampleMorrisLine"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-material floating" data-plugin="formMaterial">
            <label class="form-control-label" for="closureSelect">Closure Date</label>
            <select class="form-control" name="closureSelect" id="closureSelect" onchange="loadDonut()">
                <option value=""></option>
            </select>
        </div>
        <div class="panel">
            <div class="panel-body">
                <div class="row row-lg" id="donutChart">

                    <!-- <div class="col-lg-12">
                        <div class="example-wrap">
                          <h4 class="example-title">Percentage of contributions by each Faculty for any academic year. (From April 01 2019, 00:04:48 am To April 23 2019, 00:04:49 am)</h4>

                          <div class="example">
                            <div id="exampleMorrisDonut1"></div>
                          </div>
                        </div>
                      </div> -->
                    <!-- <div class="col-lg-12">

                        <div class="example-wrap">
                          <h4 class="example-title">Percentage of contributions by each Faculty for any academic year (From April 25 2019, 00:04:42 am To May 11 2019, 00:05:42 am)</h4>

                          <div class="example">
                            <div id="exampleMorrisDonut2"></div>
                          </div>
                        </div>
                      </div> -->

                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script src="{{asset('global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="{{asset('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.2/waves.min.js"></script>

<!-- Plugins -->
<script src="{{asset('global/vendor/switchery/switchery.js')}}"></script>
<script src="{{asset('global/vendor/intro-js/intro.js')}}"></script>
<script src="{{asset('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('global/vendor/raphael/raphael.min.js')}}"></script>

<script src="{{asset('global/vendor/morris/morris.min.js')}}"></script>
<script src="https://momentjs.com/downloads/moment.min.js"></script>

<!-- dropdown js -->
<script src="{{asset('global/vendor/bootstrap-markdown/bootstrap-markdown.js')}}"></script>
<script src="{{asset('global/vendor/marked/marked.js')}}"></script>
<script src="{{asset('global/vendor/to-markdown/to-markdown.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
<!-- alert js -->
<script src="{{asset('global/vendor/alertify/alertify.js')}}"></script>
<script src="{{asset('global/vendor/notie/notie.js')}}"></script>
<!-- Scripts -->
<script src="{{asset('global/js/Component.js')}}"></script>
<script src="{{asset('global/js/Plugin.js')}}"></script>
<script src="{{asset('global/js/Base.js')}}"></script>
<script src="{{asset('global/js/Config.js')}}"></script>

<script src="{{asset('assets/js/Section/Menubar.js')}}"></script>
<script src="{{asset('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{asset('assets/js/Section/PageAside.js')}}"></script>
<script src="{{asset('assets/js/Plugin/menu.js')}}"></script>

<!-- Config -->
<script src="{{asset('global/js/config/colors.js')}}"></script>
<script src="{{asset('assets/js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '{{asset('assets')}}');

</script>

<!-- Page -->
<script src="{{asset('assets/js/Site.js')}}"></script>
<script src="{{asset('global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{asset('global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{asset('global/js/Plugin/switchery.js')}}"></script>
<script src="{{asset('styling/general.js')}}"></script>
<script src="{{asset('js/statistics.js')}}"></script>
<script src="{{asset('assets/examples/js/charts/morris.js')}}"></script>
<script src="{{asset('js/header.js')}}"></script>
<script src="{{asset('js/logout.js')}}"></script>
<!-- page alert -->
<script src="{{asset('global/js/Plugin/alertify.js')}}"></script>
<script src="{{asset('global/js/Plugin/notie-js.js')}}"></script>


</body>

</html>
