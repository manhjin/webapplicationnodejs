<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Review Article</title>

    <link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">
    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">
    <!-- Page Plugins -->
    <link rel="stylesheet" href="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/dropify/dropify.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/summernote/summernote.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/project.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/bootstrap-select/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/bootstrap-markdown/bootstrap-markdown.css')}}">
    <!-- My Styling -->
    <link rel="stylesheet" href="{{asset('styling/article/article.css')}}">


    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('global/fonts/font-awesome/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>


    <!--[if lt IE 9]>
    <script src="{{asset('global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{asset('global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="{{asset('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
        Breakpoints();

    </script>
</head>

<body class="animsition page-project">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse" role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="../../assets/images/logo.png" title="Remark">
            <span class="navbar-brand-text hidden-xs-down"> Remark</span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search" data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
        </button>
    </div>

    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">
                <li class="nav-item hidden-float" id="toggleMenubar">
                    <a class="nav-link" data-toggle="menubar" href="#" role="button">
                        <i class="icon hamburger hamburger-arrow-left">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
                <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                    <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                        <span class="sr-only">Toggle fullscreen</span>
                    </a>
                </li>
                <li class="nav-item hidden-float">
                    <a class="nav-link icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                       role="button">
                        <span class="sr-only">Toggle Search</span>
                    </a>
                </li>

            </ul>
            <!-- End Navbar Toolbar -->

            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">

                <li class="nav-item dropdown">
                    <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                       data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="../../../global/portraits/5.jpg" alt="...">
                <i></i>
              </span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" onclick="profile()" role="menuitem"><i class="icon md-account"
                                                                                        aria-hidden="true"></i> Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" onclick="logout()" role="menuitem"><i class="icon md-power"
                                                                                       aria-hidden="true"></i> Logout</a>
                    </div>
                </li>
            </ul>
            <ul class="nav navbar-toolbar navbar-right" id="userAction">
                <li class="nav-item dropdown" role="presentation">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button">
                        <span><i class="icon md-collection-folder-image" aria-hidden="true"></i> Faculty</span>
                    </a>
                    <div class="dropdown-menu" role="menu" id="listFaculty">

                    </div>
                </li>
            </ul>
            <!-- End Navbar Toolbar Right -->

            <div class="navbar-brand navbar-brand-center">
                <a href="../homepage/homepage.html">
                    <img class="navbar-brand-logo navbar-brand-logo-normal" src="../../assets/images/logo.png" title="Remark">
                    <img class="navbar-brand-logo navbar-brand-logo-special" src="../../assets/images/logo-colored.png"
                         title="Remark">
                </a>
            </div>
        </div>
        <!-- End Navbar Collapse -->

        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
                <div class="form-group">
                    <div class="input-search">
                        <i class="input-search-icon md-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="site-search" placeholder="Search...">
                        <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                                data-toggle="collapse" aria-label="Close"></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Site Navbar Seach -->
    </div>
</nav>
<div class="site-menubar">
    <div class="site-menubar-header">
        <div class="cover overlay">
            <img class="cover-image" src="../../assets//examples/images/dashboard-header.jpg" alt="...">
            <div class="overlay-panel vertical-align overlay-background">
                <div class="vertical-align-middle">
                    <a class="avatar avatar-lg" href="javascript:void(0)">
                        <img src="../../../global/portraits/1.jpg" alt="">
                    </a>
                    <div class="site-menubar-info">
                        <h5 class="site-menubar-user" id="lbluser"></h5>
                        <p class="site-menubar-email" id="lblemail"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu" data-plugin="menu" id="left-menu">
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-3d-sign" id="Edit" aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
     tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h2 class="modal-title">Edit</h2>
            </div>
              <div class="modal-body">
          <div class="panel-body container-fluid">
            <form autocomplete="off" id="uploadForm" enctype="multipart/form-data"
                  action="http://128.199.231.68/articles/updateArticle" method="post">
              <div class="form-group form-material" data-plugin="formMaterial">
                <label class="form-control-label" for="inputTitle">Title</label>
                <input type="text" class="form-control" id="inputTitle" name="articleName" placeholder="Title" />
              </div>
              <input type="text" class="form-control" name="person" id="person" readonly />
              <input type="text" class="form-control" name="postemail" id="postemail" readonly hidden />
              <input type="text" class="form-control" name="closure_id" id="closure_id" readonly hidden />

              <div class="form-group form-material" data-plugin="formMaterial">
                <label class="form-control-label" for="inputTitle">Category</label>
                <input type="text" class="form-control" name="articleCategory" id="articleCategory" readonly />
                <input type="hidden" class="form-control" name="articleId" id="articleId" readonly />

              </div>
              <div class="form-group form-material" data-plugin="formMaterial">
                <label class="form-control-label">Upload articles</label>
                <input type="text" class="form-control" placeholder="Browse.." readonly="" />
                <input name="files" id="docxFile" type="file" multiple="" />
              </div>
              <div class="form-group form-material" data-plugin="formMaterial" hidden>
                <label class="form-control-label" for="articleFaculty">Content</label>
                <input class="form-control" id="articleFaculty" name="articleFaculty">
              </div>
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <div class="example-wrap">
                  <h4 class="example-title">Upload picture</h4>
                  <div class="example">
                    <input type="file" name="files" id="inputImage" data-plugin="dropify" data-default-file=""
                           multiple />
                  </div>
                </div>
              </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button class="btn btn-outline btn-default" type="submit" id="submit-btn" onsubmit="return false">Save changes</button>
          </div>
          </form>
        </div>
        </div>
    </div>
</div>
<div class="modal fade modal-3d-sign" id="changeStatus" aria-hidden="true" aria-labelledby="exampleModalTitle"
     role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h2 class="modal-title">Change Status</h2>
            </div>
            <div class="modal-body">
                <div class="panel-body container-fluid">
                    <div class="form-group form-material" data-plugin="formMaterial">
                        <label for="articleStatus" class="form-control-label">Status</label>
                        <select class="form-control" data-plugin="selectpicker" id="articleStatus">
                            <!-- append option in js -->
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-outline btn-default" type="button" data-dismiss="modal"
                            onclick="actChangeStatus()">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page">
    <div class="page-content">
        <div class="row">
            <div class="col-xxl-12 col-xl-9 col-lg-12" style="margin: 0 auto">
                <div class="card">
                    <div class="card-block" id="article">
                    </div>
                    <div class="card-block project-comments">
                        <h4 class="m-0">Comments</h4>
                        <div class="comments">
                            <form autocomplete="off" id="cmtForm" enctype="multipart/form-data"
                                  action="http://128.199.231.68/articles/addComment" method="post">
                                <!--<input type="text" id="commentId" name="commentId"/>-->
                                <input type="text" id="artId" name="articleId"  hidden/>
                                <input type="text" id="comment_user" name="comment_user" hidden />
                                <div class="comment media">
                                    <div class="pr-15">
                                        <a href="#" class="avatar avatar-sm">
                                            <img src="{{asset('')}}global/portraits/1.jpg" alt="...">
                                        </a>
                                    </div>
                                    <div class="comment-body media-body">
                                        <div class="comment-title">
                                            <div class="comment-meta float-right">
                                                <span></span>
                                            </div>
                                            <a href="javascript:void(0)" class="comment-author" id="current_user">
                                            </a>
                                        </div>
                                        <input type="text" class="form-control" id="status" name="status" value="1" hidden/>

                                        <div class="comment-content" style="display:grid;grid-template-columns:95% 3% 2%">
                                            <input type="text" class="form-control" id="comment_content" name="comment_content" />
                                            <br>
                                            <img src='{{asset('')}}assets/images/image.png' style="height:36px;cursor:pointer"
                                                 onclick="chooseimage()">
                                        </div>
                                        <button type="submit" id="submitBtn" hidden></button>
                                        <div class="comment-actions text-left">
                                            <div class="">
                                                <h4 class="example-title">Upload picture</h4>
                                                <div class="example">
                                                    <input type="file" name="files" id="image" data-plugin="dropify" data-default-file=""
                                                           multiple />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div id="comment_section">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-3 col-xl-4 col-lg-12">
                <div class="card">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">© 2018 <a
                href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
    <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a
                href="https://themeforest.net/user/creation-studio">Creation Studio</a>
    </div>
</footer>
<!-- Core  -->
<script src="{{asset('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
<script src="{{asset('global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="{{asset('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.2/waves.min.js"></script>

<!-- Plugins -->
<script src="{{asset('global/vendor/switchery/switchery.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>
<script src="{{asset('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('global/vendor/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{asset('global/vendor/asprogress/jquery-asProgress.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-tmpl/tmpl.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-load-image/load-image.all.min.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-process.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-image.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-audio.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-video.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-validate.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-ui.js')}}"></script>
<script src="{{asset('global/vendor/summernote/summernote.min.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
<script src="{{asset('global/vendor/dropify/dropify.min.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap-markdown/bootstrap-markdown.j')}}s"></script>
<script src="{{asset('global/vendor/marked/marked.js')}}"></script>
<script src="{{asset('global/vendor/to-markdown/to-markdown.js')}}"></script>
<!-- Scripts -->

<!-- Scripts -->
<script src="{{asset('global/js/Component.js')}}"></script>
<script src="{{asset('global/js/Plugin.js')}}"></script>
<script src="{{asset('global/js/Base.js')}}"></script>
<script src="{{asset('global/js/Config.js')}}"></script>

<script src="{{asset('assets/js/Section/Menubar.js')}}"></script>
<script src="{{asset('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{asset('assets/js/Section/PageAside.js')}}"></script>
<script src="{{asset('assets/js/Plugin/menu.js')}}"></script>

<!-- Config -->
<script src="{{asset('global/js/config/colors.js')}}"></script>
<script src="{{asset('assets/js/config/tour.j')}}s"></script>
<script>
    Config.set('assets', '{{asset('')}}assets');

</script>

<!-- Page -->
<script src="{{asset('assets/js/Site.js')}}"></script>
<script src="{{asset('global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{asset('global/js/Plugin/slidepanel.j')}}s"></script>
<script src="{{asset('global/js/Plugin/switchery.js')}}"></script>
<script src="{{asset('global/js/Plugin/asprogress.js')}}"></script>
<script src="{{asset('global/js/Plugin/summernote.js')}}"></script>

<script src="{{asset('assets/examples/js/pages/project.js')}}"></script>
<script src="{{asset('js/header.js')}}"></script>
<script src="{{asset('js/review-article.js')}}"></script>
<script src="{{asset('js/logout.js')}}"></script>

</body>

</html>
