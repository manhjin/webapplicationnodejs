<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Article</title>
    
    <link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">

    <!-- General Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">
    <!-- My Plugins -->
    <link rel="stylesheet" href="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/dropify/dropify.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/summernote/summernote.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/project.css')}}">
    <!-- My Styling -->
    <link rel="stylesheet" href="{{asset('styling/article/article.css')}}">


    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('global/fonts/font-awesome/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>


    <!--[if lt IE 9]>
    <script src="{{asset('global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{asset('global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="{{asset('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
        Breakpoints();

    </script>
</head>

<body class="animsition page-project">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

@include('layouts.header')
<div class="page">
    <div class="page-content">
        <div class="row">
            <div class="col-xxl-12 col-xl-9 col-lg-12" style="margin: 0 auto">
                <div class="card">
                    <div class="card-block" id="article">
                    </div>
                    <div class="card-block project-comments">
                        <h4 class="m-0">Comments</h4>
                        <div class="comments">
                            <form autocomplete="off" id="uploadForm" enctype="multipart/form-data"
                                  action="http://128.199.231.68/articles/addComment" method="post">
                                <!--<input type="text" id="commentId" name="commentId"/>-->
                                <input type="text" id="articleId" name="articleId" hidden />
                                <input type="text" class="form-control" id="status" name="status" value="0" hidden />

                                <input type="text" id="comment_user" name="comment_user" hidden />
                                <div class="comment media">
                                    <div class="pr-15">
                                        <a href="#" class="avatar avatar-sm">
                                            <img src="{{asset('')}}../global/portraits/1.jpg" alt="...">
                                        </a>
                                    </div>
                                    <div class="comment-body media-body">
                                        <div class="comment-title">
                                            <div class="comment-meta float-right">
                                                <span></span>
                                            </div>
                                            <a href="javascript:void(0)" class="comment-author" id="current_user">
                                            </a>
                                        </div>
                                        <div class="comment-content" style="display:grid;grid-template-columns:95% 3% 2%">
                                            <input type="text" class="form-control" id="comment_content" name="comment_content" />
                                            <br>
                                            <img src='{{asset('')}}assets/images/image.png' style="height:36px;cursor:pointer"
                                                 onclick="chooseimage()">
                                        </div>
                                        <button type="submit" id="submitBtn" hidden></button>
                                        <div class="comment-actions text-left">
                                            <div class="">
                                                <h4 class="example-title">Upload picture</h4>
                                                <div class="example">
                                                    <input type="file" name="files" id="image" data-plugin="dropify" data-default-file=""
                                                           multiple />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div id="comment_section">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-3 col-xl-4 col-lg-12">
                <div class="card">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">
        © 2018 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a>
    </div>
    <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a href="https://themeforest.net/user/creation-studio">Creation Studio</a>
    </div>
</footer>
<!-- Core  -->
<script src="{{asset('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
<script src="{{asset('global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="{{asset('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.2/waves.min.js"></script>
<!-- General Plugins -->
<script src="{{asset('global/vendor/switchery/switchery.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>
<script src="{{asset('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<!-- My Plugins -->
<script src="{{asset('global/vendor/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{asset('global/vendor/asprogress/jquery-asProgress.js')}}../"></script>
<script src="{{asset('global/vendor/blueimp-tmpl/tmpl.js')}}../"></script>
<script src="{{asset('global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-load-image/load-image.all.min.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload.js')}}../"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-process.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-image.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-audio.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-video.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-validate.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-ui.js')}}"></script>
<script src="{{asset('global/vendor/summernote/summernote.min.js')}}../"></script>
<!-- Scripts -->
<script src="{{asset('global/js/Component.js')}}"></script>
<script src="{{asset('global/js/Plugin.js')}}"></script>
<script src="{{asset('global/js/Base.js')}}"></script>
<script src="{{asset('global/js/Config.js')}}"></script>

<script src="{{asset('assets/js/Section/Menubar.js')}}"></script>
<script src="{{asset('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{asset('assets/js/Section/PageAside.js')}}"></script>
<script src="{{asset('assets/js/Plugin/menu.js')}}"></script>
<script src="{{asset('global/vendor/dropify/dropify.min.js')}}"></script>
<!-- Config -->
<script src="{{asset('global/js/config/colors.js')}}"></script>
<script src="{{asset('assets/js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '{{asset('assets')}}');

</script>

<!-- Page -->
<script src="{{asset('assets/js/Site.js')}}"></script>
<script src="{{asset('global/js/Plugin/asscrollable.js')}}../"></script>
<script src="{{asset('global/js/Plugin/slidepanel.js')}}../"></script>
<script src="{{asset('global/js/Plugin/switchery.js')}}../"></script>
<script src="{{asset('global/js/Plugin/asprogress.js')}}../"></script>
<script src="{{asset('global/js/Plugin/summernote.js')}}../"></script>
<script src="{{asset('assets/examples/js/forms/uploads.js')}}"></script>
<script src="{{asset('assets/examples/js/pages/project.js')}}"></script>
<script src="{{asset('js/header.js')}}"></script>
<script src="{{asset('js/article.js')}}"></script>
<script src="{{asset('js/logout.js')}}"></script>
</body>

</html>
