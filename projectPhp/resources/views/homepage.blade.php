{{--<!DOCTYPE html>--}}
{{--<html class="no-js css-menubar" lang="en">--}}

{{--<head>--}}
{{--<meta charset="utf-8">--}}
{{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">--}}
{{--<meta name="description" content="bootstrap material admin template">--}}
{{--<meta name="author" content="">--}}

{{--<title>Homepage</title>--}}

{{--<link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">--}}
{{--<link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">--}}

{{--<!-- Stylesheets -->--}}
{{--<link rel="stylesheet" href="{{asset('global/css/bootstrap.min.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.min.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">--}}
{{--<!-- My Styling -->--}}
{{--<link rel="stylesheet" href="{{asset('styling/homepage/homepage.css')}}">--}}

{{--<!-- Plugins -->--}}
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css">--}}
{{--<link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('global/vendor/switchery/switchery.css')}}">--}}
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">--}}
{{--<link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('global/vendor/flag-icon-css/flag-icon.css')}}">--}}
{{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">--}}
{{--<link rel="stylesheet" href="{{asset('assets/examples/css/structure/navbars.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('assets/examples/css/uikit/badges.css')}}">--}}


{{--<!-- Fonts -->--}}
{{--<link rel="stylesheet" href="{{asset('global/fonts/material-design/material-design.min.css')}}">--}}
{{--<link rel="stylesheet" href="{{asset('global/fonts/brand-icons/brand-icons.min.css')}}">--}}
{{--<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>--}}
{{--<script src="{{asset('global/vendor/breakpoints/breakpoints.js')}}"></script>--}}
{{--<script>--}}
{{--Breakpoints();--}}
{{--</script>--}}
{{--</head>--}}


@extends('layouts.master')
<link rel="stylesheet" href="{{asset('assets/examples/css/structure/navbars.css')}}">
<link rel="stylesheet" href="{{asset('assets/examples/css/uikit/badges.css')}}">
@section('content')
    <div class="content">
        <div class="trending">
            <div class="trending_text">
                Trending Today
            </div>
            <div class="trending_item1">
                <div class="trending_item_text">Internet Of Things</div>
            </div>
            <div class="trending_item2">
                <div class="trending_item_text">Artificial Intelligence</div>
            </div>
            <div class="trending_item3">
                <div class="trending_item_text">Movie</div>
            </div>
            <div class="trending_item4">
                <div class="trending_item_text">Politic</div>
            </div>
        </div>
        <div class="panel_text">
            Popular posts
        </div>
        <div class="left_content">
        </div>
        <div class="right_content">
            <div class="itemTopic">
                <div style="font-size: 16px;
          font-weight: 500;
          line-height: 20px;
          color: rgb(28, 28, 28);
          margin: 12px 0 12px 12px;">Original Content Categories
                </div>
                <div class="listTopic">
                    <!-- <a class="badge badge-outline badge-primary" href="#">.NET</a>
                    <span class="badge badge-outline badge-primary">Design</span>
                    <span class="badge badge-outline badge-primary">Economic</span>
                    <span class="badge badge-outline badge-primary">2D</span>
                    <span class="badge badge-outline badge-primary">3D</span>
                    <span class="badge badge-outline badge-primary">Business</span>
                    <span class="badge badge-outline badge-primary">NodeJS</span> -->
                </div>
            </div>
        </div>
    </div>
@endsection

