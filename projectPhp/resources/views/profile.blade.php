<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Profile v3 | Remark Material Admin Template</title>

    <link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">
    <!-- My Styling  -->
    <link rel="stylesheet" href="{{asset('styling/profile/profile.css')}}">

    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/plyr/plyr.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/magnific-popup/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile_v3.css')}}">


    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!--[if lt IE 9]>
    <script src="{{asset('global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{asset('global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="{{asset('global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
        Breakpoints();

    </script>
</head>

<body class="animsition page-profile-v3">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
@include('layouts.header')


<div class="page">
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-8" id="content">
                <div class="card">
                    <img class="card-img-top w-full" src="{{asset('global/photos/placeholder.png')}}">
                    <div class="card-block wall-person-info">
                        <a class="avatar bg-white img-bordered person-avatar">
                            <img src="{{asset('global/portraits/2.jpg')}}">
                        </a>
                        <h2 class="person-name">
                            <a href="#">Jonathan Smith</a>
                        </h2>
                        <p class="card-text">
                            <a href="#" class="blue-grey-400">jsmith@flatlab.com</a>
                        </p>
                    </div>
                </div>
                <div class="card card-shadow wall-posting">
                    <div class="card-block p-0">
                        <div class="form-group mb-0">
                            <input class="form-control form-control-lg" type="text" name="write"
                                   placeholder="Whats in your mind today?" />
                            <div class="p-10">
                                <button class="btn btn-pure btn-default icon md-image" type="button" name="button"></button>
                                <button class="btn btn-pure btn-default icon md-tv-play" type="button" name="button"></button>
                                <button class="btn btn-pure btn-default icon md-calendar" type="button" name="button"></button>
                                <button class="btn btn-pure btn-default icon md-map" type="button" name="button"></button>
                                <button class="btn btn-primary btn-round submit float-right mr-10">
                                    Post
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="card card-shadow">
                  <div class="card-block media clearfix p-25">
                    <div class="pr-20">
                      <a href="#" class="avatar avatar-lg">
                        <img class="img-fluid" src="{{asset('')}}global/portraits/6.jpg">
                      </a>
                    </div>
                    <div class="media-body text-middle">
                      <h4 class="mt-0 mb-5">
                        Mallinda Hollaway
                      </h4>
                      <small>Posted on 31st Aug 2017 at 07:00</small>
                    </div>
                  </div>
                  <div class="card-block px-25  pt-0">
                    <div class="panel-content">
                      <div class="itemContent">

                        <div class="itemTitle">
                          TIL - In 1836, a sewer worker accidentally discovered an old drain which ran directly into the Bank
                          of
                          England's gold vault. He wrote letters to the directors of the bank and requested a meeting inside
                          the
                          vault at an hour of their choosing - and popped out of the floor to greet them
                        </div>
                        <div class="itemDesc">
                          <img src="{{asset('')}}assets/images/item.jpg" alt="">
                        </div>

                      </div>
                      <ul class="wall-attrs clearfix p-0 m-0">
                        <li class="attrs-meta float-left">
                          <span>
                            <i class="icon md-chat"></i> 0
                          </span>
                          <span class="ml-10">
                            </if>
                            <i class="icon md-heart"></i> 0
                          </span>
                        </li>
                        <li class="float-right">
                          <a href="#" class="avatar avatar-sm" data-paticipator-id="p_1">
                            <img src="{{asset('')}}global/portraits/3.jpg">
                          </a>
                        </li>
                      </ul>
                    </div>

                  </div>
                  <div class="card-footer p-20">
                    <div class="wall-comment">
                      <a href="#" class="avatar avatar-md float-left">
                        <img src="{{asset('')}}global/portraits/3.jpg">
                      </a>
                      <div class="ml-60">
                        <a href="#">Stacey Hunt</a>
                        <small class="ml-10">30th July 2017</small>
                        <p class="mt-5">
                          Cillum sit aliquip irure minim. Cillum mollit aute consectetur non et ut ipsum.
                          Occaecat laboris eu voluptate ipsum commodo consequat laborum
                          laborum. Anim deserunt voluptate voluptate labore Lorem dolore
                          proident.
                        </p>
                      </div>
                      <div class="dropdown comment-operation">
                        <button type="button" class="btn btn-icon btn-round btn-default btn-sm" data-toggle="dropdown"
                          aria-expanded="false">
                          <i class="icon md-more-vertical"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Report</a>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Delete</a>
                        </div>
                      </div>
                    </div>
                    <div class="wall-comment">
                      <a href="#" class="avatar avatar-md float-left">
                        <img src="{{asset('')}}global/portraits/5.jpg">
                      </a>
                      <div class="ml-60">
                        <a href="#">Sam Anderson</a>
                        <small class="ml-10">3 mins ago...</small>
                        <p class="mt-5">
                          Aliqua nulla culpa nulla quis. Adipisicing et consequat in Lorem. Pariatur sunt
                          esse nostrud occaecat velit anim commodo. Magna nostrud nisi
                          id deserunt cupidatat mollit incididunt.
                        </p>
                      </div>
                      <div class="dropdown comment-operation">
                        <button type="button" class="btn btn-icon btn-round btn-default btn-sm" data-toggle="dropdown"
                          aria-expanded="false">
                          <i class="icon md-more-vertical"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Report</a>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Delete</a>
                        </div>
                      </div>
                    </div>
                    <form class="wall-comment-reply">
                      <input type="text" class="form-control" placeholder="Write Something...">
                      <div class="reply-operation">
                        <button class="btn btn-sm btn-primary reply-post" type="button">POST</button>
                        <button class="btn btn-sm btn-pure btn-default reply-cancel" type="button">CANCEL</button>
                      </div>
                    </form>
                  </div>
                </div> -->
                <!-- <div class="card card-shadow">
                  <div class="card-block media clearfix p-25">
                    <div class="pr-20">
                      <a href="#" class="avatar avatar-lg">
                        <img class="img-fluid" src="{{asset('')}}global/portraits/1.jpg">
                      </a>
                    </div>
                    <div class="media-body text-middle">
                      <h4 class="mt-0 mb-5">
                        Felix Harper
                      </h4>
                      <small>Posted on 29th Aug 2017 at 02:10</small>
                    </div>
                  </div>
                  <div class="card-block px-25  pt-0">
                    <p class="card-text mb-20">
                      Sint eiusmod aute cupidatat consectetur proident incididunt. Laborum Lorem magna
                      anim ut do consequat. Adipisicing et ad irure irure velit veniam.
                      Adipisicing nostrud et proident ea id officia nostrud elit irure.
                      Proident minim voluptate cillum deserunt. Consectetur esse cillum
                      amet eiusmod. Mollit exercitation proident cillum pariatur. Velit
                      duis duis exercitation enim commodo cupidatat dolore.
                    </p>
                    <ul class="wall-attrs clearfix p-0 m-0">
                      <li class="attrs-meta float-left">
                        <span>
                          <i class="icon md-chat"></i> 0
                        </span>
                        <span class="ml-10">
                          </if>
                          <i class="icon md-heart"></i> 0
                        </span>
                      </li>
                      <li class="float-right">
                        <a href="#" class="avatar avatar-sm" data-paticipator-id="p_1">
                          <img src="{{asset('')}}global/portraits/3.jpg">
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="card-footer p-20">
                    <form class="wall-comment-reply">
                      <input type="text" class="form-control" placeholder="Write Something...">
                      <div class="reply-operation">
                        <button class="btn btn-sm btn-primary reply-post" type="button">POST</button>
                        <button class="btn btn-sm btn-pure btn-default reply-cancel" type="button">CANCEL</button>
                      </div>
                    </form>
                  </div>
                </div> -->

            </div>
            <div class="col-lg-4">
                <div class="card card-block">
                    <h4 class="card-title">About me</h4>
                    <p class="card-text">
                        Do sint ex duis non ut in amet commodo. Laborum laborum reprehenderit proident
                        deserunt. Aute duis sunt adipisicing sint incididunt occaecat Lorem.
                        Elit deserunt aliquip duis irure.
                        <a href="#">Read more...</a>
                    </p>
                </div>
                <div class="card wall-recent-comments">
                    <div class="card-header card-header-transparent">
                        <h4 class="card-title">
                            Recent Comments
                        </h4>
                        <p class="card-text">
                            Cras congue nec lorem eget posuere
                        </p>
                    </div>
                    <div class="list-group">
                        <a class="list-group-item" data-recomment-id="c_1">
                            <div class="media">
                                <div class="pr-15">
                                    <div class="avatar avatar-md">
                                        <img class="img-fluid" src="{{asset('')}}global/portraits/6.jpg">
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h5 class="list-group-item-heading mt-0 mb-5">David Belle</h5>
                                    <p class="list-group-item-text">
                                        Deserunt eu commodo anim laboris sit excepteur....
                                    </p>
                                </div>
                            </div>
                        </a>
                        <a class="list-group-item" data-recomment-id="c_2">
                            <div class="media">
                                <div class="pr-15">
                                    <div class="avatar avatar-md">
                                        <img class="img-fluid" src="{{asset('')}}global/portraits/7.jpg">
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h5 class="list-group-item-heading mt-0 mb-5">Jonathan Morris</h5>
                                    <p class="list-group-item-text">
                                        Id cillum irure elit ullamco consequat velit deserunt adipisicing fugiat....
                                    </p>
                                </div>
                            </div>
                        </a>
                        <a class="list-group-item" data-recomment-id="c_3">
                            <div class="media">
                                <div class="pr-15">
                                    <div class="avatar avatar-md">
                                        <img class="img-fluid" src="{{asset('')}}global/portraits/4.jpg">
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h5 class="list-group-item-heading mt-0 mb-5">Fredric Mitchell Jr.</h5>
                                    <p class="list-group-item-text">
                                        Incididunt cillum occaecat dolor voluptate....
                                    </p>
                                </div>
                            </div>
                        </a>
                        <a class="list-group-item" data-recomment-id="c_4">
                            <div class="media">
                                <div class="pr-15">
                                    <div class="avatar avatar-md">
                                        <img class="img-fluid" src="{{asset('')}}global/portraits/7.jpg">
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h5 class="list-group-item-heading mt-0 mb-5">Jonathan Morris</h5>
                                    <p class="list-group-item-text">
                                        Commodo esse consequat irure mollit labore....
                                    </p>
                                </div>
                            </div>
                        </a>
                        <a class="list-group-item" data-recomment-id="c_5">
                            <div class="media">
                                <div class="pr-15">
                                    <div class="avatar avatar-md">
                                        <img class="img-fluid" src="{{asset('')}}global/portraits/4.jpg">
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h5 class="list-group-item-heading mt-0 mb-5">Fredric Mitchell Jr.</h5>
                                    <p class="list-group-item-text">
                                        Deserunt nulla consequat aliquip ad elit do eu consectetur....
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="comments-view card-footer card-footer-transparent">
                        <a href="#">View All</a>
                    </div>
                </div>
                <div class="card p-20">
                    <h4 class="card-title">
                        Contact Information
                    </h4>
                    <p class="card-text">
                        Fusce eget dolor id justo luctus commodo vel pharetra nisi. Donec velit libero
                    </p>
                    <div class="card-block p-0">
                        <p data-info-type="phone" class="mb-10 text-nowrap">
                            <i class="icon md-account mr-10"></i>
                            <span class="text-break">00971123456789
                  <span>
                        </p>
                        <p data-info-type="email" class="mb-10 text-nowrap">
                            <i class="icon md-email mr-10"></i>
                            <span class="text-break">malinda.h@gmail.com
                  <span>
                        </p>
                        <p data-info-type="fb" class="mb-10 text-nowrap">
                            <i class="icon bd-facebook mr-10"></i>
                            <span class="text-break">malinda.hollaway
                  <span>
                        </p>
                        <p data-info-type="twitter" class="mb-10 text-nowrap">
                            <i class="icon bd-twitter mr-10"></i>
                            <span class="text-break">@malinda (twitter.com/malinda)
                  <span>
                        </p>
                        <p data-info-type="address" class="mb-10 text-nowrap">
                            <i class="icon md-pin mr-10"></i>
                            <span class="text-break">44-46 Morningside Road,Edinburgh,Scotland
                  <span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">© 2018 <a
                href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
    <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a
                href="https://themeforest.net/user/creation-studio">Creation Studio</a>
    </div>
</footer>
<!-- Core  -->
<script src="{{asset('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script src="{{asset('global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="{{asset('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.2/waves.min.js"></script>

<!-- Plugins -->
<script src="{{asset('global/vendor/switchery/switchery.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>
<script src="{{asset('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('global/vendor/plyr/plyr.js')}}"></script>
<script src="{{asset('global/vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>

<!-- Scripts -->
<script src="{{asset('global/js/Component.js')}}"></script>
<script src="{{asset('global/js/Plugin.js')}}"></script>
<script src="{{asset('global/js/Base.js')}}"></script>
<script src="{{asset('global/js/Config.js')}}"></script>

<script src="{{asset('assets/js/Section/Menubar.js')}}"></script>
<script src="{{asset('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{asset('assets/js/Section/PageAside.js')}}"></script>
<script src="{{asset('assets/js/Plugin/menu.js')}}"></script>

<!-- Config -->
<script src="{{asset('global/js/config/colors.js')}}"></script>
<script src="{{asset('assets/js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '{{asset('')}}assets');

</script>

<!-- Page -->
<script src="{{asset('assets/js/Site.js')}}"></script>
<script src="{{asset('global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{asset('global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{asset('global/js/Plugin/switchery.js')}}"></script>
<script src="{{asset('global/js/Plugin/plyr.js')}}"></script>
<script src="{{asset('global/js/Plugin/magnific-popup.js')}}"></script>

<script src="{{asset('assets/examples/js/pages/profile_v3.js')}}"></script>
<!-- My JS -->
<script src="{{asset('js/header.js')}}"></script>
<script src="{{asset('js/profile.js')}}"></script>
<script src="{{asset('js/logout.js')}}"></script>
</body>

</html>
