<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse" role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="../../assets/images/logo.png" title="Remark">
            <span class="navbar-brand-text hidden-xs-down"> Remark</span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search" data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
        </button>
    </div>

    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">
                <li class="nav-item hidden-float" id="toggleMenubar">
                    <a class="nav-link" data-toggle="menubar" href="#" role="button">
                        <i class="icon hamburger hamburger-arrow-left">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
                <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                    <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                        <span class="sr-only">Toggle fullscreen</span>
                    </a>
                </li>
                <li class="nav-item hidden-float">
                    <a class="nav-link icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                       role="button">
                        <span class="sr-only">Toggle Search</span>
                    </a>
                </li>
                <li class="nav-item hidden-float" style="color:white;display:flex">
            <p style="display:none" class="nav-link" id="exsub">Deadline:</p>
            <span class="nav-link" id="expire_submit"></span>
          </li>
            </ul>
            <!-- End Navbar Toolbar -->

            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">

                <li class="nav-item dropdown">
                    <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                       data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="../../../global/portraits/5.jpg" alt="...">
                <i></i>
              </span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" onclick="profile()" role="menuitem"><i class="icon md-account"
                                                                                        aria-hidden="true"></i> Profile</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" onclick="logout()" role="menuitem"><i class="icon md-power"
                                                                                       aria-hidden="true"></i>
                            Logout</a>
                    </div>
                </li>
            </ul>
            <ul class="nav navbar-toolbar navbar-right">
                      <li class="nav-item hidden-float" style="color:white;display:flex;float:right">
                        <p style="display:none" class="nav-link" id="exedi">Edit deadline</p>
                        <span class="nav-link" id="expire_edit"></span>
                      </li>

                <li class="nav-item dropdown" role="presentation">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
                       role="button">
                        <span><i class="icon md-collection-folder-image" aria-hidden="true"></i> Faculty</span>
                    </a>
                    <div class="dropdown-menu" role="menu" id="listFaculty">
                        <!-- <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Action</a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Another action</a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">Separated link</a> -->
                    </div>
                </li>
            </ul>
            <!-- End Navbar Toolbar Right -->

            <div class="navbar-brand navbar-brand-center">
                <a href="homepage">
                    <img class="navbar-brand-logo navbar-brand-logo-normal" src="../../assets/images/logo.png"
                         title="Remark">
                    <img class="navbar-brand-logo navbar-brand-logo-special" src="../../assets/images/logo-colored.png"
                         title="Remark">
                </a>
            </div>
        </div>
        <!-- End Navbar Collapse -->

        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
                <div class="form-group">
                    <div class="input-search">
                        <i class="input-search-icon md-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="site-search" placeholder="Search...">
                        <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                                data-toggle="collapse" aria-label="Close"></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Site Navbar Seach -->
    </div>
</nav>
<div class="site-menubar">
    <div class="site-menubar-header">
        <div class="cover overlay">
            <img class="cover-image" src="../../assets//examples/images/dashboard-header.jpg" alt="...">
            <div class="overlay-panel vertical-align overlay-background">
                <div class="vertical-align-middle">
                    <a class="avatar avatar-lg" href="javascript:void(0)">
                        <img src="../../../global/portraits/1.jpg" alt="">
                    </a>
                    <div class="site-menubar-info">
                        <h5 class="site-menubar-user" id="lbluser">Machi</h5>
                        <p class="site-menubar-email" id="lblemail">machidesign@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu" data-plugin="menu" id="left-menu">

                </ul>
            </div>
        </div>
    </div>
</div>
