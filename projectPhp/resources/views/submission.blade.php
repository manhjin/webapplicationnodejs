<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title>Submission</title>

    <link rel="apple-touch-icon" href="{{asset('assets/images/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">

    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.min.css')}}">

    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/introjs.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.css">
    <link rel="stylesheet" href="{{asset('global/vendor/bootstrap-select/bootstrap-select.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/dropify/dropify.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/bootstrap-markdown/bootstrap-markdown.css')}}">
    <!-- alert css -->
    <link rel="stylesheet" href="{{asset('global/vendor/alertify/alertify.css')}}">
    <link rel="stylesheet" href="{{asset('global/vendor/notie/notie.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/advanced/alertify.css')}}">

    <script src="{{asset('styling/general/dist/index.min.js')}}"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('global/fonts/font-awesome/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/material-design/material-design.min.css')}}">
    <link rel="stylesheet" href="{{asset('global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!--[if lt IE 9]>
    <script src="{{asset('global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="{{asset('global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="{{asset('global/vendor/breakpoints/breakpoints.js')}}"></script>

    <script>
        Breakpoints();

    </script>
</head>

<body class="animsition">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
@include('layouts.header')
<!-- Page -->
<div class="page">
    <div class="page-header">
        <h1 class="page-title">Submission Form</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{asset('')}}homepage/homepage.html">Home</a></li>
            <li class="breadcrumb-item active">Submission</li>
        </ol>
        <div class="page-header-actions">
            <button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip"
                    data-original-title="Edit">
                <i class="icon md-edit" aria-hidden="true"></i>
            </button>
            <button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip"
                    data-original-title="Refresh">
                <i class="icon md-refresh-alt" aria-hidden="true"></i>
            </button>
            <button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip"
                    data-original-title="Setting">
                <i class="icon md-settings" aria-hidden="true"></i>
            </button>
        </div>
    </div>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- Panel Static Labels -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Submit a Contribution</h3>
                    </div>
                    <div class="panel-body container-fluid">
                        <form autocomplete="off" id="uploadForm"
                              enctype="multipart/form-data"
                              action="http://128.199.231.68/articles/upload/createNewArticle"
                              method="post">
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label" for="inputTitle">Title</label>
                                <input type="text" class="form-control" id="inputTitle" name="articleName" placeholder="Title" />
                            </div>
                            <div class="form-group form-material" data-plugin="formMaterial">
                                <label class="form-control-label">Upload articles</label>
                                <input type="text" class="form-control" placeholder="Browse.." readonly="" />
                                <input name="files" id="docxFile" type="file" multiple="" />
                            </div>
                            <input class="form-control" id="person" name="person" hidden>
                            <input class="form-control" id="postemail" name="postemail" hidden>
                            <div class="form-group form-material" data-plugin="formMaterial" hidden>
                                <label class="form-control-label" for="articleFaculty">Content</label>
                                <input class="form-control" id="articleFaculty" name="articleFaculty">
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="example-wrap">
                                    <h4 class="example-title">Upload picture</h4>
                                    <div class="example">
                                        <input type="file" name="files" id="inputImage" data-plugin="dropify" data-default-file="" multiple />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-material floating" data-plugin="formMaterial">
                                <label class="form-control-label" for="articleCategory">Category</label>
                                <select class="form-control" name="articleCategory" id="articleCategory" onchange="loadcatename()">
                                    <option value=""></option>
                                </select>
                                <input type="hidden" id="categoryName" name="categoryName"  />
                            </div>
                            <span class="input-group-btn">
                  <button class="btn btn-info" type="submit" id="submit-btn" hidden onsubmit="return false;">Submit</button>
                </span>
                            <button class='btn btn-info' style="width:100%" data-target='#roleform' data-toggle='modal' type='button'><span><i class='icon md-collection-plus' aria-hidden='true'></i>Submit</span></button>
                        </form>
                    </div>
                </div>
                <div class="form-group form-material row" data-plugin="formMaterial">
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- End Panel Static Labels -->
</div>


<div class="modal fade modal-3d-sign" id="roleform" aria-hidden="true"
     aria-labelledby="exampleModalTitle" role="dialog" tabindex="1">
    <div class="modal-dialog modal-simple">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title" style="margin:auto">TERMS AND CONDITIONS</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <ul class="form" style="height:100%">
                        <li>
                            An electronic copy of your work for this coursework should be fully uploaded by midnight (local time) on the Deadline Date.
                        </li>
                        <li>
                            The last version you upload will be the one that is marked.
                        </li>
                        <li>
                            For this coursework you must submit a single Acrobat PDF document. In general, any text in the document must not be an image (i.e. must not be scanned) and would normally be generated from other documents (e.g. MS Office using "Save As .. PDF").
                        </li>

                        <li>
                            There are limits on the file size. The current limits are displayed on the coursework submission page on the Intranet
                        </li>
                        <li>
                            Make sure that any files you upload are virus-free and not protected by a password or corrupted otherwise they will be treated as null submissions.
                        </li>
                        <li>
                            Comments on your work will be available from the Coursework page on the Intranet. The grade will be made available in the portal.
                        </li>
                        <li>
                            All coursework must be submitted as above
                        </li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-pure" data-dismiss="modal" id="closesave">Decline</button>
                <button type="button" class="btn btn-primary" onclick="ok()">Accept</button>
            </div>
        </div>
    </div>
</div><!-- End Page -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
<!-- Footer -->
<!-- <footer class="site-footer">
  <div class="site-footer-legal">© 2018 <a
      href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
  <div class="site-footer-right">
    Crafted with <i class="red-600 icon md-favorite"></i> by <a
      href="https://themeforest.net/user/creation-studio">Creation
      Studio</a>
  </div>
</footer> -->
<!-- Core  -->
<script src="{{asset('global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
<script src="{{asset('global/vendor/popper-js/umd/popper.min.js')}}"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="{{asset('global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
<script src="{{asset('global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.2/waves.min.js"></script>
<!-- Plugins -->
<script src="{{asset('global/vendor/switchery/switchery.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>
<script src="{{asset('global/vendor/screenfull/screenfull.js')}}"></script>
<script src="{{asset('global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
<script src="{{asset('global/vendor/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-tmpl/tmpl.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-load-image/load-image.all.min.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-process.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-image.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-audio.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-video.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-validate.js')}}"></script>
<script src="{{asset('global/vendor/blueimp-file-upload/jquery.fileupload-ui.js')}}"></script>
<script src="{{asset('global/vendor/dropify/dropify.min.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap-markdown/bootstrap-markdown.js')}}"></script>
<script src="{{asset('global/vendor/marked/marked.js')}}"></script>
<script src="{{asset('global/vendor/to-markdown/to-markdown.js')}}"></script>
<script src="{{asset('global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
<!-- alert js -->
<script src="{{asset('global/vendor/alertify/alertify.js')}}"></script>
<script src="{{asset('global/vendor/notie/notie.js')}}"></script>

<!-- Scripts -->
<script src="{{asset('global/js/Component.js')}}"></script>
<script src="{{asset('global/js/Plugin.js')}}"></script>
<script src="{{asset('global/js/Base.js')}}"></script>
<script src="{{asset('global/js/Config.js')}}"></script>
<script src="{{asset('assets/js/Section/Menubar.js')}}"></script>
<script src="{{asset('assets/js/Section/Sidebar.js')}}"></script>
<script src="{{asset('assets/js/Section/PageAside.js')}}"></script>
<script src="{{asset('assets/js/Plugin/menu.js')}}"></script>
<!-- Config -->
<script src="{{asset('global/js/config/colors.js')}}"></script>
<script src="{{asset('assets/js/config/tour.js')}}"></script>
<script>
    Config.set('assets', '{{asset('assets')}}');

</script>
<!-- Page -->
<script src="{{asset('assets/js/Site.js')}}"></script>
<script src="{{asset('global/js/Plugin/asscrollable.js')}}"></script>
<script src="{{asset('global/js/Plugin/slidepanel.js')}}"></script>
<script src="{{asset('global/js/Plugin/switchery.js')}}"></script>
<script src="{{asset('global/js/Plugin/jquery-placeholder.js')}}"></script>
<script src="{{asset('global/js/Plugin/material.js')}}"></script>
<script src="{{asset('global/js/Plugin/dropify.js')}}"></script>
<script src="{{asset('assets/examples/js/forms/uploads.js')}}"></script>
<script src="{{asset('global/js/Plugin/alertify.js')}}"></script>
<script src="{{asset('global/js/Plugin/notie-js.js')}}"></script>

<script src="{{asset('js/submission.js')}}"></script>
<script src="{{asset('js/header.js')}}"></script>
<script src="{{asset('js/logout.js')}}"></script>
<script src="{{asset('styling/general.js')}}"></script>
</body>

</html>
