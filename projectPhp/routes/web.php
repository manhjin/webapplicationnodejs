<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('homepage', function () {
    return view('homepage');
});
Route::get('submission', function () {
    return view('submission');
});

Route::get('article', function () {
    return view('article');
});
Route::get('article-by-faculty', function () {
    return view('article-by-faculty');
});
Route::get('article-by-topic', function () {
    return view('article-by-topic');
});
Route::get('closure-date', function () {
    return view('closure-date');
});
Route::get('manage-account', function () {
    return view('manage-account');
});
Route::get('manage-article', function () {
    return view('manage-article');
});
Route::get('manage-faculty', function () {
    return view('manage-faculty');
});
Route::get('manage-guest', function () {
    return view('manage-guest');
});
Route::get('manage-topic', function () {
    return view('manage-topic');
});
Route::get('my-article', function () {
    return view('my-article');
});
Route::get('profile', function () {
    return view('profile');
});
Route::get('register', function () {
    return view('register');
});
Route::get('review-article', function () {
    return view('review-article');
});
Route::get('statistics', function () {
    return view('statistics');
});
Route::get('view-faculty', function () {
    return view('view-faculty');
});
//Route::get('', function () {
//    return view('');
//});
//

