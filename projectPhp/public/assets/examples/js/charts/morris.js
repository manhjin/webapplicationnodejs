function loadDonut() {
    var closureSelect = $("#closureSelect");
    var donutData = [];
    var closureDateArr = [];
    //xử lý closure date
    closureSelect.selectpicker();
    var closureVal = $("#closureSelect option:selected").val();
    var closureText = $("#closureSelect option:selected").text();
    //xử lý donut
    $.ajax({
        url: "http://128.199.231.68/statistic/statisticByFaculty",
        type: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        data: {
            closureid: closureVal
        },
        async: false,
        success: function (response) {
            var arrdetail = response;
            var total = arrdetail.data.total;
            //html of donut
            $("#donutChart").append('<div class="col-lg-4">' +
                '<div class="card card-shadow" id="widgetLineareaFour">' +
                '<div class="card-block p-20 pt-10">' +
                '<div class="clearfix">' +
                '<div class="grey-800 float-left py-10">' +
                '<i class="icon md-view-list grey-600 font-size-24 vertical-align-bottom mr-5"></i>Articles</div>' +
                '<span class="float-right grey-700 font-size-30" id="total">' + total + '</span>' +
                '</div>' +
                '<div class="mb-20 grey-500">' +
                '<i class="icon md-long-arrow-up green-500 font-size-16"></i>                  18.4% From this yesterday' +
                '</div>' +
                '<div class="ct-chart h-50"><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" color="red" height="100%" class="ct-chart-line" style="width: 100%; height: 100%;"><g class="ct-grids"></g><g><g class="ct-series ct-series-a"><path d="M0,50L0,50C12.479,45,24.958,40,37.438,35C49.917,30,62.396,20.769,74.875,20C87.354,19.231,99.833,19.508,112.313,18.75C124.792,17.992,137.271,6.25,149.75,6.25C162.229,6.25,174.708,25,187.188,25C199.667,25,212.146,18.75,224.625,18.75C237.104,18.75,249.583,29.8,262.063,35C274.542,40.2,287.021,45,299.5,50L299.5,50Z" class="ct-area"></path></g></g><g class="ct-labels"></g></svg></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-8">' +
                '<div class="example-wrap">' +
                '<h4 class="example-title">Percentage of contributions by each Faculty for any academic year.' +
                '(' + closureText + ') </h4>' +
                '<div class="example">' +
                '<div id="' + closureVal + '"></div>' +
                '</div>' +
                '</div>' +
                '</div>');
            if (arrdetail.data.data.length > 0) {
                arrdetail.data.data.forEach(item => {
                    //donut
                    var donutObject = new Object();
                    donutObject.label = getFaculty(item.id[0]);
                    donutObject.value = item.percentage;
                    donutData.push(donutObject);
                })

            } else {
                alert("This closure date is empty");
            }

            //donut
            (function () {
                Morris.Donut({
                    element: closureVal,
                    data: donutData,
                    // barSizeRatio: 0.35,
                    resize: true,
                    colors: [Config.colors("red", 500), Config.colors("primary", 500), Config.colors("grey", 400)]
                });
            })();
            donutData = [];
            //line
        },
        error: function (result) {
            console.log("error point")
        }
    });
}
function getThisMonth(month) {
    switch (month) {
        case "January":
            month = "01";
            break;
        case "February":
            month = "02";
            break;
        case "March":
            month = "03";
            break;
        case "April":
            month = "04";
            break;
        case "May":
            month = "05";
            break;
        case "June":
            month = "06";
            break;
        case "July":
            month = "07";
            break;
        case "August":
            month = "08";
            break;
        case "September":
            month = "09";
            break;
        case "October":
            month = "10";
            break;
        case "November":
            month = "11";
            break;
        case "December":
            month = "12";
            break;
    }
    return month;
}
function getThisDate(date) {
    var temp = date.split('');
    if(temp.length == 3){
        temp[0] = "0" + temp[0];
    }else{
        temp[0] = temp[0] + temp[1];
    }
    return temp[0];
}
function convertDate(dateOnConverting) {
    dateOnConverting = dateOnConverting.split(' ');
    let month = getThisMonth(dateOnConverting[0]);
    let day = getThisDate(dateOnConverting[1]);
    let year = dateOnConverting[2].replace(',', '');
    return year + "-" + month + "-" + day;
}
function displayStatistic() {
    var lineObject = new Object();
    var lineLabel = [];
    var lineKey = [];
    $.ajax({
        url: "http://128.199.231.68/faculties/getAllFaculty",
        method: "GET",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        async: false,
        success: function (result) {
            var arrdetail = JSON.parse(JSON.stringify(result));
            if (arrdetail.data.length > 0) {
                arrdetail.data.forEach(item => {
                    lineObject[item._id] = 0;
                    lineLabel.push(item.facultyName);
                    lineKey.push(item._id);
                });

            } else {
                alert("There are no faculty!");
            }
        },
        error(jqXHR) {
            alert("Failed to get faculty")
        }
    });
    var closureSelect = $("#closureSelect");
    var tempData = [];
    var donutData = [];
    var closureDateArr = [];
    //xử lý closure date
    closureSelect.selectpicker();
    $.ajax({
        url: "http://128.199.231.68/closures/info",
        method: "GET",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        async: false,
        success: function (result) {
            var arrdetail = result;
            if (arrdetail.closure.length > 0) {
                arrdetail.closure.forEach(item => {

                    var closureObject = new Object();
                    closureDate = item.created_At + " --> " + item.expired_Edit;
                    // var dateOnSplitting = item.expired_Edit;
                    // var fromDateSplitting = item.created_At;
                    // var date = moment(dateOnSplitting).format("YYYY-MMM-DD");
                    // var fromDate = moment(fromDateSplitting).format("YYYY-MMM-DD");
                    var date = convertDate(item.expired_Edit);
                    var fromDate = convertDate(item.created_At);
                    console.log(date);
                    var dateRange = fromDate + " --> " + date;
                    addOption(closureSelect, dateRange, item._id);
                    //lấy dữ liệu từ ajax
                    closureObject.d = date;
                    closureObject.id = item._id;
                    closureObject.range = dateRange;
                    closureDateArr.push(closureObject);

                });

            } else {
                alert("Không có kết quả !");
            }
        },
        error(jqXHR) {
            //
        }
    });
    // // console.log(closureDateArr);
    // // console.log("Point");
    // var closureVal = $("#closureSelect option:selected").val();
    // var closureText = $("#closureSelect option:selected").text();
    // //xử lý donut
    //   $("#donutChart").append('<div class="col-lg-12">' +
    //     '<div class="example-wrap">' +
    //     '<h4 class="example-title">Percentage of contributions by each Faculty for any academic year.' +
    //     '(' + closureText + ') </h4>' +
    //     '<div class="example">' +
    //     '<div id="' + closureVal + '"></div>' +
    //     '</div>' +
    //     '</div>' +
    //     '</div>');
    //   $.ajax({
    //     url: "http://128.199.231.68/statistic/statisticByFaculty",
    //     type: "POST",
    //     beforeSend: function (request) {
    //       request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    //     },
    //     data: {
    //       closureid: closureVal
    //     },
    //     async: false,
    //     success: function (response) {
    //       var arrdetail = response;
    //       if (arrdetail.data.data.length > 0) {
    //         arrdetail.data.data.forEach(item => {
    //           //donut
    //           var donutObject = new Object();
    //           donutObject.label = getFaculty(item.id[0]);
    //           donutObject.value = item.percentage;
    //           donutData.push(donutObject);
    //         })
    //       }
    //       //donut
    //       (function () {
    //         Morris.Donut({
    //           element: closureVal,
    //           data: donutData,
    //           // barSizeRatio: 0.35,
    //           resize: true,
    //           colors: [Config.colors("red", 500), Config.colors("primary", 500), Config.colors("grey", 400)]
    //         });
    //       })();
    //       donutData = [];
    //       //line
    //     },
    //     error: function (result) {
    //       console.log("error point")
    //     }
    //   });

    // xử lý dữ liệu statistic
    for (var i = 0; i < closureDateArr.length; i++) {
        $.ajax({
            url: "http://128.199.231.68/statistic/statisticByFaculty",
            type: "POST",
            beforeSend: function (request) {
                request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
            },
            data: {
                closureid: closureDateArr[i].id
            },
            async: false,
            success: function (response) {
                var arrdetail = response;
                lineObject.y = closureDateArr[i].d;
                if (arrdetail.data.data.length > 0) {
                    arrdetail.data.data.forEach(item => {
                        //line
                        lineObject[item.id[0]] = item.number;
                    })
                }
                tempData.push(lineObject);
                //line
            },
            error: function (result) {
                console.log("error point")
            }
        });
    }
    console.log(lineObject);
    (function (global, factory) {
        if (typeof define === "function" && define.amd) {
            define('/charts/morris', ['jquery', 'Site'], factory);
        } else if (typeof exports !== "undefined") {
            factory(require('jquery'), require('Site'));
        } else {
            var mod = {
                exports: {}
            };
            factory(global.jQuery, global.Site);
            global.chartsMorris = mod.exports;
        }
    })(this, function (_jquery, _Site) {
        'use strict';

        var _jquery2 = babelHelpers.interopRequireDefault(_jquery);
        (0, _jquery2.default)(document).ready(function ($$$1) {
            (0, _Site.run)();
        });
        // Example Morris Line
        // -------------------

        (function () {
            Morris.Line({
                element: 'exampleMorrisLine',
                data: tempData,
                xkey: 'y',
                ykeys: lineKey,
                labels: lineLabel,
                // ykeys: ykeys,
                // labels: labels,
                resize: true,
                pointSize: 3,
                smooth: true,
                gridTextColor: '#474e54',
                gridLineColor: '#eef0f2',
                goalLineColors: '#e3e6ea',
                gridTextFamily: Config.get('fontFamily'),
                gridTextWeight: '300',
                numLines: 7,
                gridtextSize: 14,
                lineWidth: 1,
                lineColors: [Config.colors("green", 600), Config.colors("primary", 600)]
            });
        })();


        // Example Morris Donut
        // --------------------
        // chẹp

        // (function () {
        //   Morris.Donut({
        //     element: 'exampleMorrisDonut2',
        //     data: [],
        //     // barSizeRatio: 0.35,
        //     resize: true,
        //     colors: [Config.colors("red", 500), Config.colors("primary", 500), Config.colors("grey", 400)]
        //   });
        // })();
    });


}
