if (localStorage.getItem('accessToken') != null &&
    localStorage.getItem('accessToken') != '' &&
    typeof (localStorage.getItem('accessToken') != 'undefined')) {
    $(function () {
        var encodedData = window.location.href.split('=')[1];
        var articleValue = decodeURI(encodedData);
        $.ajax({
            url: 'http://128.199.231.68/articles/getUsersArticle',
            method: 'POST',
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', localStorage.getItem('accessToken'));
            },
            data: {
                id: articleValue
            },
            success: function (response) {
                if (response.status === false) {
                    console.log(response.msg);
                } else {
                    console.log("123");
                    console.log(response)
                    response.articles.forEach(item => {
                        $('#content').append("<div class='card card-shadow'>" +
                            "<div class='card-block media clearfix p-25'>" +
                            "<div class='pr-20'>" +
                            "<a href='#' class='avatar avatar-lg'>" +
                            "<img class='img-fluid' src='../../../global/portraits/6.jpg'>" +
                            "</a>" +
                            "</div>" +
                            "<div class='media-body text-middle'>" +
                            "<h4 class='mt-0 mb-5'>" +
                            localStorage.getItem("user") +
                            "</h4>" +
                            "<small>Posted on" + item.created_At + "</small>" +
                            "</div>" +
                            "</div>" +
                            "<div class='card-block px-25  pt-0'>" +
                            "<div class='panel-content'>" +
                            "<div class='itemContent'>" +

                            "<div class='itemTitle'>" + item.articleContent + "</div>" +
                            "<div class='itemDesc'>" +
                            "<img src='http://128.199.231.68/" + item.articleImage + "' alt=''>" +
                            "</div>" +

                            "</div>" +
                            "<ul class='wall-attrs clearfix p-0 m-0'>" +
                            "<li class='attrs-meta float-left'>" +
                            "<span>" +
                            "<i class='icon md-chat'>" + "</i>0</span>" +
                            "<span class='ml-10'>" +
                            "</if>" +
                            "<i class='icon md-heart'>" + "</i>0 </span>" +
                            "</li>" +
                            "<li class='float-right'>" +
                            "<a href='#' class='avatar avatar-sm' data-paticipator-id='p_1'>" +
                            "<img src='../../../global/portraits/3.jpg'>" +
                            "</a>" +
                            "</li>" +
                            "</ul>" +
                            "</div>" +
                            "</div>" +
                            "<div class='card-footer p-20'>" +
                            "<div class='wall-comment'>" +
                            "<a href='#' class='avatar avatar-md float-left'>" +
                            "<img src='../../../global/portraits/3.jpg'>" +
                            "</a>" +
                            "<div class='ml-60'>" +
                            "<a href='#'>Stacey Hunt </a>" +
                            "<small class='ml-10'>30 th July 2017 </small>" +
                            "<p class='mt-5'>Cillum sit aliquip irure minim.Cillum mollit aute consectetur non et ut ipsum.Occaecat laboris eu voluptate ipsum commodo consequat laborum laborum.Anim deserunt voluptate voluptate labore Lorem dolore proident."
                            + "</p>" +
                            "</div>" +
                            "<div class='dropdown comment-operation'>" +
                            "<button type='button' class='btn btn-icon btn-round btn-default btn-sm' data-toggle='dropdown' aria-expanded = 'false' > " +
                            "<i class='icon md-more-vertical'>" + "</i>" +
                            "</button>" +
                            "<div class='dropdown-menu dropdown-menu-right' role='menu'>" +
                            "<a class='dropdown-item' href='javascript:void(0)' role='menuitem'>Report</a>" +
                            "<a class='dropdown-item' href='javascript:void(0)' role='menuitem'>Delete</a>" +
                            "</div>" +
                            "</div>" +
                            "</div>" +
                            "<div class='wall-comment'>" +
                            "<a href='#' class='avatar avatar-md float-left'>" +
                            "<img src='../../../global/portraits/5.jpg'>" +
                            "</a>" +
                            "<div class='ml-60'>" +
                            "<a href='#'>Sam Anderson</a>" +
                            "<small class='ml-10'>3 mins ago...</small>" +
                            "<p class='mt-5'>Aliqua nulla culpa nulla quis.Adipisicing et consequat in Lorem.Pariatur sunt esse nostrud occaecat velit anim commodo.Magna nostrud nisi id deserunt cupidatat mollit incididunt."
                            + "</p>" +
                            "</div>" +
                            "<div class='dropdown comment-operation'>" +
                            "<button type='button' class='btn btn-icon btn-round btn-default btn-sm' data-toggle='dropdown' aria-expanded='false'>" +
                            "<i class='icon md-more-vertical'>" + "</i>" +
                            "</button>" +
                            "<div class='dropdown-menu dropdown-menu-right' role='menu'>" +
                            "<a class='dropdown-item' href='javascript:void(0)' role='menuitem'>Report</a>" +
                            "<a class='dropdown-item' href='javascript:void(0)' role='menuitem'>Delete</a>" +
                            "</div>" +
                            "</div>" +
                            "</div>" +
                            "<form class='wall-comment-reply'>" +
                            "<input type='text' class='form-control' placeholder='Write Something...'>" +
                            "<div class='reply-operation'>" +
                            "<button class='btn btn-sm btn-primary reply-post' type='button'>POST</button>" +
                            "<button class='btn btn-sm btn-pure btn-default reply-cancel' type='button'>CANCEL</button>" +
                            "</div>" +
                            "</form>" +
                            "</div>" +
                            "</div>");
                    });
                }

            }
        })
    })
}
