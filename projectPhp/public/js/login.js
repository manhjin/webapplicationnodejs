$(document).ready(function(){
  $(document).bind('keypress', function(e) {
    if(e.keyCode==13){
      $('#login-btn').trigger('click');
    }
  });
  $("#login-btn").click(function () {
    var usernameInput = $("#inputName").val();
    if (isEmpty(usernameInput, "Username")) {

      return false;
    }
    $("#success-alert").remove();

    if (checkLength(usernameInput, "Username")) {
      return false
    }

    var passwordInput = $("#inputPassword").val();
    if (isEmpty(passwordInput, 'Password')) {
      return false;
    }
    if (checkLength(passwordInput, "Password")) {
      return false
    }
    var data_str = {
      username: usernameInput,
      password: passwordInput
    };
    var host_api = "http://128.199.231.68/users/login";
    $.ajax({
      url: host_api,
      type: "POST",
      contentType: 'application/json',
      data: JSON.stringify(data_str),
      success: function (result) {
        if (result.token != null && result.token != '' && typeof (result.token) != 'undefined') {
          localStorage.setItem('accessToken', result.token);
          localStorage.setItem("id", result.userInformation.id);
          localStorage.setItem("user", result.userInformation.username);
          localStorage.setItem('role', result.userInformation.role);
          localStorage.setItem('email', result.userInformation.email);
          localStorage.setItem('userFaculty', result.userInformation.userFaculty);
          window.location.href = "homepage";
        } else {
          alert(result.msg);
          console.log(result);
        }
      },
      error(jqXHR) {
        alert("Invalid Username or Password, Please enter again!")
      }
    });
  });
});



