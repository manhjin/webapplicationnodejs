var token = localStorage.getItem('accessToken');

function logout() {
    if (token != null && token != '' && typeof (token) != 'undefined') {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('id');
        localStorage.removeItem('user');
        localStorage.removeItem('role');
        localStorage.removeItem('email');
        localStorage.removeItem('userFaculty');
        window.location.href = "/";
    } else {
        alert("Vui lòng đăng nhập trước");
    }
}
