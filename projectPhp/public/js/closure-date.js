$(document).ready(function () {
    $("#lbluser").text(localStorage.getItem("user"));
    $("#lblemail").text(localStorage.getItem("email"));

    //console.log(localStorage.getItem("accessToken"));
    // && localStorage.getItem("role") === 0
    if (localStorage.getItem("accessToken") != null && (localStorage.getItem("role") == 0 || localStorage.getItem("role") == 2)) {
        loadpage();
    } else {
        window.location.href = "/";
    }
});
function loadpage() {
    var dataTable = $("#exampleTableSearch").dataTable().api();
    dataTable.clear();
    $("#exampleTableSearch tbody").remove("tr");
    var host_api = "http://128.199.231.68/closures/info";

    $.ajax({
        url: host_api,
        method: "GET",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        success: function (result) {
            var arrdetail = JSON.parse(JSON.stringify(result));
            console.log(arrdetail);
            if (arrdetail.closure.length > 0) {
                arrdetail.closure.forEach(item => {
                    console.log(item._id);
                    var row = "<tr>";
                    if (item.status == 1) {
                        row += "<td><span class='badge badge-success'>Activated</span></td>";
                    } else {
                        row += "<td><span class='badge badge-danger'>Deactivated</span></td>";
                    }
                    row += "<td>" + item.created_At + "</td>";
                    row += "<td>" + item.expired_At + "</td>";
                    row += "<td>" + item.expired_Edit + "</td>";
                    row += "<td><span class='badge badge-danger'>" + item.username + "</span></td>";
                    row +=
                        "<td> <button class='btn btn-primary' data-target='#roleform' data-toggle='modal'type = 'button'  id='" + item._id + "'  onclick='chagerole(this.id)' >Edit <i class='icon md-edit' aria-hidden='true'></i> </button ></td>";



                    row += "</tr>";

                    var tr = document.createElement("tr");
                    tr.innerHTML = row;
                    dataTable.row.add(tr);
                });
                dataTable.draw(false);

            } else {
                alert("Không có kết quả !");
                dataTable.clear();
                dataTable.draw();
            }
        },
        error(jqXHR) {
            //
        }
    });
}

function chagerole(id) {

    var id = id;
    var host_api = "http://128.199.231.68/closures/getClosureInfo";
    var data_str = {
        id: id
    };
    console.log(data_str);
    $.ajax({
        contentType: 'application/json',
        url: host_api,
        type: "POST",
        data: JSON.stringify(data_str),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        success: function (result) {
            var arrdetail = JSON.parse(JSON.stringify(result));

            $("#newcreated_At").val(arrdetail.closure.created_At.replace(/th|st|rd|nd/gi, "")).trigger("change");
            $("#newexpired_At").val(arrdetail.closure.expired_At.replace(/th|st|rd|nd/gi, "")).trigger("change");
            $("#newexpired_Edit").val(arrdetail.closure.expired_Edit.replace(/th|st|rd|nd/gi, "")).trigger("change");
            $("#newuser").val(arrdetail.closure.username).trigger("change");
            $("#closureid").val(arrdetail.closure._id).trigger("change");


            $("#newstatus").val(arrdetail.closure.status).trigger("change");

        },
        error(jqXHR) {
            //
        }
    });

}
function addnew() {
    var dataTable = $("#exampleTableSearch").dataTable().api();
    dataTable.clear();
    var status = 1;
    var username = localStorage.getItem("user");
    var created_At = $("#created_At").val();
    var expired_At = $("#expired_At").val();
    var expired_Edit = $("#expired_Edit").val();
    var created_By = localStorage.getItem("id");
    //var username = localStorage.getItem("user")
    if (isEmpty(created_At, "Start Date")) {
        return false;
    }
    if (isEmpty(expired_At, "Expiry Date of Submitting")) {
        return false;
    }
    if (isEmpty(expired_Edit, "Expiry Date of Editing")) {
        return false;
    }
    if (isEmpty(created_By, "Created By")) {
        return false;
    }
    if (isEmpty(username, "Username")) {
        return false;
    }
    if (created_At > expired_At) {
        alert("Expiry Date of Submitting must greater than Start Date");
        return false;
    }
    if (expired_At > expired_Edit) {
        alert("Expiry Date of Editing must greater than Expiry Date of Submitting");
        return false;
    }
    var host_api = "http://128.199.231.68/closures/createClosure";
    var data_str = {
        status: status,
        created_At: moment(created_At).format("MMMM Do YYYY, h:mm:ss a"),
        expired_At: moment(expired_At).format("MMMM Do YYYY, h:mm:ss a"),
        expired_Edit: moment(expired_Edit).format("MMMM Do YYYY, h:mm:ss a"),
        created_By: created_By,
        username: username
    };

    console.log(data_str);
    $.ajax({
        contentType: 'application/json',
        url: host_api,
        type: "POST",
        data: JSON.stringify(data_str),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        success: function (result) {
            if (result.code == 300) {
                alert("Add closure date successfully");
                loadpage();
            } else {
                alert(result.msg);
            }
        },
        error(jqXHR) {
            alert(jqXHR.msg);

            alert("Failed to add new closure date");
        }
    });
}
function savechange() {
    var dataTable = $("#exampleTableSearch").dataTable().api();
    dataTable.clear();
    var closureid = $("#closureid").val();
    var status = $("#newstatus").val();

    var created_At = $("#newcreated_At").val();
    var expired_At = $("#newexpired_At").val();
    var expired_Edit = $("#newexpired_Edit").val();
    var created_By = localStorage.getItem("id");
    var username = $("#newuser").val();
    if (isEmpty(created_At, "Start Date")) {
        return false;
    }
    if (isEmpty(expired_At, "Expiry Date of Submitting")) {
        return false;
    }
    if (isEmpty(expired_Edit, "Expiry Date of Editing")) {
        return false;
    }
    if (isEmpty(created_By, "Created By")) {
        return false;
    }
    if (isEmpty(username, "Username")) {
        return false;
    }
    if (created_At > expired_At) {
        alert("Expiry Date of Submitting must greater than Start Date");
        return false;
    }
    if ($("#newexpired_At").val() > $("#newexpired_Edit").val()) {
        alert("Expiry Date of Editing must greater than Expiry Date of Submitting");
        return false;
    }

    var host_api = "http://128.199.231.68/closures/updateClosure";
    var data_str = {
        status: status,
        created_At: moment(created_At).format("MMMM Do YYYY, h:mm:ss a"),
        expired_At: moment(expired_At).format("MMMM Do YYYY, h:mm:ss a"),
        expired_Edit: moment(expired_Edit).format("MMMM Do YYYY, h:mm:ss a"),
        created_By: created_By,
        username: username,
        closureid: closureid
    };


    $.ajax({
        contentType: 'application/json',
        url: host_api,
        type: "POST",
        data: JSON.stringify(data_str),
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        success: function (result) {
            if (result.code == 300) {
                alert("Edit closure date successfully");
                loadpage();

            } else {
                alert(result.msg)
            }
        },
        error(jqXHR) {
            alert('Failed to edit closure date');
        }
    });
}

function downloadZip() {
    var host_api = 'http://128.199.231.68/manage/downloadArticles';
    $.ajax({
        url: host_api,
        type: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        success: function (result) {
            window.location.href = result.success;
            console.log(result.success);
        },
        error(jqXHR) {
            //
        }
    });
}
