$(document).ready(function () {
  console.log(localStorage.getItem("accessToken"));
  // && localStorage.getItem("role") === 0
  if (localStorage.getItem("accessToken") != null && localStorage.getItem("role") == 3) {
    loadpage();
    loadNewFaculty();
    loadEditFaculty();
  } else {
    window.location.href = "/";
  }
});


function loadpage() {
  //table.page('first').draw(false);
  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  $("#exampleTableSearch tbody").remove("tr");
  var host_api = "http://128.199.231.68/category/getCategoryByFaculty";

  $.ajax({
    url: host_api,
    method: "POST",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    data: {
      facultyId: localStorage.getItem("userFaculty")
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      console.log(arrdetail.data);
      if (arrdetail.data.length > 0) {
        arrdetail.data.forEach(item => {
          var row = "<tr>";
          row += "<td>" + item.categoryName + "</td>";
          row += "<td>" + item.categoryDesc + "</td>";
          row += "<td>" + item.belongto_faculty.facultyName + "</td>";
          row +=
              "<td> <button class='btn btn-primary' data-target='#roleform' data-toggle='modal'type = 'button'  id='" + item._id + "'  onclick='changeCategory(this.id)' >Edit <i class='icon md-edit' aria-hidden='true'></i> </button ></td>";
          row += "</tr>";
          var tr = document.createElement("tr");
          tr.innerHTML = row;
          dataTable.row.add(tr);
        });
        dataTable.draw(false);

      } else {
        alert("Không có kết quả !");
        dataTable.clear();
        dataTable.draw();
      }
    },
    error(jqXHR) {
      alert("Failed to load topics")
    }
  });
}
var cid;

function changeCategory(id) {
  var id = id;
  cid = id;
  var host_api = "http://128.199.231.68/category/getCategoryInfo";
  var data_str = {
    id: id
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      $("#newcategoryName").val(arrdetail.category.categoryName).trigger("change");
      $("#newcategoryDesc").val(arrdetail.category.categoryDesc).trigger("change");
      $("#newstatus").val(arrdetail.category.status).trigger("change");
      $("#newcreated_At").val(arrdetail.category.created_At).trigger("change");
      $("#newbelongto_faculty").val(arrdetail.category.belongto_faculty).trigger("change");
    },
    error(jqXHR) {
      alert("Change Status Failed")
    }
  });

}
function addnew() {
  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  var categoryName = $("#categoryName").val();
  var categoryDesc = $("#categoryDesc").val();
  var belongto_faculty = $("#newfaculty option:selected").val();
  //alert(belongto_faculty);

  if (isEmpty(categoryName, "Name")) {
    return false;
  }
  if (isEmpty(categoryDesc, "Description")) {
    return false;
  }
  if (isEmpty(belongto_faculty, "Faculty")) {
    return false;
  }
  var host_api = "http://128.199.231.68/category/createNewCategory";
  var data_str = {
    categoryName: categoryName,
    categoryDesc: categoryDesc,
    belongto_faculty: belongto_faculty,
    role: "3"
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      alert("Add Topic Successfully");
      loadpage();
    },
    error: function (result) {
      alert(result.err)
    },
    error(jqXHR) {
      alert("Add Topic Failed")
    }
  });
}

function savechange() {

  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  var categoryName = $("#newcategoryName").val();
  var categoryDesc = $("#newcategoryDesc").val();
  var status = $("#newstatus").val();
  var categoryId = cid;
  var updated_By = localStorage.getItem("id");
  if (isEmpty(categoryName, "Name")) {
    return false;
  }
  if (isEmpty(categoryDesc, "Description")) {
    return false;
  }
  if (isEmpty(status, "Status")) {
    return false;
  }
  if (isEmpty(categoryId, "ID")) {
    return false;
  }
  if (isEmpty(updated_By, "Updated By")) {
    return false;
  }
  var host_api = "http://128.199.231.68/category/editCategory";
  var data_str = {
    categoryName: categoryName,
    categoryDesc: categoryDesc,
    status: status,
    categoryId: categoryId,
    updated_By: updated_By
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      if (result.code == 300) {
        alert("Edit Topic Successfully");
        loadpage();
      } else {
        alert(result.err)
      }


    },
    error(jqXHR) {
      alert("Edit Topic Failed")
    }
  });
}
