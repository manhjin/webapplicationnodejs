$(document).ready(function () {
    if (localStorage.getItem("accessToken") == null || localStorage.getItem("role") != 1) {
        window.location.href = "/";
    }
    loadcate();

    $("#articleFaculty").val(localStorage.getItem("userFaculty"));
    $("#person").val(localStorage.getItem("user"));
    $("#postemail").val(localStorage.getItem("email"));

})


$('#uploadForm').submit(function (e) {
    e.preventDefault();


    $(this).ajaxSubmit({
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        error: function (xhr) {
            alert("Submit Article Failed!")
        },
        success: function (response) {
            alert(response.msg);
            window.location.href = "my-article?id=" + localStorage.getItem("id");

            //window.location.href = "../my-article/my-article.html?id=" + localStorage.getItem("id");
            $("#status").empty().text(response);

        }
    });
});

function hasExtension(inputID, exts) {
    var fileName = document.getElementById(inputID).value;
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
}

function loadcatename() {
    var value = $("#articleCategory option:selected").text();
    $("#categoryName").val(value);
}

//checked
function ok() {
    var title = $("#uploadForm input#inputTitle").val();
    var Category = $("#uploadForm select#articleCategory").val();
    if (isEmpty(title, "Title")) {
        return false;
    }
    if (isEmpty(Category, "Category")) {
        return false;
    }
    if (!hasExtension("docxFile", ['.docx'])) {
        alert("You have upload wrong file format, please change the file format");
        return false;
    }
    if (!hasExtension("inputImage", [".jpg", ".jpeg", ".bmp", ".gif", ".png"])) {
        alert("You have upload wrong image format, please change the file format!");
        return false;
    }
    $("#submit-btn").click();
}
