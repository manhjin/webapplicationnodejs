$(document).ready(function () {
  $("#lbluser").text(localStorage.user);
  $("#lblemail").text(localStorage.email);

  console.log(localStorage.getItem("accessToken"));
  // && localStorage.getItem("role") === 0
  if (localStorage.getItem("accessToken") != null && localStorage.getItem("role") == 0) {
    loadpage();
    loadNewFaculty();
    loadEditFaculty();
    loadStatus();
    loadNewRole();
    loadEditRole();
  } else {
    window.location.href = "/";
  }
});
function loadpage() {
  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  $("#exampleTableSearch tbody").remove("tr");
  var host_api = "http://128.199.231.68/users/info";

  $.ajax({
    url: host_api,
    method: "GET",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      console.log(arrdetail);
      if (arrdetail.user.length > 0) {
        arrdetail.user.forEach(item => {
          var row = "<tr>";
          row += "<td>" + item.username + "</td>";
          row += "<td>" + item.email + "</td>";
          row += "<td>" + item.createdAt + "</td>";

          if (item.role == 0) {
            row += "<td><span class='badge badge-danger'>Admin</span></td>";
          }
          if (item.role == 1) {
            row += "<td><span class='badge badge-info'>Student</span></td>";
          }
          if (item.role == 2) {
            row += "<td><span class='badge badge-warning'>Marketing Manager</span></td>";
          }
          if (item.role == 3) {
            row += "<td><span class='badge badge-dark'>Marketing Coordinator</span></td>";
          }
          if (item.role == 4) {
            row += "<td><span class='badge badge-light'>Guest</span></td>";
          }
          if (item.userFaculty == "undefined") {
            row += "<td></td>";
          } else {
            row += "<td>" + item.userFaculty.facultyName + "</td>";

          }
          if (item.status == 0) {
            row += "<td><span class='badge badge-danger'>Activated</span></td>";
          }
          if (item.status == 1) {
            row += "<td><span class='badge badge-success'>Activated</span></td>";
          }
          row +=
              "<td> <button class='btn btn-primary' data-target='#roleform' data-toggle='modal' type = 'button'  id='" + item._id + "'  onclick='chagerole(this.id)' >Edit <i class='icon md-edit' aria-hidden='true'></i> </button ></td>";
          row += "</tr>";

          var tr = document.createElement("tr");
          tr.innerHTML = row;
          dataTable.row.add(tr);
        });
        dataTable.draw(false);

      } else {
        alert("Không có kết quả !");
        dataTable.clear();
        dataTable.draw();
      }
    },
    error(jqXHR) {
      //
    }
  });
}


function chagerole(id) {

  var id = id;
  var host_api = "http://128.199.231.68/users/getUserInfo";
  var data_str = {
    id: id
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      console.log(arrdetail);
      $("#username").val(arrdetail.user.username).trigger("change");
      $("#email").val(arrdetail.user.email).trigger("change");
      $("#firstName").val(arrdetail.user.firstName).trigger("change");
      $("#lastName").val(arrdetail.user.lastName).trigger("change");
      $("#role").val(arrdetail.user.role).trigger("change");
      $("#userId").val(arrdetail.user._id).trigger("change");
      $("#userFaculty").val(arrdetail.user.userFaculty).trigger("change");

      $("#userStatus").val(arrdetail.user.status).trigger("change");

    },
    error(jqXHR) {
      //
    }
  });

}
function addnew() {

  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  var firstName = $("#newfirstName").val().trimEnd();
  var lastName = $("#newlastName").val().trimEnd();
  var username = $("#newusername").val().trimEnd();
  var email = $("#newemail").val();
  var password = $("#newpassword").val();
  var role = $("#newrole").val();
  var newfaculty = $("#newfaculty").val();
  if(isEmpty(firstName, 'First Name')){
    return false;
  }
  if(isEmpty(lastName, 'Last Name')){
    return false;
  }
  if(isEmpty(username, 'Username')){
    return false;
  }
  if (checkLength(username, "Username")) {
    return false
  }
  if(isEmpty(email, 'Email')){
    return false;
  }
  if(isEmpty(password, 'Password')){
    return false;
  }
  if (checkLength(password, "Password")) {
    return false
  }
  if(isEmpty(role, 'Role')){
    return false;
  }
  if(isEmpty(userFaculty, 'Faculty')){
    return false;
  }

  var host_api = "http://128.199.231.68/users/register";
  var data_str = {
    firstName: firstName,
    lastName: lastName,
    username: username,
    email: email,
    password: password,
    role: role,
    userFaculty: newfaculty,
    status:1
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      console.log(result)
      alert("Insert new account successfully!");
      loadpage();


    },
    error(jqXHR) {
      alert("Error" + jqXHR)

    }
  });
}
function savechange() {
  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  var username = $("#username").val().trimEnd();
  var email = $("#email").val();
  var firstName = $("#firstName").val();
  var lastName = $("#lastName").val();
  var role = $("#role").val();
  var userId = $("#userId").val();
  var status = $("#userStatus").val();
  var userFaculty = $("#userFaculty").val();
  var updated_By = localStorage.getItem("id");
  if(isEmpty(username, 'Username')){
    return false;
  }
  if (checkLength(username, "Username")) {
    return false
  }
  if(isEmpty(email, 'Email')){
    return false;
  }
  if(isEmpty(firstName, 'First Name')){
    return false;
  }
  if(isEmpty(lastName, 'Last Name')){
    return false;
  }
  if(isEmpty(role, 'Role')){
    return false;
  }
  if(isEmpty(userId, 'User ID')){
    return false;
  }
  if(isEmpty(status, 'Status')){
    return false;
  }
  if(isEmpty(userFaculty, 'Faculty')){
    return false;
  }
  if(isEmpty(updated_By, 'Updated By')){
    return false;
  }
  var host_api = "http://128.199.231.68/users/updateUser";
  var data_str = {
    userId: userId,
    username: username,
    email: email,
    firstName: firstName,
    lastName: lastName,
    role: role,
    updated_By: updated_By,
    userFaculty: userFaculty,
    status: status
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      alert("Edit account successfully");
      loadpage();

    },
    error(jqXHR) {
      alert(jqXHR);
    }
  });
}
