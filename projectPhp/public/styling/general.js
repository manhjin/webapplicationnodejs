function addOption($select, text, value) {
    var $opt = $('<option />', {
        text: text
    });
    if (value === null) {
        value = text.toLowerCase().replace(/\s+/g, '-');
    }
    $opt.attr('value', value);
    $select.append($opt);
    $select.selectpicker('refresh');
}

function loadcate() {
    var inputFaculty = localStorage.getItem("userFaculty");
    var host_api = "http://128.199.231.68/category/getCategoryByFaculty";
    var $catSelect = $('#articleCategory');
    $catSelect.selectpicker();
    $.ajax({
        url: host_api,
        type: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        data: {
            facultyId: inputFaculty
        },
        success: function (result) {
            var arrdetail = result;
            console.log(arrdetail.data);
            if (arrdetail.data.length > 0) {
                arrdetail.data.forEach(item => {
                    addOption($catSelect, item.categoryName, item._id);
                });
            } else {
                alert("Không có kết quả !");
            }
        },
        error(jqXHR) {
            //
        }
    });
}

function loadStatus() {
    var $editStatusSelect = $('#userStatus');
    $editStatusSelect.selectpicker();
}

function loadNewRole() {
    var $newRoleSelect = $('#newrole');
    $newRoleSelect.selectpicker();
}

function loadEditRole() {
    var $editRoleSelect = $('#role');
    $editRoleSelect.selectpicker();
}

function loadNewFaculty() {
    var host_api = "http://128.199.231.68/faculties/getAllFaculty";
    var $newFacultySelect = $("#newfaculty");
    // var $editFacultySelect = $("#userFaculty");
    // var $newRoleSelect = $('#newrole');
    // var $editRoleSelect = $('#role');
    // var $editStatusSelect = $('#userStatus');
    $newFacultySelect.selectpicker();
    // $editFacultySelect.selectpicker();
    // $newRoleSelect.selectpicker();
    // $editRoleSelect.selectpicker();
    // $editStatusSelect.selectpicker();
    $.ajax({
        url: host_api,
        method: "GET",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        success: function (result) {
            var arrdetail = result;
            if (arrdetail.data.length > 0) {
                arrdetail.data.forEach(item => {
                    addOption($newFacultySelect, item.facultyName, item._id);
                    // addOption($editFacultySelect, item.facultyName, item._id);
                });

            } else {
                alert("Không có kết quả !");
            }
        },
        error(jqXHR) {
            //
        }
    });
}


function loadEditFaculty() {
    var host_api = "http://128.199.231.68/faculties/getAllFaculty";
    var $editFacultySelect = $("#userFaculty");
    $editFacultySelect.selectpicker();
    $.ajax({
        url: host_api,
        method: "GET",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        success: function (result) {
            var arrdetail = result;
            if (arrdetail.data.length > 0) {
                arrdetail.data.forEach(item => {
                    addOption($editFacultySelect, item.facultyName, item._id);
                });

            } else {
                alert("Không có kết quả !");
            }
        },
        error(jqXHR) {
            //
        }
    });
}

function alert(message) {
    $('body').append("<button type='button' class='btn btn-primary' id='alert' data-plugin='alertify'" +
        "data-alert-message='" + message + "' ></button>")

    $('#alert').click();
    $('#alert').remove();
}

function isEmpty(val, field) {
    var check = false;
    if (val == "" || val == null) {
        alert(field + " is blank, Please enter the information")
        check = true;
    }
    return check;
}

function checkLength(val, field) {
    var check = false;
    if (val.length < 5) {
        alert(field + " must longer than 5 characters, please enter again");
        check = true;
    } else if (val.length > 15) {
        alert(field + " must shorter than 15 characters, please enter again");
        check = true;
    }
    return check;
}

function getParam(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var params = url[i].split("=");
        if (params[0] == param)
            return params[1];
    }
    return false;
}

function getFaculty(facultyId) {
    var facultyName = "";
    $.ajax({
        url: "http://128.199.231.68/faculties/getFacultyInfo",
        type: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        async: false,
        data: {
            id: facultyId
        },
        success: function (response) {
            if (response.faculty.length > 0) {
                response.faculty.forEach(item => {
                    facultyName = item.facultyName;
                });
            }
        },
        error: function (result) {
            alert(result.err)
        },
    });
    return facultyName;
}

function getTopic(topicId) {
    var topicName = "";
    $.ajax({
        url: "http://128.199.231.68/category/getCategoryInfo",
        type: "POST",
        beforeSend: function (request) {
            request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        async: false,
        data: {
            id: topicId
        },
        success: function (response) {
            if (response.category.categoryName != "" && response.category.categoryName != null) {
                topicName = response.category.categoryName;
            } else {
                topicName = "Không tồn tại vui lòng thử lại!"
            }


        },
        error: function (result) {
            alert(result.err)
        },
    });
    return topicName;
}
