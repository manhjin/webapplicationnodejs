const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config/database');
const passport = require('passport');
const cors = require('cors');
const articles = require('./route/articles');
const closures = require('./route/closures');
const path = require('path');
const faculties = require('./route/faculties');
const moment_tz = require('moment-timezone');
const statistic = require('./route/statistic');
const manages = require('./route/manages');

const users = require('./route/users');
const cordova = require('./route/cordova');
const category = require('./route/categories');
var multer = require('multer');
const moment = require('moment');
moment_tz().tz("Asia/Ho_Chi_Minh");
moment_tz.locale("en");
const Closure = require('./model/closure');

//console.log(moment().format('LLLL'));



const port = config.port;

let app = express();
app.use(cors());
let server = http.createServer(app);
//Connect to mongoDB
mongoose.connect(config.database,{ useNewUrlParser: true });

//On Connection
mongoose.connection.on('connected',()=>{
    console.log('Connected to database', config.database);
});
//On error
mongoose.connection.on('error',(err)=>{
    console.log('Database Error: ' + err);
});

app.use(bodyParser.json({limit: '500mb'}));

app.use(bodyParser.urlencoded({ limit: '50mb',extended: true ,parameterLimit: 1000000}));  // <------- dòng này fix json login
//Passport Middleware
app.use(passport.initialize ());
app.use("/public", express.static(path.join(__dirname, 'public')));
app.use(passport.session());
require('./config/passport')(passport);

//app.use(express.json());       // to support JSON-encoded bodies
//app.use(express.urlencoded()); // to support URL-encoded bodies
//app.use(express.multipart());


server.listen(port,()=>{
    console.log(`the application is running on port ${port}`);
});

app.use('/testTimezone', (req,res,next) => {
    let a = moment_tz().tz("Asia/Ho_Chi_Minh").format('MMMM Do YYYY, h:mm:ss a');
    console.log(a);
   res.json({time: a, zone:moment_tz().tz("Asia/Ho_Chi_Minh").zoneName()})
});

app.use('*',(req,res,next)=>{
    Closure.getCurrentActiveInfor((err,data)=>{
        // console.log(data)
    });
    next();
});

app.use('/users',users);
app.use('/cordova',cordova);
app.use('/category', category);
app.use('/articles', articles);
app.use('/closures', closures);
app.use('/faculties', faculties);
app.use('/statistic', statistic);
app.use('/manage',manages);

