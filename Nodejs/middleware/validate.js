const emailRegrex = /[A_Za-z0-9._%+-]+@[A-Za-z0-9-]+.+.[A-Za-z]{2,4}/;
const characterRegex = /[A-Za-z]/;
const numberRegex = /[^0-9]/;
module.exports.validateUser = function (user) {
    if (user.lastName === '' || user.firstName === '' || user.username === '' || user.email === '' || user.password === '' ){
        return false;
    }else {
        console.log(emailRegrex.test(String(user.email).toLowerCase()));
        return emailRegrex.test(String(user.email).toLowerCase());
    }
};
module.exports.validateArticle = function (article) {
    if (article.articleName === '' || article.articleName === undefined) {
        return false;
    }else {
        //console.log(emailRegrex.test(String(user.email).toLowerCase()));
        return true;
    }
};

module.exports.validateFaculty = function (faculty) {
    if (faculty.facultyName === '' || faculty.facultyName === undefined || faculty.facultyDesc === '' || faculty.facultyDesc === undefined){
      return false;
  }else{
      return true;
  }
};

module.exports.validateCategory = function (category) {
  if (category.categoryName === '' ||category.categoryName === undefined || category.categoryDesc === '' || category.categoryDesc === undefined){
      return false;
  }else{
      return true;
  }
};
module.exports.validateUpDownVote = function (categoryId, upDownVote) {
  if (categoryId === '' || categoryId === undefined || upDownVote === '' || upDownVote === undefined){
      return false;
  }else{
      return true;
  }
};