
const urlB64ToUint8Array = base64String => {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4)
  const base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
  const rawData = atob(base64);
  const outputArray = new Uint8Array(rawData.length);
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  return outputArray
};
// saveSubscription saves the subscription to the backend
const saveSubscription = async (subscription,appId) => {
  let dataBody = JSON.stringify({
    subscription :subscription,
    appId: appId
  });
  console.log("saveSubcription is called");
  const SERVER_URL = 'http://localhost:8080/ws/save-subscription';
  const response = await fetch(SERVER_URL, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: dataBody,
  });
  return response.json()
};

const getKey = async (appId) => {
  console.log("getKey is called");
  const SERVER_URL = 'http://localhost:8080/ws/getPublicKeyByAppID';
  let dataBody = JSON.stringify({
    appId: appId
  });
  const response = await fetch(SERVER_URL, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: dataBody,
  });
  return response.json()
};

async function subcribe(sw,appId){
    let result = await getKey(appId);
    console.log(result);
    // This will be called only once when the service worker is activated.
    console.log("triggered event subcribe");
    try {
      const applicationServerKey = urlB64ToUint8Array(
          result.pk
      );
      const options = { applicationServerKey, userVisibleOnly: true }
      const subscription = await sw.pushManager.subscribe(options)
      const response = await saveSubscription(subscription,appId);


    } catch (err) {
      console.log('Error', err)
    }
}



self.addEventListener('push', function(event) {
  const options = {
    body: event.data.json().body,
    icon: event.data.json().icon,
    data: event.data.json().data,
  };
  if (event.data) {
    event.waitUntil(
      self.registration.showNotification(event.data.json().title, options)
    );
  } else {
    console.log("Push event but no data");
  }
  console.log(event.data.json())
});

//Trigger once when user click on notification
self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  if (event.notification.data !== null) {
    clients.openWindow(event.notification.data.url);
  }
  console.log(event.notification);
});
