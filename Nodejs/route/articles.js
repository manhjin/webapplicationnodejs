﻿const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const Article = require('../model/article');
const Comment = require('../model/comment');
const config = require("../config/database");
const validate = require('../middleware/validate');
const path = require('path');
const moment = require('moment');
const multer = require('multer');
let mammoth = require("mammoth");
var docx = require('../model/readDocx');
const moment_tz = require('moment-timezone');
const Users = require('../model/user');

const Comments = require('../model/comment');
const mailer = require('../model/mailer');
const closure = require('../model/closure');
//moment.locale("vi");

const fs = require('fs');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        closure.getCurrentActive(async (err, closureId) => {
            let uploadTo = './public/uploads/' + closureId;
            console.log(closureId);
            console.log(fs.existsSync(uploadTo));
            console.log(file);
            if (!fs.existsSync(uploadTo)) {
                await fs.mkdirSync(uploadTo);
                console.log(uploadTo);
                cb(null, uploadTo);
            }
            else {
                console.log(uploadTo);
                cb(null, uploadTo);
            }

        });

    },
    filename: function (req, file, cb) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();
        cb(null, Date.now() + '.'+outputLast);
    }
});
const uploadFolder = multer({ storage: storage });

//upload file
router.post('/uploadFile', uploadFolder.single('fileName'),passport.authenticate('jwt', {session:false}), (req, res, next) => { // ở đây tên attribute file gửi lên phải là fileName
    // docx.extract(req.file.path).then(function (res, err) {
    //     if (err) {
    //         console.log(err);
    //     }
    //     console.log(res);
    //
    // });
    // mammoth.convertToHtml({path: req.file.path})
    //     .then(function(result){
    //         let html = result.value; // The generated HTML
    //         console.log(html);
    //         let messages = result.messages; // Any messages, such as warnings during conversion
    //         console.log(messages)
    //     })
    //     .done();
    console.log(req.file);
    console.log(req.body);
});
router.post('/upload',(req,res,next)=>{
    closure.getCurrentActive((err,closureId)=>{
        if (err) res.json({err:err});
        else{
            uploadTo = uploadTo + closureId;
            next();
        }
    })
});

router.post('/testClosure', (req, res, next) => {
    closure.getCurrentActiveEdit(req.body.closure_id,(err, curentId) => {
        if (err) {
            res.json({ success: false, msg: err });
        }
        else {
            res.json({ data: curentId })

        }
    })
});
//tao moi bai dang
router.post('/upload/createNewArticle',uploadFolder.array('files'),passport.authenticate('jwt', {session:false}), (req, res, next) => {
    //console.log(uploadTo);
    if (req.user.role === 1 || req.user.role === 3 || req.user.role === 0 || req.user.role === 2) {
        Article.docxToHTML(req.files,(err,html)=>{
            if (err) res.json({code: -1, msg:'Thiếu file docx trong lúc tạo bài mới'});
            else {
                closure.getCurrentActive((err, curentId) => {

                    if (err) {
                        res.json({ success: false, msg: err });
                    } else {
                        let newArticle = new Article({
                            articleName: req.body.articleName,
                            articleImage: Article.getAllImage(req.files),
                            articleContent: html,
                            created_At: moment().format("MMMM Do YYYY, h:mm:ss a"),
                            created_By: req.user._id,
                            articleCategory: req.body.articleCategory,
                            categoryName: req.body.categoryName,
                            filename: Article.getDoc(req.files),
                            closure_id:curentId
                        });
                        if (validate.validateArticle(newArticle)) {
                            Article.insertUploadedFile(newArticle, (err, article) => {
                                if (err) {
                                    res.json({
                                        success: false,
                                        code: 512,
                                        msg: 'Failed to post',
                                        detail: err
                                    });
                                }
                                else {
                                    res.json({
                                        success: true,
                                        msg: "Thành công",
                                        code: 201,
                                        articleInformation: {
                                            article: article
                                        }
                                    });
                                    console.log(req.body.articleFaculty);
                                    Users.getCoordinatorByFaculty(req.body.articleFaculty, (err, coordi) => {
                                        if (err) throw err;
                                        else {
                                            var arti = Object.values(article);
                                            console.log(arti);
                                            let articleid = arti[0]._id;
                                            var result = Object.values(coordi);
                                            let user = result[0].username;
                                            let email = result[0].email;
                                            let postemail = req.body.postemail;
                                            let userpost = req.body.person;
                                            //console.log(result);
                                            const users = [{
                                                name: user,
                                                email: email,
                                                articleid: articleid,
                                                userpost: userpost,
                                                postemail:postemail                                           
                                            }];
                                            console.log(users);

                                            mailer.sendEmailNewArticles(users);
                                        }
                                    });
                                }
                            });
                        } else {
                            res.json({
                                success: false,
                                msg: 'Không thành công, vui lòng kiểm tra lại các trường điều kiện',
                                code: 404
                            });
                        }
                    }
                });
            }
        });
    }
    else {
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
});



//router.post('/getArticleById', passport.authenticate('jwt', {session:false}),(req,res,next)=> {
//    let articleId = req.body.articleId;

//});



router.post('/callSendMailEditArticle', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const users = [{
        username: req.body.username,
        email: req.body.email,
        articleId: req.body.articleId

    }];
    mailer.sendEmailEditArticles(users);
});
//lấy thông tin tất cả aricle
router.post('/info', (req, res, next) => {
    let facultyId = req.body.facultyId;

    Article.find().populate({ path: 'articleCategory', populate: { path: "belongto_faculty", match: { _id: facultyId } } }).populate("created_By").exec(function (err, article) {
  
        return res.json({ article: article });
        
    });
});


router.post('/searchArticle', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    let articleSearch = req.body.articleSearch;
    Article.findArticle(articleSearch, (err, article) => {
        if (err) {
            res.send({ code: 404, msg: 'Có lỗi trong quá trình thực hiện thao tác, vui lòng thử lại sau!' });
        } else {
            res.send({ code: 300, data: article });
        }
    });
});

router.post('/getUsersArticle', (req, res, next) => {
    let id = req.body.id;

    Article.getUserArticle(id, (err, articles) => {
        if (err) throw err;

        if (!articles) {
            return res.json({ success: false, msg: "Khong tim thay" });
        } else {
            return res.json({ articles });
        }

    });
});

// coordinator set status cua bai dang
router.post('/editArticleStatus', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    if (req.user.role === 0 || req.user.role === 3) {
        let updateArticleStat = {
            status: req.body.status,    
            articleId: req.body.articleId,
            updatedBy: req.body.updatedBy,
            updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a")
        };
        console.log(updateArticleStat);

        if (updateArticleStat.articleId) {
            Article.updateArticleStatus(updateArticleStat, (err, article) => {
                if (err) {
                    res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau!', err: err });
                } else {
                    res.json({ code: 300, msg:"Success", data: updateArticleStat });
                }
            });
           
        } else {
            res.json({ code: -1, msg: 'Bạn cần phải chọn 1 bài đăng trước khi thực hiện update!' });
        }
    } else {
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
});

//student chinh sua bai dang
router.post('/updateArticle',uploadFolder.array('files'),passport.authenticate('jwt', {session:false}), (req, res, next) => {
    if (req.user.role === 1 || req.user.role === 0 ) {
        Article.docxToHTML(req.files, (err, html) => {
            if (err) res.json({ code: -1, msg: 'Thiếu file docx trong lúc tạo bài mới' });
            else {
                closure.getCurrentActiveEdit(req.body.closure_id,(err, curentId) => {

                    if (err) {
                        res.json({ success: false, msg: err });
                    } else {
                        let newEditArticle = {
                            articleId: req.body.articleId,
                            articleName: req.body.articleName,
                            articleImage: Article.getAllImage(req.files),
                            articleContent: html,
                            updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a"),
                            updatedBy: req.user._id,
                            filename: Article.getDoc(req.files),
                            closure_id: curentId
                        };
                        if (validate.validateArticle(newEditArticle)) {
                            Article.editArticle(newEditArticle, (err, article) => {
                                if (err) {
                                    res.json({
                                        success: false,
                                        code: 512,
                                        msg: 'Failed to post',
                                        detail: err
                                    });
                                }
                                else {
                                    res.json({
                                        success: true,
                                        msg: "Thành công",
                                        code: 201,
                                        articleInformation: {
                                            article: article
                                        }
                                    });
                                    Users.getCoordinatorByFaculty(req.body.articleFaculty, (err, coordi) => {
                                        if (err) throw err;
                                        else {
                                            var arti = Object.values(article);
                                            console.log(arti);
                                            let articleid = arti[0]._id;
                                            var result = Object.values(coordi);
                                            let user = result[0].username;
                                            let email = result[0].email;
                                            let postemail = req.body.postemail;
                                            let userpost = req.body.person;
                                            //console.log(result);
                                            const users = [{
                                                name: user,
                                                email: email,
                                                articleid: articleid,
                                                userpost: userpost,
                                                postemail: postemail
                                            }];
                                            //console.log(users);

                                            mailer.sendEmailEditArticles(users);
                                        }
                                    });
                                }
                            });
                        } else {
                            res.json({
                                success: false,
                                msg: 'Không thành công, vui lòng kiểm tra lại các trường điều kiện',
                                code: 404
                            });
                        }
                    }
                });
            }
        });
    }
    else {
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
});


router.post('/getPostByCategory',passport.authenticate('jwt', {session:false}),(req,res,next)=>{
    let categoryId = req.body.categoryId;
    let paginationOption = {
        page: parseInt(req.body.page) || 0,
        limit: parseInt(req.body.limit) || 3
    };

    Article.getArticleByCategory(paginationOption,categoryId, (err, article) => {
        if (err) {
            res.send({ code: 404, success: false, msg: 'Có vấn đề xảy ra với server. Vui lòng thử lại sau!' });
        } else {
            if (article.length > 0) {
                console.log(article);
                res.json({ code: 300, success: true, data: article });
            } else {
                res.json({ code: -1, success: false, msg: 'Hết dữ liệu trong hệ thống' });
            }
    
        }
    });
});

router.post('/getPostByFaculty', (req, res, next) => {
    let facultyId = req.body.facultyId;

    let pageOptions = {
        page: parseInt(req.body.page) || 0,
        limit: parseInt(req.body.limit) || 3
    };

    Article.find({ status: "2" }).populate('created_By').populate({
        path: 'articleCategory',
        populate: {
            path: "belongto_faculty",
            match: { _id: facultyId }
            
        }
    }).sort({ _id: -1 }).skip(pageOptions.page * pageOptions.limit)
        .limit(pageOptions.limit).exec(function (err, floorplan) {
            if (err) { return res.send(err); }
            if (!floorplan) { return res.status(401).json(); }
            if (floorplan.length > 0) {
                for (var i = 0; i < floorplan.length; i++) {
                    if (floorplan[i].articleCategory.belongto_faculty == null) {
                        delete floorplan[i];
                        console.log(floorplan[i]);

                    }
                }
                res.json({ code: 300, success: true, data: floorplan });
            } else {
                res.json({ code: -1, success: false, msg: 'Hết dữ liệu trong hệ thống' });
            }
        

    });
});
//get article by nguoi tao
router.post('/getArticleInfo',passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.body.id;
    Article.getArticleById(id, (err, article) => {
        if (err) throw err;
        if (!article) {
            return res.json({ success: false, msg: "Khong tim thay" });
        } else {     
            
            var responseData = {
                _id: article._id,
                upVote : article.upVote.length,
                downVote: article.downVote.length,
                articleName: article.articleName,
                closure_id: article.closure_id,
                articleImage: article.articleImage,
                articleContent:article.articleContent,
                filename: article.files,
                upVoteData: article.upVote,
                categoryName: article.categoryName,
                downVoteData: article.downVote,
                created_By: article.created_By,
                articleCategory: article.articleCategory,
                comments: article.comment,
                created_At: article.created_At,
                subTime: moment(article.created_At, "MMMM Do YYYY, h:mm:ss a").fromNow(),
                upDowned: Article.upOrDownVote(article.upVote, article.downVote, req.user._id),
                status: article.status
            };
            //Comment.getCommentById(article.comment._id, (err, cmt) => {
            //    if (err) throw err;

            //    article.comment = cmt;
            //    console.log(cmt);
            //    return 1;
            //}),
          
            return res.json({ article: responseData });
        }

    });
});
//get article by coordinator and student comment
router.post('/getArticleApprovement', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    let id = req.body.id;
    Article.getArticleApprovementById(id, (err, article) => {
        if (err) throw err;
        if (!article) {
            return res.json({ success: false, msg: "Khong tim thay" });
        } else {

            var responseData = {
                _id: article._id,
                upVote: article.upVote.length,
                downVote: article.downVote.length,
                articleName: article.articleName,
                articleImage: article.articleImage,
                articleContent: article.articleContent,
                closure_id: article.closure_id,
                filename: article.files,
                upVoteData: article.upVote,
                categoryName: article.categoryName,
                downVoteData: article.downVote,
                created_By: article.created_By,
                articleCategory: article.articleCategory,
                comments: article.comment,
                created_At: article.created_At,
                subTime: moment(article.created_At, "MMMM Do YYYY, h:mm:ss a").fromNow(),
                upDowned: Article.upOrDownVote(article.upVote, article.downVote, req.user._id),
                status: article.status
            };
            //Comment.getCommentById(article.comment._id, (err, cmt) => {
            //    if (err) throw err;

            //    article.comment = cmt;
            //    console.log(cmt);
            //    return 1;
            //}),

            return res.json({ article: responseData });
        }

    });
});

router.post('/getIndex',passport.authenticate('jwt', {session:false}),(req,res,next)=>{
    let paginationOption = {
        page: parseInt(req.body.page) || 0,
        limit: parseInt(req.body.limit) || 3
    };
    Article.getAllArticlesOrderByCreatedDate(paginationOption, (err, articles) => {
        if (err) {
            console.log(err);
            res.send({ code: 404, success: false, msg: 'Error with server! Try again Later' });
        } else {
            if (articles.length > 0) {
                console.log(articles);
                res.json({ code: 300, success: true, data: articles });
            } else {
                res.json({ code: -1, success: false, msg: 'Out of data' });
            }
        }
    });
});


router.post('/addComment', uploadFolder.array('files'), (req, res, next) => {
    let commentId = req.body.commentId;
    if (commentId) { // sub - comment
        let comment = new Comments({
            comment: req.body.comment_content,
            commented_At: moment().format("MMMM Do YYYY, h:mm:ss a"),
            comment_user: req.body.comment_user,
            image: Comment.getAllImage(req.files),
            cmt_status: req.body.status
        });
        console.log(comment);

        Comments.addComment(comment, (err, cmt) => {
            if (err) res.json({ code: 404, msg: 'Có vấn đề xảy ra trong quá trình thực hiện',err:err });
            else {
                if (comment) {
                    Comment.addSubComment(commentId,cmt._id, (err, article) => {
                        if (err) res.json({ code: 404, msg: 'Có vấn đề xảy ra', err: err  });
                        else {
                            res.json({ code: 300, msg: 'OK' });
                        }
                    });
                }
            }
        });
    } else { // comment
        let comment = new Comments({
            comment: req.body.comment_content,
            commented_At: moment().format("MMMM Do YYYY, h:mm:ss a"),
            comment_user: req.body.comment_user,
            image: Comment.getAllImage(req.files),
            cmt_status: req.body.status

        });
        console.log(comment);
       
        Comments.addComment(comment, (err, cmt) => {
            if (err) res.json({ code: 404, msg: 'Có vấn đề xảy ra trong quá trình thực hiện', err: err  });
            else {
                if (comment) {
                    Article.addComment(cmt._id, req.body.articleId, (err, article) => {
                        if (err) res.json({ code: 404, msg: 'Có vấn đề xảy ra', err: err  });
                        else {
                            res.json({ code: 300, msg: 'OK' });
                        }
                    });
                }
            }
        });
    }
});
module.exports = router;