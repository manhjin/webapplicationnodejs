const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../model/user');
const Article = require('../model/article');
const config = require("../config/database");
const validate = require('../middleware/validate');
const path = require('path');
const moment = require('moment');
var bcrypt = require('bcryptjs');
const multer = require('multer');
var docx = require('../model/readDocx');

const moment_tz = require('moment-timezone');
moment.locale("vi");
const storage = multer.diskStorage({
   destination: function (req,file,cb) {
       cb(null, './uploads/');
   },
    filename: function (req,file,cb) {
        cb(null, Date.now()+'.docx');
    }
});
const uploadFolder = multer({ storage: storage });

//Register
router.post('/register', (req, res, next) => {
    let newUser = new User({
        lastName: req.body.lastName,
        firstName: req.body.firstName,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        createdAt: moment().format("MMMM Do YYYY, h:mm:ss a"),
        role: req.body.role,
        userFaculty: req.body.userFaculty,
        status:1
    });
    //console.log(req.body);
    if (validate.validateUser(newUser)) {
        User.addUser(newUser, (err, user) => {
            if (err) {
                res.json({
                    success: false,
                    code: 512,
                    msg: 'Failed to register user',
                    detail: err
                });
            } else {
                res.json({
                    success: true,
                    msg: "Tạo tài khoản thành công",
                    code: 201,
                    userInformation: {
                        username: user.username,
                        email: user.email,
                        role: user.role
                    }
                });
            }
        });
    } else {
        res.json({
            success: false,
            msg: 'Đăng kí không thành công, vui lòng kiểm tra lại các trường điều kiện',
            code: 404,
            dataPost: newUser
        });
    }
});


//Login
router.post('/login', (req, res, next) => {   
  const username = req.body.username;
  const password = req.body.password;
    User.getUserByUsername(username,(err,user)=>{
      if(err) throw err;
      if(!user){
        return res.json({success:false,msg:"Cannot Find User"})
      }
        User.comparePassword(password, user.password, function (err, result) {
            if (err) throw err;

            if (result) {
                if (user.status == 1) {
                    let userToken = {
                        _id: user.id,
                        email: user.email,
                        role: user.role,
                        username: user.username,
                    };
                    const token = jwt.sign(userToken, config.secret, {expiresIn: 86400 });
                    res.json({
                        success: true,
                        msg: "Login Successfully",
                        token: 'Bearer ' + token,
                        userInformation: {
                            id: user.id,
                            username: user.username,
                            email: user.email,
                            role: user.role,
                            userFaculty: user.userFaculty
                        }
                    });
                }
                else {
                    return res.json({ success: false, msg: "Account has been inactive" });
                }
            } else {
                return res.json({ success: false, msg: "Sai mật khẩu!" });
            }
        });
    });
});

//delete
router.delete('/del/:username', (req, res,next) => {          
    User.findOneAndRemove({username: req.params.username}, (err) => {
        if (err) {
          req.flash("error", err);
        }
    
        req.flash("success", "Your account has been deleted.");
      });
});
//lấy thông tin tất cả user
router.get('/info', (req, res, next) => {
    User.find().populate('userFaculty').then((user) => {
        res.send({user});
      }, (e) => {
        res.status(400).send(e);
      });
});

//chinh sua user
router.post('/updateUser', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    if (req.user.role === 0 ) {
        let updateUser = {
            username: req.body.username,
            email: req.body.email,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            role: req.body.role,
            updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a"),
            updatedBy: req.body.updatedBy,
            userId: req.body.userId,
            userFaculty: req.body.userFaculty,
            status: req.body.status

        };

        if (updateUser.userId) {
            //if (validate.validateUser(updateUser) === true) {

                User.editUser(updateUser, (err, user) => {
                    if (err) {
                        console.log(err);
                        res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau' });
                    } else {
                        res.json({ code: 300, data: updateUser });
                    }
                });
            //} else {
            //    res.json({ code: -1, msg: 'Các trường như userName và email không thể bỏ trống' });
            //}
        } else {
            res.json({ code: -1, msg: 'Bạn cần phải chọn 1 user trước khi thực hiện update' });
        }
    } else {
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
});

//getuser info by id
router.post('/getUserInfo', (req, res, next) => {
    const id = req.body.id;

    User.getUserById(id, (err, user) => {
        if (err) throw err;

        if (!user) {
            return res.json({ success: false, msg: "Khong tim thay user" });
        } else {
            return res.json({ user });
        }

    });
});

router.post('/getGuestByFaculty', (req, res, next) => {
    const id = req.body.id;

    User.getGuestByFaculty(id, (err, user) => {
        if (err) throw err;

        if (!user) {
            return res.json({ success: false, msg: "Khong tim thay user" });
        } else {
            return res.json({ user });
        }

    });
});
router.post('/upDownVoteArticle',passport.authenticate('jwt', {session:false}),(req,res,next)=>{
   let userId = req.user._id;
   let articleId = req.body.articleId;
   let upDown = req.body.upDown;
   if (validate.validateUpDownVote(articleId,upDown) === true){
       if (upDown === 1){
           Article.upVoteArticle(userId, articleId, (err, article) => {
               if (err) res.json({ code: 404, msg: 'Error Problem', detail: err });
               else {
                   res.json({ code: 300, success: true, data: article });
               }
           });
       } else{
           Article.downVoteArticle(userId, articleId, (err, article) => {
               if (err) res.json({ code: 404, msg: 'Error Problem hiện' });
               else {
                   console.log(article)
                   res.json({ code: 300, success: true , data: article});
               }
           });

       }
   }else{
       res.json({ success: false, code: -1, mgs: 'Article Id is required!' });
   }

});

router.get('/testGet',(req,res,next)=>{
    console.log(req.body);
});

//lay profile user hien dang dang nhap
router.get('/profile',passport.authenticate('jwt', {session:false}), (req, res, next) => {          
  res.json({user: req.user});
});
module.exports = router;