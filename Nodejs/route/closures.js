﻿const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../model/user');
const config = require("../config/database");
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");

const Closure = require('../model/closure');


router.post('/createClosure', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    Closure.getCurrentActive((isAbleToCreate,closure_id)=>{
        if (isAbleToCreate !== null){
            if (req.user.role === 0) {
                let closure = new Closure({
                    created_At: req.body.created_At,
                    expired_At: req.body.expired_At,
                    expired_Edit: req.body.expired_Edit,
                    created_By: req.body.created_By,
                    username: req.body.username,
                    status:"1"
                });
                //if (validate.validateCategory(closure) === true) {
                Closure.addNewClosure(closure, (err, wtf) => {
                    if (err) {
                        res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau!', detail: err });
                    } else {
                        res.json({ code: 300, msg: 'Thêm mới Closure thành công!', data: closure });
                    }
                });
                //} else {
                //    res.json({ code: -1, msg: 'Lỗi validate !' });
                //}
            } else {
                res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
            }
        }else{
            res.json({success: false, msg: 'Hiện tại đang có closure date đang active, vui lòng đóng hoặc chờ hết hạn'})
        }
    })
});
router.post('/getClosureInfo', (req, res, next) => {
    const id = req.body.id;

    Closure.getClosureById(id, (err, closure) => {
        if (err) throw err;

        if (!closure) {
            return res.json({ success: false, msg: "Khong tim thay closure" });
        } else {
            return res.json({ closure });
        }

    });
});
router.post('/getClosure', (req, res, next) => {

    Closure.getOne((err, closure) => {
        if (err) throw err;

        if (!closure) {
            return res.json({ success: false, msg: "Closure not found" });
        } else {
            return res.json({ data:closure });
        }

    });
});
router.post('/updateClosure', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    if (req.user.role === 0) {
        let updateClosure = {
            created_At: req.body.created_At,
            expired_At: req.body.expired_At,
            expired_Edit: req.body.expired_Edit,
            status: req.body.status,
            closureid: req.body.closureid,
            updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a")
        };

        if (updateClosure.closureid) {
            Closure.editClosure(updateClosure, (err, closure) => {
                if (err) {
                    console.log(err);
                    res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau' });
                } else {
                    res.json({ code: 300, msg: "OK", data: closure });
                }
            });


        } else {
            res.json({ code: -1, msg: 'Bạn cần phải chọn 1 ' });
        }
    } else {
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
    
});
//lấy thông tin tất cả closure
router.get('/info', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    Closure.find().then((closure) => {
        res.send({ closure });
    }, (e) => {
        res.status(400).send(e);
    });
});
module.exports = router;

