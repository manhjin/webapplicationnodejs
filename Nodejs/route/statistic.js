const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../model/user');
const Article = require('../model/article');
const config = require("../config/database");
const ObjectId = mongoose.Types.ObjectId;
const faculty = require('../model/Faculty')



router.post('/countAllArticle', (req, res, next) => {
    const start = req.body.start;
    const end = req.body.end;

    Article.aggregate([{
        $match: {
            status: { $gte: 1 },
            created_At: {}
        }
    },
    {
        $group: {
            _id: { articleFaculty: "$articleFaculty" },
           
            count: { $sum: 1 }
        }
    }], function (err, data) {
        if (err) {
            res.send(err);
            return;
        }
        console.log(data);
        res.send({ data });
    });

});

router.post('/statistic',(req,res,next)=>{
    let closure_id = req.body.closureid || null;
    console.log(closure_id)
   Article.totalArticleByClosure(closure_id,(error,total)=>{
       if (error) res.json({error:error});
       else{
           Article.statistic(closure_id,(err,results)=>{
               if (err) res.json({err:err});
               else {

                   let dataResponse = {
                       total : total,
                       data :[]
                   };
                   for (const result of results) {

                      let data = {
                          id : result._id,
                          number: result.points,
                          percentage : (result.points / total) * 100
                      };
                      dataResponse.data.push(data)
                   };
                   res.json({data:dataResponse})
               }
           });
       }
   })
});

router.post('/statisticByFaculty',(req,res,next)=>{
    let closure_id = req.body.closureid || null;
    Article.AllArticleByClosureDate(closure_id,(error,total)=>{
        if (error) res.json({error:error});
        else{
            Article.statisticByFaculty(closure_id,(err,results)=>{
                if (err) res.json({err:err});
                else {

                    let dataResponse = {
                        total : total,
                        data :[]
                    };
                    for (const result of results) {

                        let data = {
                            id : result._id,
                            number: result.points,
                            percentage : (result.points / total) * 100
                        };
                        dataResponse.data.push(data)
                    };
                    res.json({data:dataResponse})
                }
            });
        }
    })
});


module.exports = router;