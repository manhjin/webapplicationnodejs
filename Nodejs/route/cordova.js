const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../model/user');
const config = require("../config/database");
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const product = require('../model/cordova_product');

moment.locale("vi");

router.post('/index',passport.authenticate('jwt', {session:false}),(req,res,next)=>{
    let paginationOption = {
        page: parseInt(req.body.page) || 0,
        limit: parseInt(req.body.limit) || 10
    };
    product.getIndexNoFilter(paginationOption,(error, products)=>{
        if (error) {
            console.log(error);
            res.send({code:404,success:false,msg:'Có vấn đề xảy ra với server. Vui lòng thử lại sau!'})
        }else{
            if (products.length >0){
                res.json({code:300, success: true, data:products})
            }else{
                res.json({code:-1, success: false, msg:'Hết dữ liệu trong hệ thống'})
            }
        }
    })
});

router.post('/addProduct',passport.authenticate('jwt', {session:false}), (req,res,next)=>{
   let newProduct = new product({
       productName: req.body.productName,
       productDesc: req.body.productDesc,
       productOwner: req.user._id,
       price : req.body.price,
       image: req.body.image
   });
    product.addNewProducts(newProduct,(err,product)=>{
      if (err){
          res.send({code:404, success: false, msg:'Có vấn đề xảy ra với server, vui lòng thử lại sau!'})
      }else{
          res.send({code:300, success: true, data: product})
      }
    })
}); 

module.exports = router;