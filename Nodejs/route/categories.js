const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../model/user');
const config = require("../config/database");
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const Category = require('../model/category');

router.post('/createNewCategory',passport.authenticate('jwt', {session:false}),(req,res,next)=>{
    if (req.user.role === 3 || req.user.role === 0){
        let category = new Category({
            categoryName: req.body.categoryName,
            categoryDesc: req.body.categoryDesc,
            created_By: req.user._id,
            belongto_faculty: req.body.belongto_faculty
        });
        if (validate.validateCategory(category) === true){
            Category.addNewCategory(category, (err, newCate) => {
                if (err) {
                    res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau!', detail: err });
                } else {
                    res.json({ code: 300, msg: 'Thêm mới category thành công!', data: category });
                }
            });
        } else{
            res.json({ code: -1, msg: 'Lỗi validate, không được để trống các trường như Category Name, Category Desc!' });
        }
    }else{
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
});

router.get('/getAllCategory', passport.authenticate('jwt', {session:false}),(req,res,next)=>{
    Category.getAllCategory((err, categories) => {
        if (err) {
            res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau' });
        } else {
            res.json({ code: 300, data: categories });
        }
    });
});
router.post('/getCategoryByFaculty', (req, res, next) => {
    let facultyId = req.body.facultyId;
    Category.getCategoryByFaculty(facultyId, (err, Faculty) => {
        if (err) {
            res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau' });
        } else {
            res.json({ code: 300, data: Faculty });
        }
    });
})
router.post('/searchCategory',passport.authenticate('jwt', {session:false}),(req,res,next)=>{
    let categorySearch = req.body.categorySearch;
    Category.findCategory(categorySearch,(err,categories)=>{
        if (err){
            res.send({ code: 404, msg: 'Có lỗi trong quá trình thực hiện thao tác, vui lòng thử lại sau!' });
        } else{
            res.send({ code: 300, data: categories });
        }
    });
});
//getuser category by id
router.post('/getCategoryInfo', (req, res, next) => {
    const id = req.body.id;

    Category.getCategoryById(id, (err, category) => {
        if (err) throw err;

        if (!category) {
            return res.json({ success: false, msg: "Khong tim thay" });
        } else {
            return res.json({ category });
        }

    });
});
router.post('/editCategory',passport.authenticate('jwt', {session:false}),(req,res,next)=>{
    if (req.user.role === 0 || req.user.role === 3){
        let updateCategory = {
            categoryName: req.body.categoryName,
            categoryDesc: req.body.categoryDesc,
            categoryId : req.body.categoryId,
            updatedBy: req.user._id,
            updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a")
        };

        if (updateCategory.categoryId){
            if (validate.validateCategory(updateCategory) === true) {

                Category.updateCategory(updateCategory, (err, category) => {
                    if (err) {
                        res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau' });
                    } else {
                        res.json({ code: 300, data: updateCategory });
                    }
                });
            }else {
                res.json({code: -1, msg:'Các trường như category name và category description không thể bỏ trống'});
            }
        } else {
            res.json({ code: -1, msg: 'Bạn cần phải chọn 1 category trước khi thực hiện update' });
        }
    } else{
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
});
router.post('/getCategoryByFaculty', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    let facultyId = req.body.facultyId;
    Category.getCategoryByFaculty(facultyId, (err, category) => {
        if (err) {
            res.json({ code: 404, msg: 'Có lỗi trong quá trình thực hiện thao tác. Vui lòng thử lại sau' });
        } else {
            res.json({ code: 300, data: category });
        }
    });
});

router.post('/subcribeCategory',passport.authenticate('jwt', { session: false }), (req, res, next)=>{

});

module.exports = router;