﻿const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require("../config/database");
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const Faculty = require('../model/Faculty');

router.post('/createNewFaculty', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    if (req.user.role === 0) {
        let faculty = new Faculty({
            facultyName: req.body.facultyName,
            facultyDesc: req.body.facultyDesc,
            created_By: req.user._id
        });
        if (validate.validateFaculty(faculty) === true) {
            Faculty.addNewFaculty(faculty, (err, newFaculty) => {
                if (err) {
                    res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau!', detail: err });
                } else {
                    res.json({ code: 300, msg: 'Thêm mới category thành công!', data: faculty });
                }
            });
        } else {
            res.json({ code: -1, msg: 'Lỗi validate, không được để trống các trường như Faculty Name, Faculty Desc!' });
        }
    } else {
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
});
router.post('/editFaculty', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    if (req.user.role === 0 || req.user.role === 3) {
        let updateFaculty = {
            facultyId: req.body.facultyId,
            facultyName: req.body.facultyName,
            facultyDesc: req.body.facultyDesc,
            updatedBy: req.user._id,
            updatedAt: moment().format("MMMM Do YYYY, h:mm:ss a")
        };

        if (updateFaculty.facultyId) {
            if (validate.validateFaculty(updateFaculty) === true) {

                Faculty.updateFaculty(updateFaculty, (err, faculty) => {
                    if (err) {
                        res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau' });
                    } else {
                        res.json({ code: 300, data: faculty, msg: "Success" });
                    }
                });
            } else {
                res.json({ code: -1, msg: 'Các trường như faculty name và faculty description không thể bỏ trống' });
            }
        } else {
            res.json({ code: -1, msg: 'Bạn cần phải chọn 1 faculty trước khi thực hiện update' });
        }
    } else {
        res.status(401).json({ code: 401, msg: 'Bạn không có đủ quyền để thực hiện hành động này!' });
    }
});
router.get('/getAllFaculty', (req, res, next) => {
    Faculty.getAllFaculty((err, faculty) => {
        if (err) {
            res.json({ code: 404, msg: 'Có lỗi xảy ra trong quá trình thực hiện thao tác, vui lòng thử lại sau' });
        } else {
            res.json({ code: 300, data: faculty });
        }
    });
});

//getuser category by id
router.post('/getFacultyInfo', (req, res, next) => {
    const id = req.body.id;

    Faculty.getFacultyById(id, (err, faculty) => {
        if (err) throw err;
        else{
            return res.json({ faculty });
        }

    });
});
module.exports = router;