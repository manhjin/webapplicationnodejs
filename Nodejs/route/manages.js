const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require("../config/database");
const validate = require('../middleware/validate');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
moment.locale("vi");
const manage = require('../model/manage');
const Closure = require('../model/closure');

router.post('/downloadArticles',passport.authenticate('jwt', {session:false}), (req, res, next) => {
    Closure.getCurrentActive((err,closureId)=>{
        if (err) res.json({err:err});
        else{
            manage.zipAllBySeason(closureId,(err,linkFolder,)=>{
                if (err) res.json({err:err});
                else{
                    res.json({success:linkFolder})
                }
            });
        }
    })

});

module.exports = router;