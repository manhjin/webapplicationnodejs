const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment = require('moment');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
moment_tz.locale("vi");
const productSchema = mongoose.Schema({
    productName:{
        type:String,
        required:true
    },
    productDesc:{
        type: String
    },
    image:{
        type:Object,
        required:true
    },
    price:{
        type:Number,
        required:true
    },
    createdAt: { type: String, default:vi_timezone.format("LLLL") },
    productOwner:{
        type: ObjectId,
        ref:'Users'
    }
});

const ProductSchema = module.exports = mongoose.model('Product', productSchema);

module.exports.getIndexNoFilter = function (pageOptions,callback) {
    ProductSchema.find()
        .skip(pageOptions.page*pageOptions.limit)
        .limit(pageOptions.limit).exec(callback)
};

module.exports.addNewProducts = function (newProduct, callback) {
    newProduct.save(callback);
};