const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const moment_tz = require('moment-timezone');
mongoose.Promise = global.Promise;

const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
const moment = require('moment');
let mammoth = require("mammoth");

moment.locale("vi");


const CommentSchema = mongoose.Schema({
    comment:{
        type:String
    },
    comment_user: {
        type: ObjectId,
        ref: 'Users'
    },
    commented_At: {
        type: String
    },
    image:{
        type: Object
    },
    upVote:[{
        type:ObjectId,
        ref:'Users'
    }],
    cmt_status:{
      type:Number
      // 0: public // 1 between student and coordinator post that article only
    },
    downVote:[{
        type:ObjectId,
        ref:'Users'
    }],
    subComment:[ObjectId]
},{ collection: 'Comment' });

const Comments = module.exports = mongoose.model('Comments', CommentSchema);

module.exports.addComment = function (comment,callback) {
    comment.save(callback);
};
module.exports.addSubComment = function (commentId, subCommentId, callback) {
    Comments.findOneAndUpdate({ _id: commentId }, {
        $push: {
            subComment: subCommentId
        }
    }, callback);};

module.exports.getCommentById = function (id, callback) {
    Comments.find({ id: id }, callback).populate('comment_user');

};
module.exports.getAllImage = function (files) {
    let image = [];
    for (let file of files) {
        let arr = file.originalname.split('.');
        let outputLast = arr.pop();

        console.log(outputLast);
        if (outputLast === 'png' || outputLast === 'jpg' || outputLast === 'jpeg' || outputLast === 'gif') {
            console.log(file.path);
            image.push(file.path);
        }
    }
    return image;
};
