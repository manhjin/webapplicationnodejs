const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;
const ObjectIdLog = mongoose.Types.ObjectId;
const bcrypt = require('bcryptjs');
const moment_tz = require('moment-timezone');
const vi_timezone = moment_tz().tz("Asia/Ho_Chi_Minh");
vi_timezone.locale("vi");
const moment = require('moment');
moment.locale("vi");
const updatedSchema = mongoose.Schema({
   updatedAt: {
       type: String,
       //default: moment().format("LLLL")

   },
   updatedBy:{
       type:ObjectId
   }
},{_id:false});

const categorySchema = mongoose.Schema({
   categoryName:{
       type:String,
       required:true
   },
   categoryDesc:{
       type:String,
       required:true,
       text: true
   },
   status:{
     type:Number,
     default: 1 // 1: Active, 0: Disable
   },
   followedBy:[{
       type:ObjectId
   }],
   created_At:{
       type:String,
       //default: moment().format('LLLL')
   },
   created_By:{
       type:ObjectId,
       ref:'Users',
       required: true
    },
    belongto_faculty: {
        type: ObjectId,
        ref: 'faculties',
        required: true
    },
    updated: [updatedSchema]
  
});

const Category = module.exports = mongoose.model('categories', categorySchema);


module.exports.addNewCategory = function (newCategory, callback) {
    newCategory.save(callback);
};

module.exports.getAllCategory = function (callback) {
    Category.find({}, callback).populate("belongto_faculty");
};
module.exports.findCategory = function (text,callback) {
    Category.find(
        { $text : { $search : text} },
        { score : { $meta: "textScore" } }
    ).sort({ score : { $meta : 'textScore' } }).exec(callback);
};
module.exports.getCategoryById = function (id, callback) {
    Category.findById(id, callback);
};
module.exports.updateCategory = function (updateCategory,callback) {
    console.log(updateCategory);
    Category.findOneAndUpdate({ _id: updateCategory.categoryId }, {
        $set: {
            'categoryName': updateCategory.categoryName,
            'categoryDesc': updateCategory.categoryDesc
        }, $push: {
            updated: { updatedAt: updateCategory.updatedAt, updatedBy: updateCategory.updatedBy }
        }
    }, { multi: true }, callback);
};
module.exports.getCategoryByFaculty = function (facultyId, callback) {
    Category.find({ belongto_faculty: facultyId }, callback).populate("belongto_faculty");
};
module.exports.getByFaculty = function (facultyId, callback) {
    Category.find({ belongto_faculty: facultyId }, callback);
};


