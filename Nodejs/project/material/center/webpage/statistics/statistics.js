$(document).ready(function () {
  if (localStorage.getItem("accessToken") != null && localStorage.getItem("role") == 2) {
    displayStatistic();
  } else {
    window.location.href = "../login/login.html";
  }
});
