function getParam(param) {
  var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for (var i = 0; i < url.length; i++) {
    var params = url[i].split("=");
    if (params[0] == param)
      return params[1];
  }
  return false;
}
var articleId = getParam("id");
var userId = getParam("userid");
function load() {
  $("#comment_user").val(localStorage.getItem("id"));
  $("#articleId").val(articleId);
  $("#newuseraction").html("");

  $.ajax({
    url: "http://128.199.231.68/articles/getArticleApprovement",
    type: "POST",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    data: {
      id: articleId
    },
    success: function (response) {
      if (response.status === false) {
        alert(response.msg);
      } else {

        console.log(response.article);
        if (localStorage.getItem("role") == 1 && response.article.created_By._id == userId) {
          $("#userAction").append("<li class='nav-item' role='presentation'id='newuseraction'>" +
            "<a class='nav-link' data-target='#Edit' data-toggle='modal' role='button'>" +
            "<i class='icon md-edit' aria-hidden='true'></i>Edit Article</span>" +
            "</a>" +
            "</li>");
          $("#articleName").val(response.article.articleName);
          $("#articleCategory").val(response.article.categoryName).trigger("change");
          $("#closure_id").val(response.article.closure_id);


          $("#inputTitle").val(response.article.articleName);
          var $catSelect = $('#articleStatus');
          $catSelect.selectpicker();
          $("#articleCategory option:selected").val(response.article.cate);

        } else if (localStorage.getItem("role") == 3) {
          $("#userAction").append("<li class='nav-item' role='presentation'id='newuseraction'>" +
            "<a class='nav-link' data-target='#changeStatus' data-toggle='modal' role='button'>" +
            "Change Status" +
            "</a>" +
            "</li>");
          var $statusSelect = $('#articleStatus');
          $statusSelect.selectpicker();
          switch (response.article.status) {
            case 0:
              addOption($statusSelect, 'Denied', '0');
              addOption($statusSelect, 'Pending', '1');
              addOption($statusSelect, 'Approved', '2');
              break;
            case 1:
              addOption($statusSelect, 'Pending', '1');
              addOption($statusSelect, 'Denied', '0');
              addOption($statusSelect, 'Approved', '2');
              break;
            case 2:
              addOption($statusSelect, 'Approved', '2');
              addOption($statusSelect, 'Denied', '0');
              addOption($statusSelect, 'Pending', '1');
              break;
          }
        }
        $("#article").html("");

        $("#article").append("<div class='panel'>" +
          "<div class='itemVote'>" +
          "<div class='itemUpvoteContent'>" +
          "<a id='upvote'></a>" +
          "<div class='itemUpvoteNumber'>" + (response.article.upVote - response.article.downVote) + "</div>" +
          "<a id='downvote'></a></div></div>" +
          "<div class = 'itemContent'> " +
          "<div class = 'itemUser'>r/" + response.article.categoryName + " •Posted by+ " + response.article.created_By.username +
          " " + response.article.subTime + "+</div> " +
          "<div class = 'itemTitle'>" + response.article.articleName + "</div>" +
          "<div class = 'itemDesc'><img src='http://128.199.231.68/" + response.article.articleImage + "' alt=''>" +
          response.article.articleContent + "</div>" +
          "<div class = 'itemAction' > " +
          "<a href ='#'>" +
          "<img src = '../../assets/images/icons8-comments-16.png' alt = '' >" +
          "<span> " + response.article.comments.length + " Comments </span> </a> <a href = '#' >" +
          "<img src = '../../assets/images/icons8-share-16.png' alt = '' >" +
          "<span> Share </span></a> <a href = '#' >" +
          "<img src = '../../assets/images/icons8-save-16.png' alt = '' >" +
          "<span> Save </span></a> </div> </div> </div>");
        if (response.article.upDowned === 1) {
          $('#upvote').css({
            "border-color": "orangered"
          })
        } else if (response.article.upDowned === 2) {
          $('#downVote').css({
            'background-color': '#FFFFCC'
          })
        }
        $("#comment_section").html("");
        for (var i = 0; i < response.article.comments.length; i++) {
          if (response.article.comments[i].subComment.length == 0) {
            if (response.article.comments[i].comment_user.role == 3) {
              $("#comment_section").append(
                '<div class="comment media" id="divcmt' + response.article.comments[i]._id + '">' +
                '<div class="pr-15"> <a href="#" class="avatar avatar-sm"><img src="../../../global/portraits/1.jpg" alt="..."></a></div>' +
                '<div class="comment-body media-body"><div class= "comment-title">' +
                '<div class="comment-meta float-right">' +
                '<span>' + response.article.comments[i].commented_At + '</span></div>' +
                '<a href="javascript:void(0)" class="comment-author">' + response.article.comments[i].comment_user.username + '</a><a style="color:purple"> - Coordinator</a></div >' +
                '<div class="comment-content">' +
                '<p>' + response.article.comments[i].comment + '</p>' +
                //'<div class="itemDesc"><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +

                '<div class="itemDesc" id =img' + response.article.comments[i]._id + '><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +
                //'<button type="button" class="btn btn-pure btn-default icon md-edit p-0 btn-edit"></button>' +
                //'<button type="button" class="btn btn-pure btn-default icon md-delete p-0 btn-trash"></button></div>' +
                '<div class="comment-actions text-left"> <a href="javascript:void(0)" role="button" id=reply' + response.article.comments[i]._id + ' onclick="Reply(this.id)">Reply</a>' +
                '<a href="#"><i class="icon md-chevron-down"></i></a>' +
                '<span class="pos-number mx-5">' + response.article.comments[i].status + '</span>'
                + '<a href="#"><i class="icon md-chevron-up"></i></a></div></div>' +

                '<form autocomplete="off" style="display:none;border:10px aliceblue solid" id="subCommentForm' + response.article.comments[i]._id + '" enctype="multipart/form-data" action = "http://128.199.231.68/articles/addComment" method = "post" >' +
                '<input type = "text" id = "" name = "commentId" hidden/>' +
                '<input type="text" class="form-control" id="status" name="status" value="1" hidden />' +

                '<input type = "text" id = "clickcount" value="0" hidden/>' +
                '<input type="text" id="" name="articleId" value="' + response.article._id + '" hidden/>' +
                '<input type="text" id="" name="comment_user" value="' + localStorage.getItem("id") + '" hidden/>' +
                '<div class="comment media">' +
                '<div class="pr-15">' +
                '<a href="#" class="avatar avatar-sm">' +
                '<img src="../../../global/portraits/1.jpg" alt="...">' +
                '</a></div>' +
                '<div class="comment-body media-body">' +
                '<div class="comment-title">' +
                '<div class="comment-meta float-right">' +
                '</div>' +
                '<a href="javascript:void(0)" class="comment-author" id="current_user">' + localStorage.getItem("user") +

                '<a style="color:purple"> - Coordinator</a></a>' +
                '</div>' +
                '<div class="comment-content" style="display:grid;grid-template-columns:95% 2% 11%">' +
                '<input type="text" class="form-control" id="comment_content" name="comment_content" />' +
                '</br>' +
                '<button type="submit" id="submitSubCmt' + response.article.comments[i]._id + '" class= "btn btn-success" onclick="replyComment(event,this.id)" >Reply</button>' +

                '</div>' +
                '<i class="far fa-image"></i>' +

                '<div class="comment-actions text-left">' +
                '<div class="">' +
                '<h4 class="example-title">Upload picture</h4>' +
                '<div class="example">' +
                '<input type="file" name="files" id="image" data-plugin="dropify" data-default-file="" multiple />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div></form></div>');
            }
            if (response.article.comments[i].comment_user.role == 2) {
              $("#comment_section").append(
                '<div class="comment media" id="divcmt' + response.article.comments[i]._id + '">' +
                '<div class="pr-15"> <a href="#" class="avatar avatar-sm"><img src="../../../global/portraits/1.jpg" alt="..."></a></div>' +
                '<div class="comment-body media-body"><div class= "comment-title">' +
                '<div class="comment-meta float-right">' +
                '<span>' + response.article.comments[i].commented_At + '</span></div>' +
                '<a href="javascript:void(0)" class="comment-author">' + response.article.comments[i].comment_user.username + '</a><a style="color:blue"> - Manager</a></div >' +
                '<div class="comment-content">' +
                '<p>' + response.article.comments[i].comment + '</p>' +
                //'<div class="itemDesc"><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +

                '<div class="itemDesc" id =img' + response.article.comments[i]._id + '><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +
                //'<button type="button" class="btn btn-pure btn-default icon md-edit p-0 btn-edit"></button>' +
                //'<button type="button" class="btn btn-pure btn-default icon md-delete p-0 btn-trash"></button></div>' +
                '<div class="comment-actions text-left"> <a href="javascript:void(0)" role="button" id=reply' + response.article.comments[i]._id + ' onclick="Reply(this.id)">Reply</a>' +
                '<a href="#"><i class="icon md-chevron-down"></i></a>' +
                '<span class="pos-number mx-5">' + response.article.comments[i].status + '</span>'
                + '<a href="#"><i class="icon md-chevron-up"></i></a></div></div>' +

                '<form autocomplete="off" style="display:none;border:10px aliceblue solid" id="subCommentForm' + response.article.comments[i]._id + '" enctype="multipart/form-data" action = "http://128.199.231.68/articles/addComment" method = "post" >' +
                '<input type = "text" id = "" name = "commentId" hidden/>' +
                '<input type="text" class="form-control" id="status" name="status" value="1" hidden />' +

                '<input type = "text" id = "clickcount" value="0" hidden/>' +
                '<input type="text" id="" name="articleId" value="' + response.article._id + '" hidden/>' +
                '<input type="text" id="" name="comment_user" value="' + localStorage.getItem("id") + '" hidden/>' +

                '<div class="comment media">' +
                '<div class="pr-15">' +
                '<a href="#" class="avatar avatar-sm">' +
                '<img src="../../../global/portraits/1.jpg" alt="...">' +
                '</a></div>' +
                '<div class="comment-body media-body">' +
                '<div class="comment-title">' +
                '<div class="comment-meta float-right">' +
                '</div>' +
                '<a href="javascript:void(0)" class="comment-author" id="current_user">' + localStorage.getItem("user") +

                '<a style="color:blue"> - Manager</a></a>' +
                '</div>' +
                '<div class="comment-content" style="display:grid;grid-template-columns:95% 2% 11%">' +
                '<input type="text" class="form-control" id="comment_content" name="comment_content" />' +
                '</br>' +
                '<button type="submit" id="submitSubCmt' + response.article.comments[i]._id + '" class= "btn btn-success" onclick="replyComment(event,this.id)" >Reply</button>' +

                '</div>' +
                '<i class="far fa-image"></i>' +

                '<div class="comment-actions text-left">' +
                '<div class="">' +
                '<h4 class="example-title">Upload picture</h4>' +
                '<div class="example">' +
                '<input type="file" name="files" id="image" data-plugin="dropify" data-default-file="" multiple />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div></form></div>');

            }
            if (response.article.comments[i].comment_user.role == 0) {
              $("#comment_section").append(
                '<div class="comment media" id="divcmt' + response.article.comments[i]._id + '">' +
                '<div class="pr-15"> <a href="#" class="avatar avatar-sm"><img src="../../../global/portraits/1.jpg" alt="..."></a></div>' +
                '<div class="comment-body media-body"><div class= "comment-title">' +
                '<div class="comment-meta float-right">' +
                '<span>' + response.article.comments[i].commented_At + '</span></div>' +
                '<a href="javascript:void(0)" class="comment-author">' + response.article.comments[i].comment_user.username + '</a><a style="color:red"> - Admin</a></div >' +
                '<div class="comment-content">' +
                '<p>' + response.article.comments[i].comment + '</p>' +
                //'<div class="itemDesc"><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +

                '<div class="itemDesc" id =img' + response.article.comments[i]._id + '><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +
                //'<button type="button" class="btn btn-pure btn-default icon md-edit p-0 btn-edit"></button>' +
                //'<button type="button" class="btn btn-pure btn-default icon md-delete p-0 btn-trash"></button></div>' +
                '<div class="comment-actions text-left"> <a href="javascript:void(0)" role="button" id=reply' + response.article.comments[i]._id + ' onclick="Reply(this.id)">Reply</a>' +
                '<a href="#"><i class="icon md-chevron-down"></i></a>' +
                '<span class="pos-number mx-5">' + response.article.comments[i].status + '</span>'
                + '<a href="#"><i class="icon md-chevron-up"></i></a></div></div>' +

                '<form autocomplete="off" style="display:none;border:10px aliceblue solid" id="subCommentForm' + response.article.comments[i]._id + '" enctype="multipart/form-data" action = "http://128.199.231.68/articles/addComment" method = "post" >' +
                '<input type = "text" id = "" name = "commentId" hidden/>' +
                '<input type="text" class="form-control" id="status" name="status" value="1" hidden />'+

                '<input type = "text" id = "clickcount" value="0" hidden/>' +
                '<input type="text" id="" name="articleId" value="' + response.article._id + '" hidden/>' +
                '<input type="text" id="" name="comment_user" value="' + localStorage.getItem("id") + '" hidden/>' +

                '<div class="comment media">' +
                '<div class="pr-15">' +
                '<a href="#" class="avatar avatar-sm">' +
                '<img src="../../../global/portraits/1.jpg" alt="...">' +
                '</a></div>' +
                '<div class="comment-body media-body">' +
                '<div class="comment-title">' +
                '<div class="comment-meta float-right">' +
                '</div>' +
                '<a href="javascript:void(0)" class="comment-author" id="current_user">' + localStorage.getItem("user") +

                '<a style="color:red"> - Admin</a></a>' +
                '</div>' +
                '<div class="comment-content" style="display:grid;grid-template-columns:95% 2% 11%">' +
                '<input type="text" class="form-control" id="comment_content" name="comment_content" />' +
                '</br>' +

                '<button type="submit" id="submitSubCmt' + response.article.comments[i]._id + '" class= "btn btn-success" onclick="replyComment(event,this.id)" >Reply</button>' +

                '</div>' +
                '<i class="far fa-image"></i>' +

                '<div class="comment-actions text-left">' +
                '<div class="">' +
                '<h4 class="example-title">Upload picture</h4>' +
                '<div class="example">' +
                '<input type="file" name="files" id="image" data-plugin="dropify" data-default-file="" multiple />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div></form></div>');
            }
            if (response.article.comments[i].comment_user.role == 1 || response.article.comments[i].comment_user.role == 4) {
              $("#comment_section").append(
                '<div class="comment media" id="divcmt' + response.article.comments[i]._id + '">' +
                '<div class="pr-15"> <a href="#" class="avatar avatar-sm"><img src="../../../global/portraits/1.jpg" alt="..."></a></div>' +
                '<div class="comment-body media-body"><div class= "comment-title">' +
                '<div class="comment-meta float-right">' +
                '<span>' + response.article.comments[i].commented_At + '</span></div>' +
                '<a href="javascript:void(0)" class="comment-author">' + response.article.comments[i].comment_user.username + '</a></div >' +
                '<div class="comment-content">' +
                '<p>' + response.article.comments[i].comment + '</p>' +
                //'<div class="itemDesc"><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +

                '<div class="itemDesc" id =img' + response.article.comments[i]._id + '><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +
                //'<button type="button" class="btn btn-pure btn-default icon md-edit p-0 btn-edit"></button>' +
                //'<button type="button" class="btn btn-pure btn-default icon md-delete p-0 btn-trash"></button></div>' +
                '<div class="comment-actions text-left"> <a href="javascript:void(0)" role="button" id=reply' + response.article.comments[i]._id + ' onclick="Reply(this.id)">Reply</a>' +
                '<a href="#"><i class="icon md-chevron-down"></i></a>' +
                '<span class="pos-number mx-5">' + response.article.comments[i].status + '</span>'
                + '<a href="#"><i class="icon md-chevron-up"></i></a></div></div>' +

                '<form autocomplete="off" style="display:none;border:10px aliceblue solid" id="subCommentForm' + response.article.comments[i]._id + '" enctype="multipart/form-data" action = "http://128.199.231.68/articles/addComment" method = "post" >' +
                '<input type = "text" id = "" name = "commentId" hidden/>' +
                '<input type="text" class="form-control" id="status" name="status" value="1" hidden />' +

                '<input type = "text" id = "clickcount" value="0" hidden/>' +
                '<input type="text" id="" name="articleId" value="' + response.article._id + '" hidden/>' +
                '<input type="text" id="" name="comment_user" value="' + localStorage.getItem("id") + '" hidden/>' +

                '<div class="comment media">' +
                '<div class="pr-15">' +
                '<a href="#" class="avatar avatar-sm">' +
                '<img src="../../../global/portraits/1.jpg" alt="...">' +
                '</a></div>' +
                '<div class="comment-body media-body">' +
                '<div class="comment-title">' +
                '<div class="comment-meta float-right">' +
                '</div>' +
                '<a href="javascript:void(0)" class="comment-author" id="current_user">' + localStorage.getItem("user") +

                '</a>' +
                '</div>' +
                '<div class="comment-content" style="display:grid;grid-template-columns:95% 2% 11%">' +
                '<input type="text" class="form-control" id="comment_content" name="comment_content" />' +
                '</br>' +

                '<button type="submit" id="submitSubCmt' + response.article.comments[i]._id + '" class= "btn btn-success" onclick="replyComment(event,this.id)" >Reply</button>' +

                '</div>' +
                '<i class="far fa-image"></i>' +

                '<div class="comment-actions text-left">' +
                '<div class="">' +
                '<h4 class="example-title">Upload picture</h4>' +
                '<div class="example">' +
                '<input type="file" name="files" id="image" data-plugin="dropify" data-default-file="" multiple />' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div></form></div>');
            }
          }
          if (response.article.comments[i].subComment.length > 0) {
            $("#comment_section").append(
              '<div class="comment media" id="divcmt' + response.article.comments[i]._id + '">' +
              '<div class="pr-15"> <a href="#" class="avatar avatar-sm"><img src="../../../global/portraits/1.jpg" alt="..."></a></div>' +
              '<div class="comment-body media-body"><div class= "comment-title">' +
              '<div class="comment-meta float-right">' +
              '<span>' + response.article.comments[i].commented_At + '</span></div>' +
              '<a href="javascript:void(0)" class="comment-author">' + response.article.comments[i].comment_user.username + '</a></div >' +
              '<div class="comment-content">' +
              '<p>' + response.article.comments[i].comment + '</p>' +
              //'<div class="itemDesc"><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +

              '<div class="itemDesc" id =img' + response.article.comments[i]._id + '><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].image[0] + '" alt=""></div>' +
              //'<button type="button" class="btn btn-pure btn-default icon md-edit p-0 btn-edit"></button>' +
              //'<button type="button" class="btn btn-pure btn-default icon md-delete p-0 btn-trash"></button></div>' +
              '<div class="comment-actions text-left"> <a href="javascript:void(0)" role="button" id=reply' + response.article.comments[i]._id + ' onclick="Reply(this.id)">Reply</a>' +
              '<a href="#"><i class="icon md-chevron-down"></i></a>' +
              '<span class="pos-number mx-5">' + response.article.comments[i].status + '</span>'
              + '<a href="#"><i class="icon md-chevron-up"></i></a></div></div>' +

              '<div class="" id="subdiv"></div>' +

              '<form autocomplete="off" style="display:none;border:10px aliceblue solid" id="subCommentForm' + response.article.comments[i]._id + '" enctype="multipart/form-data" action = "http://128.199.231.68/articles/addComment" method = "post" >' +
              '<input type = "text" id = "commentId" name = "commentId" hidden/>' +
              '<input type="text" class="form-control" id="status" name="status" value="1" hidden />' +

              '<input type = "text" id = "clickcount" value="0" hidden/>' +
              '<input type="text" id="" name="comment_user" value="' + localStorage.getItem("id") + '" hidden/>' +
              '<h4 class="m-0">Reply Comments</h4>' +
              '<div class="comment media">' +
              '<div class="pr-15">' +
              '<a href="#" class="avatar avatar-sm">' +
              '<img src="../../../global/portraits/1.jpg" alt="...">' +
              '</a></div>' +
              '<div class="comment-body media-body">' +
              '<div class="comment-title">' +
              '<div class="comment-meta float-right">' +
              '</div>' +
              '<a href="javascript:void(0)" class="comment-author" id="current_user">' + localStorage.getItem("user") +

              '</a>' +
              '</div>' +
              '<div class="comment-content" style="display:grid;grid-template-columns:95% 2% 11%">' +
              '<input type="text" class="form-control" id="comment_content" name="comment_content" />' +
              '</br>' +

              '<button type="submit" id="submitSubCmt' + response.article.comments[i]._id + '" class= "btn btn-success" onclick="replyComment(event,this.id)" >Reply</button>' +

              '</div>' +
              '<i class="far fa-image"></i>' +

              '<div class="comment-actions text-left">' +
              '<div class="">' +
              '<h4 class="example-title">Upload picture</h4>' +
              '<div class="example">' +
              '<input type="file" name="files" id="image" data-plugin="dropify" data-default-file="" multiple />' +
              '</div>' +
              '</div>' +
              '</div>' +
              '</div>' +
              '</div></form></div>');
            for (var a = 0; a < response.article.comments[i].subComment.length; a++) {

              $("#divcmt" + response.article.comments[i]._id).find("div[id=subdiv]").append(

                '<div class="comment media">' +
                '<div class="pr-15"> <a href="#" class="avatar avatar-sm"><img src="../../../global/portraits/1.jpg" alt="..."></a></div>' +
                '<div class="comment-body media-body"><div class= "comment-title">' +
                '<div class="comment-meta float-right">' +
                '<span>' + response.article.comments[i].subComment[a].commented_At + '</span></div>' +
                '<a href="javascript:void(0)" class="comment-author">' + response.article.comments[i].subComment[a].comment_user.username + '</a></div >' +
                '<div class="comment-content">' +
                '<p>' + response.article.comments[i].subComment[a].comment + '</p>' +
                '<div class="itemDesc" id =img' + response.article.comments[i].subComment[a]._id + '><img style="max-width:500px;max-height:400px" src="http://128.199.231.68/' + response.article.comments[i].subComment[a].image + '" alt="img"></div>' +

                + '</div></div></div>');

              if (response.article.comments[i].subComment[a].image.length == 0) {
                $("#img" + response.article.comments[i].subComment[a]._id).hide();
              }
            }
          }

          if (response.article.comments[i].image.length == 0) {
            $("#img" + response.article.comments[i]._id).hide();
          }
        }
      }

    }
  });
}
$(document).ready(function () {
  $("#lbluser").val(localStorage.getItem("user"));
  $("#lblemail").val(localStorage.getItem("email"));
  $("#lbluser").text(localStorage.getItem("user"));
  $("#lblemail").text(localStorage.getItem("email"));
  $("#person").val(localStorage.getItem("user"));
  $("#postemail").val(localStorage.getItem("email"));

  if (localStorage.getItem("accessToken") != null && (localStorage.getItem("role") == 1 ||
    localStorage.getItem("role") == 3)) {
    load();
    $("#artId").val(articleId);

  }
  else {
    alert("U dont have any business here. GTFO");
    window.location.href = "../homepage/homepage.html";
  }
})
function Reply(id) {
  $("#" + id).parent().parent().parent().find("form").find("input[name=commentId]").val(id.replace("reply", ""))
  var clicktimes = $("#" + id).parent().parent().parent().find("form").find("input[id=clickcount]").val();

  if (clicktimes == 0) {
    var ele = $("#" + id).parent().parent().parent().find("form").attr('id');

    $("#" + id).parent().parent().parent().find("form").css("display", "block");
    $("#" + id).parent().parent().parent().find("form").css("display", "block");

    $("#" + id).parent().parent().parent().find("form").find("input[id=clickcount]").val(1);
    $('html,body').animate({
      scrollTop: $("#" + ele).offset().top
    }, 'fast');
    //console.log(ele);

  }
  if (clicktimes == 1) {
    $("#" + id).parent().parent().parent().find("form").css("display", "none");
    $("#" + id).parent().parent().parent().find("form").find("input[id=clickcount]").val(0);
  }

}
function replyComment(evt, id) {
  evt.preventDefault();

  var comment = $("#" + id).parent().parent().parent().parent().find("input[id=comment_content]").val().trimEnd();
  var image = $("#" + id).parent().parent().parent().parent().find("input[id=image]").val().trimEnd();

  if (comment == "" && image == "") {
    alert("Gõ gì vào đã rồi hẵng comment");
    return false;
  }
  var data = $("#" + id).parent().parent().parent().parent();
  //console.log(data);

  $(data).ajaxSubmit({

    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
      //request.setRequestHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2I3NjE3OGU3Y2IyODEzNmNlNjQ3MDAiLCJlbWFpbCI6InN0dWRlbnRAZnB0LmNvbSIsInJvbGUiOjEsInVzZXJuYW1lIjoic3R1ZGVudCIsImlhdCI6MTU1NTUyMTkyNywiZXhwIjoxNTU1NjA4MzI3fQ.uGu-uc_28wpG5YuCdyT161bJJCspNECQVZj8kTxqK64");

    },
    error: function (xhr) {
      console.log('Error: ' + xhr.status);
    },
    success: function (response) {
      console.log(response);
      load();
    }
  });
}
function chooseimage() {
  $("#image").click();
}
function handleKeyPress(evt) {
  var key = e.keyCode || e.which;
  if (key == 13) {
    //addComment();
    $("#submitBtn").click();
  }
}
$('#cmtForm').submit(function (e) {
  e.preventDefault();
  var comment = $("#comment_content").val().trimEnd();
  if (comment == "" && $("#image").val() == "") {
    alert("Gõ gì vào đã rồi hẵng comment");
    return false;
  }

  //var host_api = "http://128.199.231.68/articles/addComment";
  //var host_api = "http://128.199.231.68/articles/addComment";
  //console.log($(this));
  $(this).ajaxSubmit({

    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));

    },
    async: false,
    error: function (xhr) {
      console.log('Error: ' + xhr.status);
    },
    success: function (response) {
      console.log(response);

      load();
    }
  });

  return false;
});

function actChangeStatus() {
  var statusId = $('#articleStatus').val();
  var data = {
    articleId: articleId,
    updatedBy: localStorage.getItem("id"),
    status: statusId
  }
  console.log(data);
  $.ajax({
    url: "http://128.199.231.68/articles/editArticleStatus",
    type: "POST",
    contentType: "application/json",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    data: JSON.stringify(data),
    success: function (response) {
      alert(response.msg);
      
      console.log(response)
    },
    error: function (response) {
      alert(response.msg);
      console.log(response)
    }
  })
}

function addOption($select, text, value) {
  var $opt = $('<option />', {
    text: text
  });
  if (value === null) {
    value = text.toLowerCase().replace(/\s+/g, '-');
  }
  $opt.attr('value', value);
  $select.append($opt);
  $select.selectpicker('refresh');
}



$('#uploadForm').submit(function (e) {
  e.preventDefault();
  var title = $("#uploadForm input#inputTitle").val();
  $("#articleFaculty").val(localStorage.getItem("userFaculty"));
  console.log($(this))
  if (title == "" || title == null) {
    alert("Please enter title!");
    return false;
  }

  $(this).ajaxSubmit({
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));

    },
    error: function (xhr) {
      alert(xhr.msg);

      console.log('Error: ' + xhr.status);
    },
    success: function (response) {
      load()
      alert(response.msg);
      console.log(response);
    }
  });

});
