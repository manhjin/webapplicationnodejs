$(document).ready(function () {
  $("#lbluser").text(localStorage.user);
  $("#lblemail").text(localStorage.email);

  console.log(localStorage.getItem("accessToken"));
  // && localStorage.getItem("role") === 0
  if (localStorage.getItem("accessToken") != null && localStorage.getItem("role") == 1) {
    loadpage();

  } else {
    window.location.href = "../login/login.html";
  }
});

function loadpage() {
  var dataTable = $("#tblArticle").dataTable().api();
  dataTable.clear();
  $("#tblArticle tbody").remove("tr");
  var encodedData = window.location.href.split('=')[1];
  var articleValue = decodeURI(encodedData);
  var host_api = "http://128.199.231.68/articles/getUsersArticle";

  $.ajax({
    url: host_api,
    method: 'POST',
    beforeSend: function (request) {
      request.setRequestHeader('Authorization', localStorage.getItem('accessToken'));
    },
    data: {
      id: articleValue
    },
    success: function (response) {
      console.log(response.articles);
      if (response.articles.length > 0) {
        response.articles.forEach(item => {
          var row = "<tr>";
          row += "<td id='articleId' hidden>" + item._id + "</td>";
          row += "<td>" + item.articleName + "</td>";
          row += "<td>" + item.created_At + "</td>";
          row += "<td>" + item.categoryName + "</td>";
          switch (item.status) {
            case 0:
              row += "<td style='color:red'>Decline</td>";
              break;
            case 2:
              row += "<td style='color:green'>Approved</td>";
              break;
            default:
              row += "<td style='color:blue'>Pending</td>";
          }
          var tr = document.createElement("tr");
          tr.innerHTML = row;
          dataTable.row.add(tr);
        });
        dataTable.draw(false);

      } else {
        alert("Không có kết quả !");
        dataTable.clear();
        dataTable.draw();
      }
    },
    error(jqXHR) {
      //
    }
  });
}
$(document).ready(function () {
  var table = $('#tblArticle').DataTable();

  $('#tblArticle tbody').on('click', 'tr', function () {
    var articleId = $(this).find("td#articleId").html();
    var userId = localStorage.getItem("id");
    window.location.href = '../review-article/review-article.html?id=' 
    + articleId + "&userid=" + userId;

  });

  $('#button').click(function () {
    table.row('.selected').remove().draw(false);
  });
});
