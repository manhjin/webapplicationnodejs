if (localStorage.getItem("accessToken") != null &&
  localStorage.getItem("accessToken") != '' &&
  typeof (localStorage.getItem("accessToken") != 'undefined' &&
    localStorage.getItem("role") != null &&
    localStorage.getItem("role") != '' &&
    typeof (localStorage.getItem("role") != 'undefined'))) {
  $(document).ready(function () {
    $("#lbluser").text(localStorage.getItem("user"));
    $("#lblemail").text(localStorage.getItem("email"));
    $.ajax({
      url: "http://128.199.231.68/closures/getClosure",
      type: "POST",
      success: function (response) {
        console.log(response.data[0]);
        $("#exsub").css("display","block");
        $("#exedi").css("display", "block");
        $("#expire_submit").text(response.data[0].expired_At).trigger("change");
        $("#expire_edit").text(response.data[0].expired_Edit).trigger("change");

      }
    })

    var arr = [];
    var url = [];
    var icon = [];
    listFaculty();
    switch (localStorage.getItem("role")) {
      case "0": //admin
        arr = ["Manage Account", "Manage Faculty", "Manage Closure Date"];
        url = ["manage-account",
          "manage-faculty",
          "closure-date"
        ];
        icon = ["site-menu-icon md-border-all", "site-menu-icon md-border-all", "site-menu-icon md-border-all"];
        break;
      case "1": //student
        arr = ["Create Post", "My Articles"];
        url = ["submission",
          "my-article?id=" + localStorage.getItem("id")
        ];
        icon = ["site-menu-icon md-comment-alt-text", "site-menu-icon md-palette"]
        break;
      case "2": //marketing manager
        arr = ["Statistical analysis", "List Of Closing Dates",];
        url = ["statistics", "closure-date"];
        icon = ["site-menu-icon md-chart", "site-menu-icon md-border-all"]
        break;
      case "3": //coordinator manager
        arr = ["Manage Topics", "Manage Articles", "Manage Guest"];
        url = ["manage-topic",
          "manage-article",
          "manage-guest"
        ];
        icon = ["site-menu-icon md-border-all", "site-menu-icon md-border-all", "site-menu-icon md-border-all"]
        break;
      case "4": //guest
        break;
    }
    if (localStorage.getItem("role") != 4) {
      $("#left-menu").append("<li class='site-menu-item'>" +
        "<a class='animsition-link' href='../view-faculty'>" +
        "<i class='site-menu-icon md-border-all' aria-hidden='true'></i>" +
        "<span class='site-menu-title'>Faculties</span></a></li>");
    } else {
      $("#left-menu").append("<li class='site-menu-item'>" +
        "<a class='animsition-link' href='../view-categories'>" +
        "<i class='site-menu-icon md-border-all' aria-hidden='true'></i>" +
        "<span class='site-menu-title'>Categories</span></a></li>");
    }
    for (var i = 0; i < arr.length; i++) {
      $("#left-menu").append("<li class='site-menu-item'>" +
        "<a class='animsition-link' href='../" + url[i] + "'>" +
        "<i class='" + icon + "' aria-hidden='true'></i>" +
        "<span class='site-menu-title'>" + arr[i] + "</span></a></li>");
    }
  });
} else {
  window.location.href = "../login/login.html";
}

function profile() {
  if (localStorage.getItem("accessToken") != null &&
    localStorage.getItem("accessToken") != '' &&
    typeof (localStorage.getItem("accessToken") != 'undefined')) {
    window.location.href = "profile?id=" + localStorage.getItem("id");
  }
}

function listFaculty() {
  var host_api = "http://128.199.231.68/faculties/getAllFaculty";
  $.ajax({
    url: host_api,
    method: "GET",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      console.log(arrdetail.data);
      if (arrdetail.data.length > 0) {
        arrdetail.data.forEach(item => {
          $('#listFaculty').append("<a class='dropdown-item' href='article-by-faculty?id=" + item._id + "' role='menuitem'>" +
            item.facultyName +
            "</a><div class='dropdown-divider'></div>")
        });

      } else {
        alert("Không có kết quả !");
      }
    },
    error(jqXHR) {
      //
    }
  });
}
