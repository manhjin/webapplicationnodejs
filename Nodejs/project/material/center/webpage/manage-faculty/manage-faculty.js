$(document).ready(function () {
  $("#lbluser").text(localStorage.user);
  $("#lblemail").text(localStorage.email);
  
  console.log(localStorage.getItem("accessToken"));
  // && localStorage.getItem("role") === 0
  if (localStorage.getItem("accessToken") != null && localStorage.getItem("role") == 0) {
    loadpage();
    
  } else {
    window.location.href = "../login/login.html";
  }
});

function loadpage() {
  //table.page('first').draw(false);
  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  $("#exampleTableSearch tbody").remove("tr");
  var host_api = "http://128.199.231.68/faculties/getAllFaculty";

  $.ajax({
    url: host_api,
    method: "GET",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      console.log(arrdetail.data);
      if (arrdetail.data.length > 0) {
        arrdetail.data.forEach(item => {
          var row = "<tr>";
          row += "<td>" + item.facultyName + "</td>";
          row += "<td>" + item.facultyDesc + "</td>";
          row += "<td>" + item.created_By.username + "</td>";
          row +=
            "<td> <button class='btn btn - primary' data-target='#roleform' data-toggle='modal'type = 'button'  id='" + item._id + "'  onclick='chagerole(this.id)' >Edit <i class='icon md-account' aria-hidden='true'></i> </button ></td>";
          row += "</tr>";
          var tr = document.createElement("tr");
          tr.innerHTML = row;
          dataTable.row.add(tr);
        });
        dataTable.draw(false);

      } else {
        alert("Không có kết quả !");
        dataTable.clear();
        dataTable.draw();
      }
    },
    error(jqXHR) {
      //
    }
  });
}
function addnew() {

  var dataTable = $("#exampleTableSearch").dataTable().api();
  dataTable.clear();
  var facultyName = $("#facultyName").val();
  if (facultyName == "") {
    alert("Please input the faculty Name");
    return false;
  }
  var facultyDesc = $("#facultyDesc").val();
  if (facultyDesc == "") {
    alert("Please input the faculty Desc");
    return false;
  }



  var host_api = "http://128.199.231.68/faculties/createNewFaculty";
  var data_str = {
    facultyName: facultyName,
    facultyDesc: facultyDesc

  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      if (result.code == 300) {
        alert("Add new faculty successfully!");
        loadpage();

      } else {
        alert(result.err)
      }
    },
    error(jqXHR) {
      alert(jqXHR)
    }
  });
}

function chagerole(id) {
  var id = id;
  var host_api = "http://128.199.231.68/faculties/getFacultyInfo";
  var data_str = {
    id: id
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      console.log(result);

      var arrdetail = JSON.parse(JSON.stringify(result));
      $("#newfacultyname").val(arrdetail.faculty[0].facultyName).trigger("change");
      $("#newfacultydesc").val(arrdetail.faculty[0].facultyDesc).trigger("change");
      $("#facultyid").val(arrdetail.faculty[0]._id).trigger("change");

    },
    error(jqXHR) {
      alert(jqXHR);
    }
  });

}
function savechange() {
  var ChangefacultyName = $("#newfacultyname").val();
  if (ChangefacultyName == "") {
    alert("Please enter the faculty Name");
    return false;
  }
  var ChangefacultyDesc = $("#newfacultydesc").val();
  if (ChangefacultyDesc == "") {
    alert("Please enter the faculty Description");
    return false;
  }
  var facultyid = $("#facultyid").val();
    

  var host_api = "http://128.199.231.68/faculties/editFaculty";
  var data_str = {
    facultyName: ChangefacultyName,
    facultyDesc: ChangefacultyDesc,
    facultyId: facultyid
  };
  console.log(data_str);
  $.ajax({
    contentType: 'application/json',
    url: host_api,
    type: "POST",
    data: JSON.stringify(data_str),
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      alert(result.msg);
      loadpage();
    },
    error(jqXHR) {
      alert(jqXHR.msg);

    }
  });
}
