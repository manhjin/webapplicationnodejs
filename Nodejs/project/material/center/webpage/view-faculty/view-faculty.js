$(document).ready(function () {
  $("#lbluser").text(localStorage.user);
  $("#lblemail").text(localStorage.email);

  console.log(localStorage.getItem("accessToken"));
  // && localStorage.getItem("role") === 0
  if (localStorage.getItem("accessToken") != null && localStorage.getItem("role") != 4) {
    loadpage();

  } else {
    window.location.href = "../login/login.html";
  }
});

function loadpage() {
  var host_api = "http://128.199.231.68/faculties/getAllFaculty";
  $.ajax({
    url: host_api,
    method: "GET",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      if (arrdetail.data.length > 0) {
        arrdetail.data.forEach(item => {
          $('#listFaculty').append('<li>' +
            '<div class="panel">' +
            '<figure class="overlay overlay-hover animation-hover">' +
            '<img class="caption-figure overlay-figure" src="../../../global/photos/placeholder.png">' +
            '<figcaption class="overlay-panel overlay-background overlay-fade text-center vertical-align">' +
            '<a href="../article-by-faculty/article-by-faculty.html?id=' + item._id + '">' +
            '<button type="button" class="btn btn-inverse project-button" href="#">View Faculty</button>' +
            '</a>' +
            '</figcaption>' +
            '</figure>' +
            '<div class="time float-right">' + item.created_By.createdAt + '</div>' +
            '<div class="text-truncate">' + item.facultyName + '</div>' +
            '</div>' +
            '</li>');
        });

      } else {
        alert("No result");
      }
    },
    error(jqXHR) {
      alert("Failed to load faculty")
    }
  });
}
