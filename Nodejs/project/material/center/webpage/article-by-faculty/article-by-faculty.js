if (localStorage.getItem("accessToken") != null) {
  var pagOption = {
    page: 0,
    limit: 4
  };
  var inputCategoryId = getParam("id");
  $("#pageTitle").append("<h2 style='color: #0079D3'>" + getFaculty(inputCategoryId) + "</h2>")
  $(document).ready(function () {
    listTopic();
    $("#lbluser").text(localStorage.getItem("user"));
    $("#lblemail").text(localStorage.getItem("email"));
    $.ajax({
      url: "http://128.199.231.68/articles/getPostByFaculty",
      type: "POST",
      beforeSend: function (request) {
        request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
      },
      data: {
        pagOption,
        facultyId: inputCategoryId

      },
      success: function (response) {
        if (response.code === 404) {
          console.log(response.msg);
        }
        if (response.code === -1) {
          console.log(response.msg);
        }
        if (response.code === 300) {

          pagOption.page++;
          var data = response.data;
          var urlArticle = '';
          for (var i = 0; i < data.length; i++) {
            if (response.data[i] == null) {

            } else {
              urlArticle = '"../article/article.html?id=' + data[i]._id + '"';
              upVoteFunction = '"' + data[i]._id + '","' + data[i].upVote.length + '","' + data[i].downVote.length + '", 1';
              downVoteFunction = '"' + data[i]._id + '","' + data[i].upVote.length + '","' + data[i].downVote.length + '", 0';
              $(".left_content").append("<div class='panel'>" +
                "<div class='itemVote'>" +
                "<div class='itemUpvoteContent'>" +
                "<a id='upvote' onclick='vote(" + upVoteFunction + ")'></a>" +
                "<div class='itemUpvoteNumber' id='" + data[i]._id + "'></div>" +
                "<a id='downvote' onclick='vote(" + downVoteFunction + ")'></a></div></div>" +
                "<div class = 'itemContent' onclick='window.location.href=" + urlArticle + "'> " +
                "<div class = 'itemUser' ><a style='color:green'>r/" + data[i].categoryName + "</a> •Posted by <a style='color:blue'>" + data[i].created_By.username +
                "</a> at " + data[i].created_At + "</div> " +
                "<div class = 'itemTitle'>" + data[i].articleName + "</div>" +
                "<div class = 'itemDesc'><img src='http://128.199.231.68/" + data[i].articleImage + "' alt=''>" + data[i].articleContent + "</div>" +
                "<div class = 'itemAction' > " +
                "<a href ='#'>" +
                "<img src = '../../assets/images/icons8-comments-16.png' alt = '' >" +
                "<span> 612 Comments </span> </a> <a href = '#' >" +
                "<img src = '../../assets/images/icons8-share-16.png' alt = '' >" +
                "<span> Share </span></a> <a href = '#' >" +
                "<img src = '../../assets/images/icons8-save-16.png' alt = '' >" +
                "<span> Save </span></a> </div> </div> </div>");
            }

          }
        }
      }
    })
  });

  $(window).scroll(function () {
    if ($(window).scrollTop() >= $(document).height() - $(window).height()) {

      $.ajax({
        url: "http://128.199.231.68/articles/getPostByFaculty",
        type: "POST",
        beforeSend: function (request) {
          request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        data: {
          pagOption,
          categoryId: inputCategoryId
        },
        success: function (response) {
          if (response.code === 404) {
            console.log(response.msg);
          }
          if (response.code === -1) {
            console.log(response.msg);
          }
          if (response.code === 300) {
            console.log(response)

            pagOption.page++;
            var data = response.data;
            for (var i = 0; i < data.length; i++) {
              urlArticle = '"../article/article.html?id=' + data[i]._id + '"';
              upVoteFunction = '"' + data[i]._id + '","' + data[i].upVote.length + '","' + data[i].downVote.length + '", 1';
              downVoteFunction = '"' + data[i]._id + '","' + data[i].upVote.length + '","' + data[i].downVote.length + '", 0';
              $(".left_content").append("<div class='panel'>" +
                "<div class='itemVote'>" +
                "<div class='itemUpvoteContent'>" +
                "<a id='upvote' onclick='vote(" + upVoteFunction + ")'></a>" +
                "<div class='itemUpvoteNumber' id='" + data[i]._id + "'></div>" +
                "<a id='downvote' onclick='vote(" + downVoteFunction + ")'></a></div></div>" +
                "<div class = 'itemContent' onclick='window.location.href=" + urlArticle + "'> " +
                "<div class = 'itemUser'><a style='color:green'>r/" + data[i].categoryName + "</a> •Posted by " + localStorage.getItem("user") +
                "4 hours ago</div> " +
                "<div class = 'itemTitle'>" + data[i].articleName + "</div>" +
                "<div class = 'itemDesc'><img src='http://128.199.231.68/" + data[i].articleImage + "' alt=''>" + data[i].articleContent + "</div>" +
                "<div class = 'itemAction' > " +
                "<a href ='#'>" +
                "<img src = '../../assets/images/icons8-comments-16.png' alt = '' >" +
                "<span> 612 Comments </span> </a> <a href = '#' >" +
                "<img src = '../../assets/images/icons8-share-16.png' alt = '' >" +
                "<span> Share </span></a> <a href = '#' >" +
                "<img src = '../../assets/images/icons8-save-16.png' alt = '' >" +
                "<span> Save </span></a> </div> </div> </div>");

            }
          }
        }
      })
    }
  })
} else {
  window.location.href = "../login/login.html";
}



function listTopic() {
  var inputFaculty = localStorage.getItem("userFaculty");
  var host_api = "http://128.199.231.68/category/getCategoryByFaculty";
  $.ajax({
    url: host_api,
    type: "POST",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    data: {
      facultyId: inputFaculty
    },
    success: function (result) {
      var arrdetail = JSON.parse(JSON.stringify(result));
      if (arrdetail.data.length > 0) {
        arrdetail.data.forEach(item => {
          $('.listTopic').append("<a class='badge badge-outline badge-primary' href='../article-by-topic/article-by-topic.html?id='" + item._id + ">" +
            item.categoryName + "</span>")
        });
      } else {
        alert("Không có kết quả !");
      }
    },
    error(jqXHR) {
      alert("Failed to load topic")
    }
  });
}
