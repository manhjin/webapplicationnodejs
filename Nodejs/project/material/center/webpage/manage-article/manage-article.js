$(document).ready(function () {
  $("#lbluser").text(localStorage.user);
  $("#lblemail").text(localStorage.email);

  console.log(localStorage.getItem("accessToken"));
  // && localStorage.getItem("role") === 0
  if (localStorage.getItem("accessToken") != null && (localStorage.getItem("role") == 0 || localStorage.getItem("role") == 3)) {
    loadpage();

  } else {
    window.location.href = "../login/login.html";
  }
});

function loadpage() {
  var dataTable = $("#tblArticle").dataTable().api();
  var inputFacultyId = localStorage.getItem("userFaculty");
  dataTable.clear();
  $("#tblArticle tbody").remove("tr");
  var host_api = "http://128.199.231.68/articles/info";

  $.ajax({
    url: host_api,
    type: "POST",
    beforeSend: function (request) {
      request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
    },
    data: {
      facultyId: inputFacultyId
    },
    success: function (result) {
      console.log(result);

      if (result.article.length > 0) {
        for (var i = 0; i < result.article.length; i++) {
          var row = "<tr>";
          row += "<td id='articleId' hidden>" + result.article[i]._id + "</td>";
          row += "<td id='userId' hidden>" + result.article[i].created_By.username + "</td>";
          row += "<td>" + result.article[i].articleName + "</td>";
          row += "<td>" + result.article[i].created_By.username + "</td>";
          row += "<td>" + result.article[i].created_At + "</td>";
          row += "<td>" + result.article[i].categoryName + "</td>";
          switch (result.article[i].status) {
            case 0:
              row += "<td style='color:red'>Decline</td>";
              break;
            case 2:
              row += "<td style='color:green'>Approved</td>";
              break;
            default:
              row += "<td style='color:darkblue'>Pending</td>";
          }
          var tr = document.createElement("tr");
          tr.innerHTML = row;
          dataTable.row.add(tr);
        }      
        dataTable.draw(false);

      } else {
        alert("Không có kết quả !");
        dataTable.clear();
        dataTable.draw();
      }
    },
    error(jqXHR) {
      console.log(jqXHR);
    }
  });
}
$(document).ready(function () {
  var table = $('#tblArticle').DataTable();

  $('#tblArticle tbody').on('click', 'tr', function () {
    var articleId = $(this).find("td#articleId").html();
    var userId = $(this).find("td#userId").html();
    window.location.href = '../review-article/review-article.html?id=' 
    + articleId + '&userid=' + userId;
  });

  $('#button').click(function () {
    table.row('.selected').remove().draw(false);
  });
});
