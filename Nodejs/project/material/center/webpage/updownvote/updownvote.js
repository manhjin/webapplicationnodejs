function vote(articleId, upCount, downCount, upDown) {
  if (localStorage.getItem("accessToken") != null &&
    localStorage.getItem("accessToken") != '' &&
    typeof (localStorage.getItem("accessToken") != 'undefined')) {
    $(document).ready(function () {
      var param = {
        articleId: articleId,
        upDown: upDown
      };
      $.ajax({
        url: "http://128.199.231.68/users/upDownVoteArticle",
        //url: "http://localhost/users/upDownVoteArticle",
        type: "POST",
        contentType: "application/json",
        beforeSend: function (request) {
          request.setRequestHeader("Authorization", localStorage.getItem("accessToken"));
        },
        data: JSON.stringify(param),
        success: function (response) {
            console.log(response);
            if(response.code === -1){
                alert(response.mgs);
            }
            if(response.code === 300){
                console.log(response);
                console.log("success");
                var voteCount = upCount - downCount;
                console.log(upCount)
                console.log(downCount);
                $('#'+articleId).text(voteCount);

            }
        }
      })
    });
  } else {
    window.location.href = "../login/login.html";
  }
}
